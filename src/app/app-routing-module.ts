import { NgModule } from '@angular/core';
import { Routes, RouterModule ,NoPreloading} from '@angular/router';

const routes: Routes = [
/** Default Routes */
  { path: "", redirectTo: "/login", pathMatch: "full" },

/** Login Module Routes */
  {
    path: "",
    loadChildren: () =>import("./components/login/login-module").then(
        l => l.LoginModule)
  },
  /** Public Incident Form Module Routes */
  {
    path: "",
    loadChildren: () =>import("./components/public-incident-forms/public-incident-form.module").then(
        s => s.PublicIncidentFormModule)
  },
  {
    path: "",
    loadChildren: () =>import("./components/incidence-report-form/incidence-report-form/incidence-report-form.module").then(
        i => i.IncidenceReportFormModule)
  },

/** Admin Module Routes */
  {
    path: "configuration",
    loadChildren: () => import("./components/admin/configuration/configuration-lists/configuration-lists.module").then(
        c => c.ConfigurationListsModule), data: {title1: "configuration-list"}
  },
  {
    path: "configuration-form",
    loadChildren: () => import("./components/admin/admin-create-edit/create-edit-form/create-edit-form.module").then(
        c => c.CreateEditFormModule)
  },
  {
    path: "admin",
    loadChildren: () => import("./components/admin/admins/admin-list/admin-list.module").then(
        a => a.AdminListModule)
  },
  {
    path: "admin-form",
    loadChildren: () => import("./components/admin/admins/admin-add-edit-form/admin-add-edit-form.module").then(
        g => g.AdminAddEditFormModule)
  },
  {
    path: "audit-log",
    loadChildren: () => import("./components/admin/admins//audit-log/audit-log-details/audit-log-details.module").then(
        a => a.AuditLogDetailsModule)
  },
  {
    path: "roles-permission",
    loadChildren: () => import("./components/admin/admins/roles-permissions/create-roles-permission/create-roles-permission.module").then(
        c => c.CreateRolesPermissionModule)
  },
  {
    path: "roles-permission",
    loadChildren: () => import("./components/admin/admins/roles-permissions/view-roles-permission/view-roles-permission.module").then(
        v => v.ViewRolesPermissionModule)
  },
  {
    path: "",
    loadChildren: () => import("./components/admin/setting/setting/setting.module").then(
        s => s.SettingModule)
  },
  {
    path: "",
    loadChildren: () => import("./components/admin/report/report/report.module").then(
        r => r.ReportModule)
  },
/** Admin & User Module Routes */

  {
    path: "case-management",
    loadChildren: () => import("./components/admin-users/case-management/case-mangement/case-mangement-list.module").then(
        c => c.CaseManagementListModule)
  },
  {
    path: "case-management-form",
    loadChildren: () => import("./components/admin-users/admin-users-create-edit/create-edit-form/create-edit-form.module").then(
        c => c.CreateEditFormModule)
  },
  {
    path: "case-management",
    loadChildren: () => import("./components/admin-users/case-management/case-management-view/case-management-view.module").then(
        c => c.CaseManagementViewModule)
  },
  {
    path: "case-management",
    loadChildren: () => import("./components/admin-users/case-management/case-management-info/case-management-info.module").then(
        c => c.CaseManagementInfoModule)
  },
  {
    path: "case-management",
    loadChildren: () => import("./components/admin-users/case-management/task-template/task-template.module").then(
        t => t.TaskTemplateModule)
  },
  {
    path: "policies-legislation",
    loadChildren: () => import("./components/admin-users/policies-legislation/policies-legislation-list/policies-legislation-list.module").then(
        p => p.PoliciesLegislationListModule)
  },
  {
    path: "policies-form",
    loadChildren: () => import("./components/admin-users/policies-legislation/policies-add-edit-form/policies-add-edit-form.module").then(
        c => c.PoliciesAddEditFormModule)
  },
  {
    path: "policies",
    loadChildren: () => import("./components/admin-users/policies-legislation/view-policies/view-policies.module").then(
        v =>v.ViewPoliciesModule)
  },

  {
    path: "skills",
    loadChildren: () => import("./components/admin-users/skills/skills-list/skills-list.module").then(
        s => s.SkillsListModule)
  },
  {
    path: "skills",
    loadChildren: () => import("./components/admin-users/skills/view-skills/view-skills.module").then(
        v => v.ViewSkillsModule)
  },
  {
    path: "rss-feeds",
    loadChildren: () => import("./components/admin-users/research/rss-feeds/rss-feeds-list/rss-feeds-list.module").then(
        b => b.RssFeedsListModule)
  },
  {
    path: "rss-feeds",
    loadChildren: () => import("./components/admin-users/research/rss-feeds/rss-feeds-details/rss-feeds-details.module").then(
        r => r.RssFeedsDetailsModule)
  },
  {
    path: "rss-feeds",
    loadChildren: () => import("./components/admin-users/research/rss-feeds/search-rss-feeds-list/search-rss-feeds-list.module").then(
        s => s.SearchRssFeedsListModule)
  },
  {
    path: "rss-feeds-form",
    loadChildren: () => import("./components/admin-users/research/rss-feeds/rss-feeds-add-edit-form/rss-feedss-add-edit-form.module").then(
        b => b.RssFeedsAddEditFormModule)
  },
  {
    path: "blog",
    loadChildren: () => import("./components/admin-users/research/blog/blog-list/blog-list.module").then(
        b => b.BlogListModule)
  },
  {
    path: "blog-form",
    loadChildren: () => import("./components/admin-users/research/blog/blog-add-edit-form/blog-add-edit-form.module").then(
        c => c.BlogAddEditFormModule)
  },
  {
    path: "profile",
    loadChildren: () => import("./components/admin-users/my-profile/my-profile/my-profile.module").then(
        m => m.MyProfileModule)
  },
  {
    path: "case-management",
    loadChildren: () => import("./components/admin-users/case-management/public-incident-view/public-incident-view.module").then(
        p => p.PublicIncidentViewModule)
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes,{ preloadingStrategy: NoPreloading, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
