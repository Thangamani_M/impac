import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import { AuthenticationService, ResponseMessageTypes, SessionExpiryService, SnackbarService,SecretKey } from 'src/app/services';
import * as CryptoJS from 'crypto-js';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  hide:boolean = true;
  loginForm: FormGroup;
  isLoading:boolean = false;
  
  constructor(private fb: FormBuilder, private snackbarService: SnackbarService,private router: Router,private authenticationService:AuthenticationService,private sessionExpiryService: SessionExpiryService) { }

  ngOnInit(): void {
    this.createLoginForm();
  }

  /** Login Form Fields */
  createLoginForm() {
    this.loginForm = this.fb.group({
      emailAddress: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

/** Login Function KeyboardEvent */
  handleSubmit(event : any){
    event.preventDefault();
  }

  handleKeyUp(event : any){
    if(event.keyCode === 13){
        this.handleSubmit(event);
        this.login();
    }
  }

  login(){
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    this.isLoading = true;
    let secret = SecretKey.SECRET_KEY;
    let encrypted = CryptoJS.AES.encrypt(this.loginForm.value.password, secret);
    this.loginForm.value.password = encrypted.toString();
    this.authenticationService.login(this.loginForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      if(res?.status === 200){
        this.sessionExpiryService.reset();
        this.router.navigate(['case-management/case-mangement-list']);
        this.isLoading = false;
        this.snackbarService.openSnackbar(res.message, ResponseMessageTypes.SUCCESS);
      }
      }, error => {
        if(error?.error?.status === 401){
          this.isLoading = false;
          this.snackbarService.openSnackbar(error?.error?.message, ResponseMessageTypes.ERROR);
        }
        this.isLoading = false;
    })
  }

}
