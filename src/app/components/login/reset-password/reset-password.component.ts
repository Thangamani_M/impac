import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { AuthenticationService, ResponseMessageTypes, ResponseMessage, SnackbarService,SecretKey } from 'src/app/services';
import * as CryptoJS from 'crypto-js';
import { ConfirmedValidator } from '../confirmed.validator';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {
  hideOld:boolean = true;
  hideNew:boolean = true;
  hideConfirm:boolean = true;
  resetPasswordForm:FormGroup;
  userEmailAddress:string;
  otpVerificationForm:FormGroup;
  showOTPVerification:boolean = true;
  showResetPassword:boolean = false;
  isOTPLoading:boolean = false;
  isResetLoading:boolean = false;

  constructor(private formBuilder: FormBuilder,private snackbarService: SnackbarService, private authenticationService:AuthenticationService,private router: Router,private activeRoute: ActivatedRoute) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.userEmailAddress = params?.email;
    })
   }
  ngOnInit(): void {
    this.createOTPVerificationForm();
  }

  createOTPVerificationForm() {
    this.otpVerificationForm = this.formBuilder.group({
      emailAddress: [{value:this.userEmailAddress, disabled: true}, Validators.required],
      otp: ['', Validators.required]
    })
  }
  
  createResetPasswordForm() {
    this.resetPasswordForm = this.formBuilder.group({
      emailAddress: [this.otpVerificationForm.value.emailAddress],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    },{
      validator: ConfirmedValidator('password', 'confirmPassword')
    })
  }

  confirmOTP(){
    if (this.otpVerificationForm.invalid) {
      this.otpVerificationForm.markAllAsTouched();
      return;
    }
    this.isOTPLoading = true;
    this.otpVerificationForm.value.emailAddress = this.userEmailAddress;
    this.authenticationService.otpVerification(this.otpVerificationForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      if(res.status == true){
        this.showOTPVerification = false;
        this.showResetPassword = true;
        this.createResetPasswordForm();
        this.isOTPLoading = false;
        this.snackbarService.openSnackbar(ResponseMessage.OTP_CREATE, ResponseMessageTypes.SUCCESS);
      }
    }, error => {
      this.isOTPLoading = false;
    })
  }

  resetPassword(){
    if (this.resetPasswordForm.invalid) {
      this.resetPasswordForm.markAllAsTouched();
      return;
    }
    this.isResetLoading = true;
    let secret = SecretKey.SECRET_KEY;
    let passwordEncrypted = CryptoJS.AES.encrypt(this.resetPasswordForm.value.password, secret);
    this.resetPasswordForm.value.password = passwordEncrypted.toString();
    delete this.resetPasswordForm.value['confirmPassword'];

    this.authenticationService.changePassword(this.resetPasswordForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      if(res?.id != null){
         this.router.navigate(['login']);
         this.isResetLoading = false;
        this.snackbarService.openSnackbar(ResponseMessage.PASSWORD_CHANGED, ResponseMessageTypes.SUCCESS);
      }
      }, error => {
        this.isResetLoading = false;
    })
  }
}