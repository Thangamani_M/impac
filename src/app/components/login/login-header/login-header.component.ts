import { Component, OnInit } from '@angular/core';
import { logoURL } from 'src/app/services';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-login-header',
  templateUrl: './login-header.component.html',
  styleUrls: ['./login-header.component.scss']
})
export class LoginHeaderComponent implements OnInit {

  showLoginBtn:boolean = false;
  currentPageUrl: string;

  constructor(private router: Router, private activeRoute: ActivatedRoute,) {
    this.activeRoute.queryParams.subscribe((params: any) => {
        this.currentPageUrl = this.router.url;
        this.showLoginBtn = true && this.currentPageUrl != '/login' && this.currentPageUrl != '/forgot-password' && !params.email;
    })
   }

  ngOnInit(): void {
  }

  logoRedirect(){
    window.open(logoURL?.url);
  }
  
}
