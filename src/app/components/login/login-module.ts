import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Custom Module */
import { LoginRoutingModule } from './login-routing-module';
import { SharedModule } from '../shared/shared.module';

/** Custom Components */
import { LoginHeaderComponent } from './login-header/login-header.component';
import { LoginFooterComponent } from './login-footer/login-footer.component';
import { LoginMenupageComponent } from './login-menupage/login-menupage.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
  declarations: [LoginHeaderComponent, LoginFooterComponent, LoginComponent, ForgotPasswordComponent, ResetPasswordComponent, LoginMenupageComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule
  ]
})
export class LoginModule { }
