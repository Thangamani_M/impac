import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/** Custom Component */
import { LoginHeaderComponent } from './login-header/login-header.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

/** Login Routes */
const routes: Routes = [
  { path: 'home', component: LoginHeaderComponent },
  { path: 'about', component: LoginHeaderComponent },
  { path: 'contact', component: LoginHeaderComponent },
  { path: 'get-involved', component: LoginHeaderComponent },
  { path: 'legal', component: LoginHeaderComponent },
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'password-reset', component: ResetPasswordComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
