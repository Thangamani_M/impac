import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login-menupage',
  templateUrl: './login-menupage.component.html',
  styleUrls: ['./login-menupage.component.scss']
})
export class LoginMenupageComponent implements OnInit {

  currentPageUrl : string;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.currentPageUrl = this.router.url;
  }

}
