import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import { AuthenticationService, ResponseMessageTypes, ResponseMessage, SnackbarService,SecretKey } from 'src/app/services';
import * as CryptoJS from 'crypto-js';
import { ConfirmedValidator } from '../confirmed.validator';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit {

  hideNew:boolean = true;
  hideConfirm:boolean = true;
  forgotPasswordForm:FormGroup;
  otpVerificationForm:FormGroup;
  emailForm:FormGroup;

  showEmailSubmitBtn:boolean = true;
  showOTPVerification:boolean = false;
  showResetPassword:boolean = false;
  isEmailLoading:boolean = false;
  isOTPLoading:boolean = false;
  isResetLoading:boolean = false;

  constructor(private formBuilder: FormBuilder,private snackbarService: SnackbarService, private authenticationService:AuthenticationService,private router: Router) { }
  ngOnInit(): void {
    this.createEmailForm();
  }

  createEmailForm(){
    this.emailForm = this.formBuilder.group({
      emailAddress: ['', Validators.required],
    })
  }

  createOtpVerificationForm(){
    this.otpVerificationForm = this.formBuilder.group({
      emailAddress: [this.emailForm.value.emailAddress],
      otp: ['', Validators.required]
    })
  }

  createForgotPasswordForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      emailAddress: [this.emailForm.value.emailAddress],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    },{
      validator: ConfirmedValidator('password', 'confirmPassword')
    })
  }

  resendOTP(){
    if (this.emailForm.invalid) {
      this.emailForm.markAllAsTouched();
      return;
    }
    this.isEmailLoading = true;
    this.authenticationService.resendOTP(this.emailForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      if(res.status == true){
        this.showOTPVerification = true;
        this.showResetPassword = false;
        this.showEmailSubmitBtn = false;
        this.emailForm.disable();
        this.createOtpVerificationForm();
        this.isEmailLoading = false;
        this.snackbarService.openSnackbar(ResponseMessage.OTP_SENT, ResponseMessageTypes.SUCCESS);
      }
    }, error => {
      this.isEmailLoading = false;
    })
  }

  confirmOTP(){
    if (this.otpVerificationForm.invalid) {
      this.otpVerificationForm.markAllAsTouched();
      return;
    }
    this.isOTPLoading = true;
    this.authenticationService.otpVerification(this.otpVerificationForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      if(res.status == true){
        this.showOTPVerification = false;
        this.showResetPassword = true;
        this.createForgotPasswordForm();
        this.isOTPLoading = false;
        this.snackbarService.openSnackbar(ResponseMessage.OTP_CREATE, ResponseMessageTypes.SUCCESS);
      }
    }, error => {
      this.isOTPLoading = false;
    })
  }

  forgotPassword(){
    if (this.forgotPasswordForm.invalid) {
      this.forgotPasswordForm.markAllAsTouched();
      return;
    }
    this.isResetLoading = true;
    let secret = SecretKey.SECRET_KEY;
    let passwordEncrypted = CryptoJS.AES.encrypt(this.forgotPasswordForm.value.password, secret);
    this.forgotPasswordForm.value.password = passwordEncrypted.toString();
    delete this.forgotPasswordForm.value['confirmPassword'];
    this.authenticationService.forgotPassword(this.forgotPasswordForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      if(res?.id != null){
        this.router.navigate(['login']);
        this.isResetLoading = false;
        this.snackbarService.openSnackbar(ResponseMessage.PASSWORD_CHANGED, ResponseMessageTypes.SUCCESS);
      }
      }, error => {
        this.isResetLoading = false;
    })
  }
}