import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DialogComponent, DeleteComponent } from 'src/app/components/shared';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels,ResponseMessageTypes, SnackbarService, CommonService, ResponseMessage, configurationTabList, defaultSortingBy,menuNameList} from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../../../environments/environment';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-configuration-lists',
  templateUrl: './configuration-lists.component.html',
  encapsulation: ViewEncapsulation.Emulated
})
export class ConfigurationListsComponent implements OnInit {

  pageSize: number = 10;
  pageNo: number = 0;
  defaultSortby = defaultSortingBy;
  labelNames: Array<any> = configurationTabList;
  selectedIndex:number = 0;
  displayedColumns: Array<any> = [];
  displayedColumnswidth: Array<any> = [];
  dataSource = new MatTableDataSource<any>([]);
  entityTypeID: number;
  entityID: number;
  selectedRow: any;
  deletedIds: Array<any> = [];
  searchKey = '';
  sorting:any;
  pageEvent: any;
  customTableColumnSize: number;
  menuName:string = menuNameList?.CONFIGURATION;

  defaultSelectDisplayedColumns: string[] = ['select'];
  defaultActionDisplayedColumns: string[] = ['Action', 'search'];

  constructor(private router: Router,private snackbarService: SnackbarService, private activeRoute: ActivatedRoute, private crudService: CrudService,public dialog: MatDialog, private commonService: CommonService) {
    this.activeRoute.queryParamMap.subscribe((params: any) => {
      this.selectedIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    })
  }

  ngOnInit(): void {
    this.onSelectedTab(this.selectedIndex);
  }

  //Selected Tab
  onSelectedTab(event: any) {
    this.selectedIndex = event;
    this.searchKey = '';
    this.displayedColumns = [];
    this.dataSource = new MatTableDataSource<any>([]);
    this.selectedRow != undefined ? this.selectedRow.clear() : this.selectedRow;
    this.router.navigate([`../configuration-list`], { relativeTo: this.activeRoute, queryParams: { tab: this.selectedIndex } });
    this.getDisplayedColumns();
    this.commonService.setbreadcrumb([{ title: this.labelNames[this.selectedIndex]?.name }]);
  }

  getDisplayedColumns() {
    this.displayedColumnswidth = [];
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (response.length > 0) {
        this.customTableColumnSize = this.commonService.setTableColumnSize(response.length);
        response?.map((columnNames: any) => {
          this.displayedColumns.push(columnNames.entityTypeAttributeName);
          if (columnNames.entityTypeAttributeName == "Description" && this.labelNames[this.selectedIndex].name == configurationTabList[0]?.name) {
            this.displayedColumnswidth.push('0 0 50% !important');
          } 
         else if (columnNames.entityTypeAttributeName == "Description" && this.labelNames[this.selectedIndex].name == configurationTabList[1]?.name || columnNames.entityTypeAttributeName == "Description" && this.labelNames[this.selectedIndex].name == configurationTabList[2]?.name) {
            this.displayedColumnswidth.push('0 0 40% !important');
          }
          else {
            this.displayedColumnswidth.push('0 0 20% !important');
          }
        })
        this.displayedColumns = this.defaultSelectDisplayedColumns.concat(this.displayedColumns);
        this.displayedColumns = this.displayedColumns.concat(this.defaultActionDisplayedColumns);
        this.getDataSource();
      }
    })
  }

  // Data Source
  getDataSource(event?: any, searchKey?: any, sortBy?: any) {
    this.pageEvent = event;
    if (this.pageEvent) {
      let { pageSize, pageIndex, length } = this.pageEvent['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else {
      this.pageNo = 0;
    }
    searchKey = searchKey ? searchKey : '';
    sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;

    this.crudService.get_params(`${appModels.ENTITY_TYPE +`/${this.labelNames[this.selectedIndex].dynamicAPIUrl}`}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, searchKey: searchKey } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.dataSource = new MatTableDataSource(response);
      this.commonService.setLoaderShownProperty(false);
    })
  }
  // Searched
  onSearched(event: any) {
    this.searchKey = event;
    this.getDataSource(this.pageEvent, this.searchKey);
  }
  // Sorted
  onSorted(eve: any) {
    this.sorting = eve;
    if (this.sorting.direction == 'asc' || this.sorting.direction == 'desc') {
      this.getDataSource(this.pageEvent, this.searchKey, this.sorting);
    }
  }
  // Page Changed
  onPageChanged(event: any) {
    this.pageEvent = event;
    this.getDataSource(event, this.searchKey);
  }
  // Create Edit Clicked
  /** Dialog Model */
  onCreateEditClicked(values?: any) {
    if (this.selectedIndex == 0 || this.selectedIndex == 3) {
      if (values) {
        const tabName = 'Edit ' + this.labelNames[this.selectedIndex].name;
        this.entityID = values.EntityId;
        this.crudService.get_params(`${appModels.ENTITIES+`/${this.labelNames[this.selectedIndex].dynamicAPIUrl}`}/edit`, {
          params: {
            entityTypeId: this.entityTypeID, entityId: this.entityID
          }
        }).pipe(untilDestroyed(this)).subscribe((response: any) => {
          this.openCreateEditDialog(response, tabName, appModels.ATTRIBUTE_VALUE +`/${this.labelNames[this.selectedIndex].dynamicAPIUrl}`, true, this.entityID,this.labelNames[this.selectedIndex].value);
          this.commonService.setLoaderShownProperty(false);
        })
      }
      else {
        const tabName = 'Create ' + this.labelNames[this.selectedIndex].name;
        this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPES}/${this.entityTypeID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
          this.openCreateEditDialog(response, tabName, appModels.ENTITIES + `/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.CREATE}`);
          this.commonService.setLoaderShownProperty(false);
        })
      }
    }
    /** New Page */
    else {
      if (values) {
        this.entityID = values.EntityId;
        const methodName = 'Edit ' + this.labelNames[this.selectedIndex].name;
        this.router.navigate(['configuration-form/edit-form'], { queryParams: { entityID: this.entityID, entityTypeID: this.entityTypeID, pagetitle :this.labelNames[this.selectedIndex].name,menuName:this.menuName, methodName: methodName, selectedIndex: this.selectedIndex, dynamicApiUrl:this.labelNames[this.selectedIndex].dynamicAPIUrl, isEdit: true }, skipLocationChange: environment.SkipLocationChange });
      }
      else {
        const methodName = 'Create ' + this.labelNames[this.selectedIndex].name;
        this.router.navigate(['configuration-form/create-form'], { queryParams: { entityTypeID: this.entityTypeID, pagetitle: this.labelNames[this.selectedIndex].name, methodName: methodName, menuName:this.menuName, selectedIndex: this.selectedIndex,dynamicApiUrl: this.labelNames[this.selectedIndex].dynamicAPIUrl }, skipLocationChange: environment.SkipLocationChange });
      }
    }
  }
  // Model Dialog
  openCreateEditDialog(response: any, header: any, api: any, isEdit = false, entityId?: any, entityTypeId?: any) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '50vw',
      height: 'auto',
      data: {
        result: response,
        headers: header,
        api: api,
        edit: isEdit,
        entityId: entityId,
        entityTypeID: entityTypeId
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.getDataSource();
      }
    });
  }
  // Selected
  onSelected(event: any) {
    this.deletedIds = []
    this.selectedRow = event;
    this.selectedRow?._selected?.map((element: any) => {
      this.deletedIds.push({ id: element.EntityId });
    });
  }

  // Delete POP UP
  openDeleteDialog() {
    if (this.deletedIds == undefined || this.deletedIds.length == 0) {
      this.snackbarService.openSnackbar(ResponseMessage.SELECT_ITEM, ResponseMessageTypes.WARNING);
      return;
    }
    const moduleName = this.labelNames[this.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: this.selectedRow,
        id: this.deletedIds,
        moduleName: moduleName,
        api: `${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.DELETEINBATCH}`,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        data.clear();
        this.getDataSource();
        this.deletedIds = [];
      }
    });
  }
  
  exportFile(entityname:any,entityId:any){
    let selectedRecordIds: any = [];
    if(this.selectedRow?._selected?.length > 0){
      this.selectedRow != undefined && this.selectedRow?._selected?.forEach((res:any) =>{
        selectedRecordIds.push(res.EntityId);
      })
      this.commonService.onFileDownload(entityname,entityId,selectedRecordIds);
    }
    else{
      this.commonService.onFileDownload(entityname,entityId);
    }
  }
}
