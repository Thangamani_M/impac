import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/** Custom Component,Service */
import { Shared } from "src/app/services";
import { ConfigurationListsComponent } from './configuration-lists.component';

const routes: Routes = [
  Shared.childRoutes([
    { path: 'configuration-list', component: ConfigurationListsComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationListsRoutingModule { }
