import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationListsRoutingModule } from './configuration-lists-routing.module';

/** Custom Component,Service */
import { SharedModule } from 'src/app/components/shared/shared.module';
import { ConfigurationListsComponent } from './configuration-lists.component';

@NgModule({
  declarations: [ConfigurationListsComponent],
  imports: [
    CommonModule,
    ConfigurationListsRoutingModule,
    SharedModule
  ]
})
export class ConfigurationListsModule { }
