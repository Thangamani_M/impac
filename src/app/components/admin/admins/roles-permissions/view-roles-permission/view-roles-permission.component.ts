import { Component, OnInit } from '@angular/core';
import { CrudService, appModels, CommonService, ResponseMessageTypes, ResponseMessage, SnackbarService, DynamicAPIEndPoints } from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";
import { Router, ActivatedRoute } from '@angular/router';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-view-roles-permission',
  templateUrl: './view-roles-permission.component.html'
})
export class ViewRolesPermissionComponent implements OnInit {
  selectedIndex: number = 3;
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns:string[] = ['displayName', 'create', 'read', 'update', 'delete'];
  updatePermissionMenu: Array<any> = [];
  rolesResult: any;
  menuPermission = {};
  dynamicAPIUrl:string = DynamicAPIEndPoints?.ROLE_MENU_URL;
  currentRouterValues: any;
  changedActionvalue:any = [];

  constructor(private snackbarService: SnackbarService, private router: Router, private crudService: CrudService, private commonService: CommonService, private activeRoute: ActivatedRoute) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.getRolesPermissionByRoleID();
  }
  //Roles Permission By Role ID
  getRolesPermissionByRoleID() {
    this.crudService.get_path(`${appModels.ROLE_MENU}/${this.dynamicAPIUrl}/${appModels.PERMISSION_BY_ID}/${this.currentRouterValues?.roleId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.rolesResult = response;
      this.rolesResult.menuPermissionDto = this.rolesResult?.menuPermissionDto?.map((items:any) =>{
        if(items?.operation == 'CRUD'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: items?.menuAction?.update, delete: items?.menuAction?.delete, create: items?.menuAction?.create}})
        }
        if(items?.operation == 'CRD'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: '--', delete: items?.menuAction?.delete, create: items?.menuAction?.create}})
        }
        if(items?.operation == 'CRU'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: items?.menuAction?.update, delete: '--', create: items?.menuAction?.create}})
        }
        if(items?.operation == 'CR'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: '--', delete: '--', create: items?.menuAction?.create}})
        }
        if(items?.operation == 'RU'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: items?.menuAction?.update, delete: '--', create: '--'}})
        }
         if(items?.operation == 'R'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: '--', delete: '--', create: '--'}})
        }
      }
     );
      this.dataSource = new MatTableDataSource<any>(response.menuPermissionDto);
      this.commonService.setLoaderShownProperty(false);
    })
  }
  //Permission Change
  onChangePermission(actionValue: any, event: any, actionName: any) {
    this.changedActionvalue.push(actionValue);
    Object.keys(actionValue.menuAction).map((result: any) => {
      if (actionName == result) {
        actionValue.menuAction[actionName] = event.checked;
      }
    })
    this.updatePermissionMenu.push(actionValue);
  }
  //update Role
  updateRolePermission() {
      if (this.updatePermissionMenu?.length > 0) {
        this.changedActionvalue?.forEach((res:any)=>{
          Object.keys(res?.menuAction).map((result: any) => {
            if(res.menuAction[result] == '--'){
              res.menuAction[result] = false;
            }
        })
        })

      this.menuPermission = {
        "roleName": this.rolesResult.roleName,
        "roleId": this.rolesResult.roleId,
        "menuPermissionDto": this.updatePermissionMenu
      }
      this.crudService.update(`${appModels.ROLE_MENU}/${this.dynamicAPIUrl}`, this.menuPermission)
        .pipe(untilDestroyed(this)).subscribe(res => {
          this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
          this.cancel();
        })
    }
    else {
      this.snackbarService.openSnackbar(ResponseMessage.NO_CHANGES, ResponseMessageTypes.WARNING);
      return;
    }
  }
  // Cancel
  cancel() {
    this.router.navigate([`../admin/admin-list`], { queryParams: { tab: this.selectedIndex } });
  }

}
