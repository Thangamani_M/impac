import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewRolesPermissionComponent } from './view-roles-permission.component'
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'view-roles-permission', component: ViewRolesPermissionComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRolesPermissionRoutingModule { }
