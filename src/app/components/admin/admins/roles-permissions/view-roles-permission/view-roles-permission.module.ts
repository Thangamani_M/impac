import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewRolesPermissionRoutingModule } from './view-roles-permission-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';

import { ViewRolesPermissionComponent } from './view-roles-permission.component';

@NgModule({
  declarations: [ViewRolesPermissionComponent],
  imports: [
    CommonModule,
    ViewRolesPermissionRoutingModule,
    SharedModule
  ]
})
export class ViewRolesPermissionModule { }
