import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRolesPermissionRoutingModule } from './create-roles-permission-routing.module';

import { SharedModule } from 'src/app/components/shared/shared.module';
import { CreateRolesPermissionComponent } from './create-roles-permission.component';

@NgModule({
  declarations: [CreateRolesPermissionComponent],
  imports: [
    CommonModule,
    CreateRolesPermissionRoutingModule,
    SharedModule
  ]
})
export class CreateRolesPermissionModule { }
