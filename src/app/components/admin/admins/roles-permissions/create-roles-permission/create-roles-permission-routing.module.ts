import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateRolesPermissionComponent } from './create-roles-permission.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'create-roles-permission', component: CreateRolesPermissionComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateRolesPermissionRoutingModule { }
