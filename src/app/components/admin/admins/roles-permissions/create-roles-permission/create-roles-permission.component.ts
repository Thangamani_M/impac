import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { CrudService,appModels,CommonService, ResponseMessageTypes,ResponseMessage, SnackbarService, DynamicAPIEndPoints} from 'src/app/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from "@angular/material/table";

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-create-roles-permission',
  templateUrl: './create-roles-permission.component.html',
  styleUrls: ['./create-roles-permission.component.scss']
})
export class CreateRolesPermissionComponent implements OnInit {

  roleForm:FormGroup = new FormGroup({});
  showPermission:Boolean = false;
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string []= ['displayName','create','read','update','delete'];
  permissionResult: any;
  dynamicAPIUrl: string = DynamicAPIEndPoints?.ROLE_MENU_URL;
  dynamicRoleAPIUrl:string = DynamicAPIEndPoints?.ROLE_URL;
  currentRouterValues: any;

  constructor(private fb: FormBuilder,private snackbarService: SnackbarService,private router: Router,private crudService: CrudService,private commonService: CommonService,private activeRoute: ActivatedRoute) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.createRoleForm();
  }
// Create Role Form
  createRoleForm() {
    this.roleForm = this.fb.group({
      roleName: ['', Validators.required],
      roleDescription:['', Validators.required],
      entityTypeId:[this.currentRouterValues?.entityTypeID]
    })
  }
    // key press
  onkeypress(e: any) {
    this.commonService.onkeyUpTextbox(e);
  }
  onKeyuptextarea(val: any) {
    this.commonService.onKeyUpTextarea(val);
  }
//Create Role
  createRole(){
    if (this.roleForm.invalid) {
      this.roleForm.markAllAsTouched();
      this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
      return;
    }
    this.crudService.post(`${appModels.ROLE}/${this.dynamicRoleAPIUrl}`,this.roleForm.value)
    .pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.permissionResult = res;
      this.permissionResult.menuPermissionDto = this.permissionResult?.menuPermissionDto?.map((items:any) =>{
        if(items?.operation == 'CRUD'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: items?.menuAction?.update, delete: items?.menuAction?.delete, create: items?.menuAction?.create}})
        }
        if(items?.operation == 'CRD'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: '--', delete: items?.menuAction?.delete, create: items?.menuAction?.create}})
        }
        if(items?.operation == 'CRU'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: items?.menuAction?.update, delete: '--', create: items?.menuAction?.create}})
        }
        if(items?.operation == 'CR'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: '--', delete: '--', create: items?.menuAction?.create}})
        }
        if(items?.operation == 'RU'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: items?.menuAction?.update, delete: '--', create: '--'}})
        }
         if(items?.operation == 'R'){
          return Object.assign({},items,{ menuAction: {read: items?.menuAction?.read, update: '--', delete: '--', create: '--'}})
        }
      }
     );
      this.dataSource = new MatTableDataSource<any>(this.permissionResult.menuPermissionDto);
      this.showPermission = true;
      this.commonService.setLoaderShownProperty(false);
    })
  }
 
//Permission Change
  onChangePermission(actionValue:any,event:any,actionName:any){
    Object.keys(actionValue.menuAction).map((result:any) =>{
     if(actionName == result){
       actionValue.menuAction[actionName] = event.checked;
     }
    })
   }
//create Permission
  createPermission(value?:any){
    if(this.permissionResult?.length == 0 || value){
      this.cancelRolePermission();    
    }
    else{
      this.permissionResult?.menuPermissionDto?.forEach((result:any) =>{
        Object.keys(result.menuAction).map((item: any) => {
          if(result.menuAction[item] == '--'){
            result.menuAction[item] = false;
          }
        })
      })
      this.crudService.post(`${appModels.ROLE_MENU}/${this.dynamicAPIUrl}`,this.permissionResult)
      .pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.commonService.setLoaderShownProperty(false);
        if(!value){
          this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
        }
        this.cancelRolePermission();
      })
    }
  }

//Cancel
  cancel(){
    this.createPermission("cancel");
  }

  cancelRolePermission(){
    this.router.navigate([`../admin/admin-list`], { queryParams: { tab: this.currentRouterValues?.selectedIndex } });
  }
}
