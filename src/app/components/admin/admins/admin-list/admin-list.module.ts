import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminListRoutingModule } from './admin-list-routing.module';

/** Custom Component,Service */
import { SharedModule } from 'src/app/components/shared/shared.module';
import { AdminListComponent } from './admin-list.component';

@NgModule({
  declarations: [AdminListComponent],
  imports: [
    CommonModule,
    AdminListRoutingModule,
    SharedModule
  ]
})
export class AdminListModule { }
