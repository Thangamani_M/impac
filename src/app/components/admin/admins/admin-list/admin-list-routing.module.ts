import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { AdminListComponent } from './admin-list.component';

const routes: Routes = [
  Shared.childRoutes([
    { path: 'admin-list', component: AdminListComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminListRoutingModule { }
