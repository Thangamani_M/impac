import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DeleteComponent } from 'src/app/components/shared';
import { CrudService, appModels, ResponseMessageTypes, SnackbarService, CommonService, ResponseMessage, AttributeType, adminTabList, defaultSortingBy,menuNameList} from 'src/app/services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../../../environments/environment';
import { forkJoin } from 'rxjs';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated

})
export class AdminListComponent implements OnInit {

  pageSize:number = 10;
  pageNo:number = 0;
  defaultSortby = defaultSortingBy;
  labelNames:Array<any> = adminTabList;
  selectedIndex:number = 0;
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource<any>([]);
  entityTypeID: number;
  entityID: number;
  selectedRow:any;
  deletedIds: Array<any> = [];
  searchKey = '';
  isToggle: boolean = true;
  sorting: any;
  pageEvent: any;
  customTableColumnSize: number;
  displayedColumnswidth: Array<any> = [];
  recordNameList: Array<any>= [];
  selectedRecordName:Array<any> = [];
  selectedRecordId: number;
  recordForm: FormGroup = new FormGroup({});
  sortby = {
    active: 'Created On',
    direction: 'desc'
  };
  selectedEntityTypeId: number;
  defaultSelectDisplayedColumns: string[] = ['select'];
  defaultActionDisplayedColumns: string[] = ['Action', 'search'];
  menuName: string = menuNameList?.ADMIN;
  attributeTypeValue = AttributeType;
  recordAllObj = [{componentValue: "All",entityTypeId: 0}];
  allRecordList:any = [];

  constructor(private router: Router,private snackbarService: SnackbarService, private activeRoute: ActivatedRoute, private crudService: CrudService,public dialog: MatDialog, private formBuilder: FormBuilder, private commonService : CommonService) {
    this.activeRoute.queryParamMap.subscribe((params: any) => {
      this.selectedIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    })
  }

  ngOnInit(): void {
    this.onSelectedTab(this.selectedIndex);
    this.auditRecordForm();
  }

  auditRecordForm() {
    this.recordForm = this.formBuilder.group({
      recordListName: [''],
    })
  }

  onSelectedTab(event: any) {
    this.selectedIndex = event;
    this.displayedColumns = [];
    this.searchKey = '';
    this.dataSource = new MatTableDataSource<any>([]);
    this.selectedRow != undefined ? this.selectedRow.clear() : this.selectedRow;
    this.router.navigate([`../admin-list`], { relativeTo: this.activeRoute, queryParams: { tab: this.selectedIndex } });
    this.getDisplayedColumns();
    this.commonService.setbreadcrumb([{ title: this.labelNames[this.selectedIndex]?.name }]);
  }

  getDisplayedColumns() {
    this.displayedColumnswidth = [];
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (this.selectedIndex == 2) {
        if (response.length > 0) {
          this.customTableColumnSize = this.commonService.setTableColumnSize(response.length);
          let dropdownAPI: any = [];
          response?.map((columnNames: any) => {
            this.displayedColumns.push(columnNames.entityTypeAttributeName);
              if (columnNames.componentName == this.attributeTypeValue?.DROP_DOWN) {
                dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${columnNames.entityTypeAttributeId}`));
              }
          })
          this.getRecordNames(dropdownAPI);
        }
      }
      else if (this.selectedIndex == 3) {
        if (response.length > 0) {
          response?.map((columnNames: any) => {
            this.displayedColumns.push(columnNames.entityTypeAttributeName);
          })
          this.getDataSource();
        }
      }
      else {
        if (response.length > 0) {
          response?.map((columnNames: any) => {
            this.displayedColumns.push(columnNames.entityTypeAttributeName);
              this.displayedColumnswidth.push('0 0 20% !important');
          })
          this.displayedColumns = this.defaultSelectDisplayedColumns.concat(this.displayedColumns);
          this.displayedColumns = this.displayedColumns.concat(this.defaultActionDisplayedColumns);
          this.getDataSource();
        }
      }
    })
  }

  getDataSource(event?: any, searchKey?: any, sortBy?: any) {
    this.pageEvent = event;
    if (this.pageEvent) {
      let { pageSize, pageIndex, length } = this.pageEvent['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else {
      this.pageNo = 0;
    }
    searchKey = searchKey ? searchKey : '';
    sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;

    this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction:sortBy.direction,searchKey:searchKey} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.dataSource = new MatTableDataSource(response);
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onSearched(event: any) {
    this.searchKey = event;
    if (this.selectedIndex == 2) {
      this.onChangeRecordName(this.selectedEntityTypeId, this.searchKey);
    }
    else {
      this.getDataSource(this.pageEvent, this.searchKey)
    }
  }

  onSorted(eve: any) {
    this.sorting = eve;
    if (this.selectedIndex == 2) {
      if (this.sorting.direction == 'asc' || this.sorting.direction == 'desc') {
        this.onChangeRecordName(this.selectedEntityTypeId, this.searchKey, this.pageEvent, this.sorting);
      }
    }
    else {
      if (this.sorting.direction == 'asc' || this.sorting.direction == 'desc') {
        this.getDataSource(this.pageEvent, this.searchKey, this.sorting);
      }
    }
  }

  onPageChanged(event: any) {
    this.pageEvent = event;
    if (this.selectedIndex == 2) {
      this.onChangeRecordName(this.selectedEntityTypeId, this.searchKey, event)
    }
    else {
      this.getDataSource(event, this.searchKey);
    }
  }

  onCreateEditClicked(values?: any) {
    if (this.selectedIndex == 3) {
      this.router.navigate(['roles-permission/create-roles-permission'], { queryParams: { pagetitle: this.labelNames[this.selectedIndex].name, selectedIndex: this.selectedIndex, entityTypeID: this.labelNames[this.selectedIndex].value }, skipLocationChange: environment.SkipLocationChange });
    }
    else {
      if (values) {
        this.entityID = values.EntityId;
        const methodName = 'Edit '+this.labelNames[this.selectedIndex].name;
        this.router.navigate(['admin-form/edit-form'], {queryParams: { entityID: this.entityID, pagetitle :this.labelNames[this.selectedIndex].name,entityTypeID: this.entityTypeID,methodName:methodName,selectedIndex:this.selectedIndex,isEdit:true,menuName:this.menuName,dynamicApiUrl:this.labelNames[this.selectedIndex].dynamicAPIUrl},skipLocationChange:environment.SkipLocationChange});
      }
      else {
        const methodName = 'Create New ' + this.labelNames[this.selectedIndex].name;
        this.router.navigate(['admin-form/create-form'], { queryParams: { entityTypeID: this.entityTypeID, pagetitle: this.labelNames[this.selectedIndex].name, methodName: methodName, selectedIndex: this.selectedIndex, menuName: this.menuName, dynamicApiUrl: this.labelNames[this.selectedIndex].dynamicAPIUrl }, skipLocationChange: environment.SkipLocationChange });
      }
    }

  }

  // Delete POP UP
  onSelected(event: any) {
    this.deletedIds = []
    this.selectedRow = event;
    this.selectedRow._selected?.map((element: any) => {
      this.deletedIds.push({ id: element.EntityId });
    });
  }

  openDeleteDialog() {
    if (this.deletedIds == undefined || this.deletedIds.length == 0) {
      this.snackbarService.openSnackbar(ResponseMessage.SELECT_ITEM, ResponseMessageTypes.WARNING);
      return;
    }
    const moduleName = this.labelNames[this.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: this.selectedRow,
        id: this.deletedIds,
        moduleName: moduleName,
        api: `${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.DELETEINBATCH}`
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        data.clear();
        this.getDataSource();
        this.deletedIds = [];
      }
    });
  }

  /** Audit log */
  getRecordNames(Api: any) {
    forkJoin(Api).subscribe((results: any) => {
      this.recordNameList = results.flat(1);
      this.allRecordList = [...this.recordAllObj, ...this.recordNameList];
      this.selectedRecordId = this.allRecordList[0]?.entityTypeId;
        this.onChangeRecordName(this.selectedRecordId)
      this.commonService.setLoaderShownProperty(false);
    })
  }  
 
  onChangeRecordName(selectedEntityTypeId?: any,searchKey?: any, event?: any, sortBy?: any) {
    this.selectedEntityTypeId = selectedEntityTypeId;
    this.allRecordList.filter((i:any) => {
       if( this.selectedEntityTypeId  == i.entityTypeId){
         this.selectedRecordName = i?.componentValue;
       }});

    if (event) {
      let { pageSize, pageIndex, length } = event['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else {
      this.pageNo = 0;
    }
    searchKey = searchKey ? searchKey : '';
    sortBy == undefined ? sortBy = this.sortby : sortBy = sortBy;

    this.crudService.get_params(`${appModels.ENTITIES}/${this.selectedRecordName}/${appModels.PAGE}/${this.selectedEntityTypeId}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, searchKey: searchKey } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if(this.selectedEntityTypeId == 0){
        this.displayedColumns = ['Entity Name', 'Entity Type', 'Created On', 'Created By', 'Modified On', 'Modified By', 'search']
      }
      else{
        this.displayedColumns = ['Entity Name', 'Created On', 'Created By', 'Modified On', 'Modified By', 'search']
      }
      this.dataSource = new MatTableDataSource(response);
      this.commonService.setLoaderShownProperty(false);
    })
  }

  exportFile(entityname:any,entityId:any){
    let selectedRecordIds: any = [];
    if(this.selectedRow?._selected?.length > 0){
      this.selectedRow != undefined && this.selectedRow?._selected?.forEach((res:any) =>{
        selectedRecordIds.push(res.EntityId);
      })
      this.commonService.onFileDownload(entityname,entityId,selectedRecordIds);
    }
    else{
      this.commonService.onFileDownload(entityname,entityId);
    }
  }
}
