import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AuditLogDetailsComponent } from './audit-log-details.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'audit-log-details', component: AuditLogDetailsComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditLogDetailsRoutingModule { }
