import { Component, OnInit } from '@angular/core';
import { CrudService, appModels, CommonService, DynamicAPIEndPoints,menuNameList } from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";
import { Router, ActivatedRoute } from '@angular/router';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-audit-log-details',
  templateUrl: './audit-log-details.component.html',
  styleUrls: ['./audit-log-details.component.scss']
})
export class AuditLogDetailsComponent implements OnInit {
  clicked: number = 0;
  dataSourceDate : Array<any>;
  displayedColumnsDate: string[] = ['date'];
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['fields','selectedEvenInfo', 'selectedOddInfo'];
  selectedEvenDate: Array<any>;
  selectedOddDate: Array<any>;
  positionFlag : any ={};
  evenData:any = [];
  oddData:any = [];
  selectedEvenData: Array<any> = [];
  selectedOddData: Array<any> = [];
  dynamicAPIUrl = DynamicAPIEndPoints?.USER_MANAGEMENT_URL;
  currentRouterValues: any;

  constructor(private crudService: CrudService,private activeRoute: ActivatedRoute,private router: Router, private commonService: CommonService) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.getAuditDetailDate();
    let url = this.currentRouterValues?.menuName == menuNameList?.ADMIN ? '/admin/admin-list' : '';
    this.commonService.setbreadcrumb([{title: "Audit Logs", relativeRouterUrl: url, queryParams: { tab: 2 }}, {title:"Log Detail"}]);
  }

  getAuditDetailDate(){
    this.crudService.get_path(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/${appModels.AUDIT_DATE}/${this.currentRouterValues?.EntityID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.dataSourceDate = response;
      if(response[0]){
        this.positionFlag={isFirst: true,isLast:false}
        this.onChangeDate(response[0],0);
      }
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onChangeDate(event?: any, index?: any){
    this.positionFlag = this.dataSourceDate.length - 1 == index && { isFirst: false,isLast:true} || index == 0 && {isFirst: true,isLast:false} || {isFirst: false,isLast:false};
      if(index % 2 == 0){
        this.selectedEvenDate = event;
          this.getLogByDate(event,"even");
      }
      else if(Math.abs(index % 2) == 1){
        this.selectedOddDate = event;
        this.getLogByDate(event,"odd");
      }  
  }

 getLogByDate(event:any,dateType:any){
   this.oddData = [];
   this.evenData = [];
  this.crudService.get_params(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/${appModels.AUDIT_BY_ENTITY}/${this.currentRouterValues?.EntityID}`, { params: {entityTypeId:this.currentRouterValues?.EntityTypeId,auditDate:event,isFirst:this.positionFlag.isFirst,isLast:this.positionFlag.isLast} }).pipe(untilDestroyed(this)).subscribe((response: any) => {

    if(dateType == "even"){
      response.map((result:any,index:any) => {
        this.evenData.push({fieldName:result.entityTypeAttributeName,evenValues:result.attributeValue,oddValues:this.selectedOddData !== undefined ? this.selectedOddData[index]?.oddValues : ""})
      })
      this.selectedEvenData = this.evenData;
      this.dataSource = this.evenData;
    }
    else {
      response.map((result:any,index:any) => {
        this.oddData.push({fieldName:result.entityTypeAttributeName,evenValues:this.selectedEvenData[index].evenValues,oddValues:result.attributeValue})
      })
      this.selectedOddData = this.oddData;
      this.dataSource = this.oddData;
    }
    this.commonService.setLoaderShownProperty(false);
    })
 }

  cancel(){
    this.router.navigate([`../admin/admin-list`], { queryParams: { tab:2 } });
  }

}
