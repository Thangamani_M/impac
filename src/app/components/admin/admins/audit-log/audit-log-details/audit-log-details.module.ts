import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditLogDetailsRoutingModule } from './audit-log-details-routing.module';

/** Custom Component,Service */

import { SharedModule } from 'src/app/components/shared/shared.module';
import { AuditLogDetailsComponent } from './audit-log-details.component';

@NgModule({
  declarations: [AuditLogDetailsComponent],
  imports: [
    CommonModule,
    AuditLogDetailsRoutingModule,
    SharedModule
  ]
})
export class AuditLogDetailsModule { }
