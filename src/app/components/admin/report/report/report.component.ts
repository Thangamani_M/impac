import { Component, OnInit } from '@angular/core';
import { CrudService, appModels, ResponseMessageTypes,ResponseMessage,SnackbarService,CommonService, caseManagement_Info_TabList,menuNameList,AttributeType} from 'src/app/services';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { DatePipe } from '@angular/common';
import { HttpClient } from "@angular/common/http";
const EXCEL_EXTENSION = '.xlsx';
const PDF_EXTENSION = '.pdf';
import { saveAs } from 'file-saver';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  menuName:string = menuNameList?.REPORT;
  labelNames: Array<any> = caseManagement_Info_TabList;
  moduleName:any;
  currentLabelNames : any;
  currentModuleId : any;
  formResponse: any;
  attributeTypeValue = AttributeType;
  filterDisplayesColumns: any = [];
  filterForm: FormGroup = new FormGroup({});
  dropDownResponse: any = [];
  templateFormValues: any = [];
  entityAttributesValues: any = [];
  createFormValues: any = [];
  templateFormObj: any = [];
  startDate:any = new Date();
  endDate:any = new Date();
  isDefaultFilterDownload:boolean = false;
  isAdvancedFilterDownload:boolean = false;
  showAdvanceFilterForm:boolean = false;


  allChecked: boolean = true;
  headerlist:any = [];
  reportFormGroup : FormGroup;
  reports:any;
  checkAdvanceFilter:boolean = false;
  selectedReports:any = [];

  constructor(private formBuilder: FormBuilder,private crudService: CrudService, private snackbarService: SnackbarService, private commonService: CommonService,private datepipe: DatePipe,private http: HttpClient) { 
    this.commonService.setbreadcrumb([{title:this.menuName}]);
  }

  ngOnInit(): void {
    this.moduleName = this.labelNames[0].value;
    this.onChangeModule(this.moduleName)
    this.createReportFormGroup();
  }
  
  createReportFormGroup(){
    this.reportFormGroup = this.formBuilder.group({
      reports: this.formBuilder.array([])
    });

  }
  onChangeModule(value:any){
    this.showAdvanceFilterForm = false;
    this.checkAdvanceFilter = false;
    this.headerlist = [];
    this.currentModuleId = value;
     this.currentLabelNames = this.labelNames?.find((res) => res.value == this.currentModuleId);
     this.getDefaultFilterColumns();
     this.advanceFilter();
  }
  onChangeAdvanceFilter(val:any){
    this.checkAdvanceFilter = val;
      if(this.checkAdvanceFilter){
        this.showAdvanceFilterForm = true;
      }
      else{
        this.showAdvanceFilterForm = false;
      }
  }

  getDefaultFilterColumns(){
    let dropdownAPI: any = [];
    this.filterDisplayesColumns =[];
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.currentLabelNames.dynamicAPIUrl}/${appModels.FILTER_HEADERS}`, { params: { entityTypeId: this.currentModuleId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.formResponse = response;
      if (response.length > 0) {
        response?.map((columnNames: any) => {
          if (columnNames.componentName == this.attributeTypeValue?.DROP_DOWN || columnNames.componentName == this.attributeTypeValue?.DATE_TIME) {
            this.filterDisplayesColumns?.push(columnNames);
            if(columnNames.componentName == this.attributeTypeValue?.DROP_DOWN){
                dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentLabelNames.dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${columnNames.entityTypeAttributeId}`));
            }
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI);
        this.filterForm = this.createFormGroup();
      }
    })

  }

  filter(val:any){
    this.templateFormValues = [];
    this.entityAttributesValues = [];
    this.createFormValues = {
      "entityTypeId": "",
      "entityAttributes": "",
      "from":"",
      "to":"",
      "isPdf":""
    } 
    this.templateFormObj = [];
    Object.keys(this.filterForm.value).forEach((key) => {
      const currentControl = this.filterForm.controls[key];
      if (!currentControl.pristine && !currentControl.untouched) {
        this.templateFormObj.push({ [key]: this.filterForm.value[key] });
      }
    });
    this.formResponse?.forEach((res: any, i: number) => {
      this.templateFormObj?.forEach((forms: any) => {
        if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
          res.attributeValue = Object.values(forms).toString();
          this.templateFormValues.push(res);
        }
      })
    })

    this.templateFormValues?.map((res: any) => {
      if (res?.componentName == this.attributeTypeValue?.DATE_TIME) {
        res.attributeValue = this.datepipe.transform(res?.attributeValue, 'yyyy-MM-ddThh:mm:ss');
      }
      this.entityAttributesValues.push({ 'entityTypeAttributeId': res.entityTypeAttributeId, 'attributeValue': res.attributeValue });
    })
    this.createFormValues.entityAttributes = this.entityAttributesValues;
    this.startDate = this.datepipe.transform(this.startDate, 'yyyy-MM-dd');
    this.endDate = this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
    this.createFormValues.entityTypeId = this.currentModuleId;
    this.createFormValues.from = this.startDate;
    this.createFormValues.to = this.endDate;
    this.createFormValues.isPdf = val;
    if(val){
      this.isAdvancedFilterDownload = true;
      this.isDefaultFilterDownload = false;
    }
    else{
      this.isDefaultFilterDownload = true;
      this.isAdvancedFilterDownload = false;
    }
  }

    advanceFilter(){
      this.crudService.get(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.currentLabelNames.dynamicAPIUrl}/${appModels.ENTITY_TYPE_DROPDOWN}/${this.currentModuleId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
        response.forEach((result:any) =>{
          if(!result?.isKey && result?.componentName != AttributeType?.DATE_TIME){
            this.headerlist.push(result);
            this.headerlist =  this.headerlist.map((items:any) =>
              Object.assign({},items,{ selected: true}));
          }
        })
        this.setAll(this.allChecked);
        this.commonService.setLoaderShownProperty(false);
      })
    }
    // Create Form
    createFormGroup(): FormGroup {
      const group = this.formBuilder.group({});
      this.formResponse?.forEach((column: any) => {
        if (column.componentName == this.attributeTypeValue?.DROP_DOWN || column.componentName == this.attributeTypeValue?.DATE_TIME) {
          group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue));
        }
      });
      return group;
    }  

  //Load Multiple DropDown
  onLoadMultipleDropDown(Api: any) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownResponse = results.flat(1);
      this.commonService.setLoaderShownProperty(false);
    })
  }

resetdefaultFilter(){
  this.filterForm.reset();
}

isFewSelected(): boolean {
  return this.headerlist.filter((t:any) => t.selected).length > 0 && !this.allChecked;
}

setAll(selected: boolean) {
  this.allChecked = selected;
  if (this.headerlist == null) {
    return;
  }
  if(selected){
    const reports = <FormArray>this.reportFormGroup.get('reports') as FormArray;
    reports.clear();
    this.headerlist.forEach((t:any) =>{ t.selected = selected;
      reports.push(new FormControl(t.entityTypeAttributeName));
    });
  }
  else{
    this.reportFormGroup.value.reports = [];
    this.headerlist.forEach((t:any) =>t.selected = selected);
    this.selectedReports = [];
  }
}
  onChange(event:any) {
    const reports = <FormArray>this.reportFormGroup.get('reports') as FormArray;
    this.selectedReports.length == 0 && reports.clear();
    if(event.checked) {
      this.selectedReports.push(event.source.value)
      reports.push(new FormControl(event.source.value));
    } else {
      const i = reports.controls.findIndex(x => x.value === event.source.value);
      reports.removeAt(i);
    }
  }

  onChangeDownload(val:any){
    this.download(val);
  }

  
  download(type?:any){
    if(this.isDefaultFilterDownload){
      this.http.post(`${appModels.REPORT}/${this.currentLabelNames.dynamicAPIUrl}/${appModels.EXPORTPDF}/${this.currentModuleId}`, this.createFormValues, { responseType: 'blob' as 'blob'}).subscribe((res: any) => {
        const blob = new Blob([res], { type: 'application/octet-stream'});
        const responseData= window.URL.createObjectURL(blob);
        saveAs(responseData, `${this.currentLabelNames.name}`+ EXCEL_EXTENSION);     
        this.commonService.setLoaderShownProperty(false);
        this.isDefaultFilterDownload = false;
      })
    }
    else if(this.isAdvancedFilterDownload){
      this.createFormValues.isPdf = type;
      this.createFormValues.reports = this.reportFormGroup.value.reports;
      this.http.post(`${appModels.REPORT}/${this.currentLabelNames.dynamicAPIUrl}/${appModels.EXPORTPDF}/${this.currentModuleId}`, this.createFormValues, { responseType: 'blob' as 'blob'}).subscribe((res: any) => {
        const blob = new Blob([res], { type: 'application/octet-stream'});
        const responseData= window.URL.createObjectURL(blob);
        if(type == true){
          saveAs(responseData, `${this.currentLabelNames.name}`+ PDF_EXTENSION);     
        }
        else{
          saveAs(responseData, `${this.currentLabelNames.name}`+ EXCEL_EXTENSION);     
        }
        this.commonService.setLoaderShownProperty(false);
        this.isDefaultFilterDownload = false;
        this.isAdvancedFilterDownload = false;
      })

    }
  }


}
