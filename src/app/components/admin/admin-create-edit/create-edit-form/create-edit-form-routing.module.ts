import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { CreateEditFormComponent } from './create-edit-form.component';



const routes: Routes = [
  Shared.childRoutes([
    { path: 'create-edit', component: CreateEditFormComponent },
    { path: 'create-form', component: CreateEditFormComponent },
    { path: 'edit-form', component: CreateEditFormComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateEditTemplateRoutingModule { }
