import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, appModels, CommonService, AttributeType, EntityTypeId,menuNameList,entityTypeAttributeName} from 'src/app/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-create-edit-form',
  templateUrl: './create-edit-form.component.html'
})
export class CreateEditFormComponent implements OnInit {
 
  templateForm: FormGroup = new FormGroup({});
  formResponse: Array<any> = [];
  dropDownResponse: Array<any> = [];
  RoleEntityTypeID:number = EntityTypeId?.ROLE;
  rolesResult:Array<any> = [];
  attributeTypeValue = AttributeType;
  currentRouterValues: any;

  constructor(private activeRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService, private commonService: CommonService) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.onGetFormControlNames();
    !this.currentRouterValues?.isEdit && this.currentRouterValues?.menuName == menuNameList?.ADMIN && this.getAllRole();
    let url = this.currentRouterValues?.menuName == menuNameList?.ADMIN ? '/admin/admin-list' : this.currentRouterValues?.menuName == menuNameList?.CONFIGURATION ? '/configuration/configuration-list' : '';
    this.commonService.setbreadcrumb([{ title: this.currentRouterValues?.pagetitle, relativeRouterUrl: url, queryParams: { tab: this.currentRouterValues?.selectedIndex } }, { title: this.currentRouterValues?.methodName}]);
  }

  onGetFormControlNames(){
    if(this.currentRouterValues?.isEdit){
      this.crudService.get_params(`${appModels.ENTITIES+`/${this.currentRouterValues?.dynamicApiUrl}`}/edit`, { params: {entityTypeId:this.currentRouterValues?.entityTypeID,entityId:this.currentRouterValues?.entityID} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.formResponse = response;
        let dropdownAPI: any = [];
        this.formResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.entityTypeAttributeId}`));
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI, true);
        this.templateForm = this.createFormGroup();
        const onDisableEditEmail = this.templateForm.get(entityTypeAttributeName?.Email);
        onDisableEditEmail?.disable();
      })
    }
    else {
      this.crudService.get(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_DROPDOWN}/${this.currentRouterValues?.entityTypeID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.formResponse = response;
        let dropdownAPI: any = [];
        this.formResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN) {
            if (res.entityTypeAttributeName != menuNameList.ROLE) {
              dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.id}`));
            }
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI);
        this.templateForm = this.createFormGroup();
      })
    }
  }

  // Load Multiple DropDown
  onLoadMultipleDropDown(Api: any, isEdit = false) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownResponse = results.flat(1);
      if (isEdit == true) {
        this.formResponse?.forEach((res: any) => {
          if (res.componentName == this.attributeTypeValue?.DROP_DOWN) {
            this.templateForm?.get(res?.entityTypeAttributeName)?.setValue(+parseInt(res?.attributeValue));
          }
          else if (res.componentName == this.attributeTypeValue?.TOGGLE) {
            let toggleAttributeValue = res?.attributeValue == "true" ? true : false;
            this.templateForm?.get(res?.entityTypeAttributeName)?.setValue(toggleAttributeValue);
          }
          else {
            this.templateForm?.get(res?.entityTypeAttributeName)?.setValue(res?.attributeValue);
          }
        })
      }
      this.commonService.setLoaderShownProperty(false);
    })
  }

  getAllRole() {
    this.crudService.get_params(`${appModels.ROLE}/${this.currentRouterValues?.dynamicApiUrl}`, { params: { entityTypeId: this.RoleEntityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.rolesResult = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }
 
  // Create Form
  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.formResponse?.forEach((column: any) => {
      if (column.isRequired == true && column.isKey == true && column.isDynamic === true) {
      }
      else if (column.entityTypeAttributeName == entityTypeAttributeName?.Email) {
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue, [Validators.required, Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]));
      }
      else if (column.entityTypeAttributeName == entityTypeAttributeName?.PhoneNumber) {
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue, [Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]));
      }
      else if (column.isRequired == true && column.isKey == true && column.isDynamic === false) {
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue, Validators.required));
      }
      else if (column.isRequired == false) {
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue));
      }
      else {
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue, Validators.required));
      }
    });
    return group;
  }


}
