import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateEditTemplateRoutingModule } from './create-edit-form-routing.module';

import { SharedModule } from 'src/app/components/shared/shared.module';
import { CreateEditFormComponent } from './create-edit-form.component';

@NgModule({
  declarations: [CreateEditFormComponent],
  imports: [
    CommonModule,
    CreateEditTemplateRoutingModule,
    SharedModule
  ],
  exports: [CreateEditFormComponent],
})
export class CreateEditFormModule { }
