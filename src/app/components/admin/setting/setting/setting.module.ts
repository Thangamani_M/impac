import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingRoutingModule } from './setting-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { SettingComponent } from './setting.component';
import { CreateEditSettingComponent } from './create-edit-setting/create-edit-setting.component';


@NgModule({
  declarations: [SettingComponent, CreateEditSettingComponent],
  entryComponents:[CreateEditSettingComponent],
  imports: [
    CommonModule,
    SettingRoutingModule,
    SharedModule
  ]
})
export class SettingModule { }
