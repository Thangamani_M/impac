import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { SettingComponent } from './setting.component';

const routes: Routes = [
  Shared.childRoutes([
    { path: 'setting', component: SettingComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
