import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { appModels,CrudService,CommonService,SnackbarService,ResponseMessageTypes,ResponseMessage } from 'src/app/services';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-create-edit-setting',
  templateUrl: './create-edit-setting.component.html',
  styleUrls: ['./create-edit-setting.component.scss']
})
export class CreateEditSettingComponent implements OnInit {

  settingForm:FormGroup = new FormGroup({});
  editResponse:any;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<CreateEditSettingComponent>,private crudService: CrudService,private commonService: CommonService, private snackbarService :SnackbarService) { 
    this.editResponse = data;
  }

  ngOnInit(): void {
    this.createSettingForm();
  }

  createSettingForm() {
    this.settingForm = this.formBuilder.group({
      sessionTimeout: [this.editResponse?.result?.sessionTimeout],
      otpTimeOut:[this.editResponse?.result?.otpTimeOut]
    })
  }

  onkeypressNumber(val: any) {
    this.commonService.onKeyUpNumbersOnly(val);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  createUpdate(){
    this.crudService.post(`${appModels.APP_USER}/${appModels.SETTING}`,this.settingForm.value)
    .pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.commonService.setLoaderShownProperty(false);
      this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
      this.dialogRef.close(res);
    })
  }

}
