import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditSettingComponent } from './create-edit-setting.component';

describe('CreateEditSettingComponent', () => {
  let component: CreateEditSettingComponent;
  let fixture: ComponentFixture<CreateEditSettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEditSettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
