import { Component, OnInit } from '@angular/core';
import { CrudService, appModels, CommonService,menuNameList,AuthenticationService, ConstantValue } from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { CreateEditSettingComponent } from './create-edit-setting/create-edit-setting.component';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit { 

  displayedColumns: string[] =[];
  menuName:string = menuNameList?.SETTING;
  dataSource = new MatTableDataSource<any>([]);
  currentUser:any;
  
  constructor(private crudService: CrudService,private commonService: CommonService,public dialog: MatDialog,private authenticationService: AuthenticationService) {
    this.commonService.setbreadcrumb([{title:this.menuName}]);
   }

  ngOnInit(): void {
    this.getDataSource();
    this.currentUser = this.authenticationService.currentUserValue;
    if(this.currentUser.roleName == ConstantValue.Value){
      this.displayedColumns = ['sessionTimeout','otpTimeOut', 'Action'];
    }
    else{
      this.displayedColumns = ['sessionTimeout','otpTimeOut'];
    }
  }

  getDataSource() {
    this.crudService.get(`${appModels.APP_USER}/${appModels.SETTING}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if(response?.sessionTimeout == null || response?.otpTimeOut == null){
        response.sessionTimeout = 0;
        response.otpTimeOut = 0;
      }
      this.dataSource = new MatTableDataSource([response]);
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onCreateEditClicked(values:any){
    const dialogRef = this.dialog.open(CreateEditSettingComponent, {
      width: '50vw',
      height: 'auto',
      data: {
        result: values
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.getDataSource();
      }
    });

  }
}
