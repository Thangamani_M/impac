import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shared } from "src/app/services";
import { CaseManagementViewComponent } from './case-management-view.component';

const routes: Routes = [
  Shared.childRoutes([
    { path: 'case-management-view', component: CaseManagementViewComponent,data: {title:"view"} }
])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseManagementViewRoutingModule { }
