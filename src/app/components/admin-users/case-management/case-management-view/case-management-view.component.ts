import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels, SnackbarService, CommonService, ResponseMessageTypes,ResponseMessage,AuthenticationService, AttributeType, caseManagement_List_View_TabList,menuNameList } from 'src/app/services';
import { MatDialog} from '@angular/material/dialog';
import { DeleteComponent , PreviewComponent, DialogComponent} from 'src/app/components/shared';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { environment } from '../../../../../environments/environment';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-case-management-view',
  templateUrl: './case-management-view.component.html',
  styleUrls: ['./case-management-view.component.scss']
})
export class CaseManagementViewComponent implements OnInit {

  mapping_id: number;
  mapping: Array<any> = [];
  searchFilterName: Array<any> = [];
  labelNames: Array<any> = caseManagement_List_View_TabList;
  commentForm: FormGroup = new FormGroup({});
  replyForm: FormGroup = new FormGroup({});
  currentUser: any;
  viewList: Array<any> = [];
  templateList:any;
  mappingResults: any;
  mapping_entityName: Array<any> = [];
  mapping_entity:Array<any> = [];
  DeletedIds: Array<any> = [];
  commentListsAttributeValue: Array<any> = [];
  replyListsAttributeValue: Array<any> = [];
  commentsFields: Array<any> = [];
  replyFields: Array<any> = [];
  commentFormObj: Array<any> = [];
  commentFormValues: any;
  updateComment: any;
  commentRequest: Array<any> = [];
  commentsList: any;
  showReplyField: boolean = false;
  replyText = { replyListIndex: 0 };
  replyFormObj: Array<any> = [];
  replyFormValues: any;
  updateReply: any;
  replyRequest: Array<any> = [];
  replyList: any;
  attributeTypeValue = AttributeType;
  customFileList:any;
  currentRouterValues: any;
  attachmentList:any;

  constructor(private router: Router, private activeRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService, private snackbarService: SnackbarService, private commonService: CommonService, public dialog: MatDialog, private authenticationService: AuthenticationService, private datepipe: DatePipe) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
      let url = this.currentRouterValues?.menuName == menuNameList?.CASEMANAGEMENT ? '/case-management/case-mangement-list' : '/case-management/case-mangement-list';
      this.commonService.setbreadcrumb([{ title: this.labelNames[this.currentRouterValues?.selectedIndex].name, relativeRouterUrl: url, queryParams: { tab: this.currentRouterValues?.selectedIndex } }, { title: 'View ' +  this.labelNames[this.currentRouterValues?.selectedIndex].name}]);
      this.onLoadCaseManagementView();
    })
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
  }
  onLoadCaseManagementView(){
    this.viewDetails();
    this.onLoadMappingDetails();
    this.onLoadCommentFields();
  }
  //Details
  viewDetails() {
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.VIEW}/${this.currentRouterValues?.entityId}`, { params: { entityTypeId: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.viewList = response;
      response?.forEach((viewRes: any) => {
      if(viewRes?.hasCaseForm == true){
        this.onLoadTemplateDetails(viewRes?.Entity_Id, viewRes?.caseFormId);
       } else {this.templateList = ''}
      })
      this.commonService.setLoaderShownProperty(false);
    })
  }
 
  //LoadTemplateDetails
  onLoadTemplateDetails(entityId: any, templateId: any) {
    this.crudService.get_params(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.EDIT}`, { params: { entityId: entityId, entityMappedId: templateId, entityTypeId:this.currentRouterValues?.entityTypeId} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.templateList = response;
      this.templateList?.forEach((result:any) =>{
        this.customFileList = result?.componentName == AttributeType?.UPLOAD &&  JSON.parse(result?.attributeValue) || '';
      })
      this.commonService.setLoaderShownProperty(false);
    })
  } 

  downloadCustomTemplateFile(files: any){
    this.commonService.downloadFile(files,this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl);
  }
  //ActiveTag
  getActiveTag(id: any) {
    return this.searchFilterName?.indexOf(id) != -1;
  }
  //Create Edit Clicked
  onCreateEditClicked(values?: any) {
    this.mappingResults?.forEach((res: any) => {
      if (res?.id == values?.id) {
        if (values?.entitytypeid == 1) {
          this.router.navigate([`case-management/case-management-view`], { queryParams: { entityId: values?.id, entityTypeId: values?.entitytypeid, selectedIndex: 3 }, skipLocationChange: environment.SkipLocationChange });
        } else if (values?.entitytypeid == 2) {
          this.router.navigate([`case-management/case-management-view`], { queryParams: { entityId: values?.id, entityTypeId: values?.entitytypeid, selectedIndex: 2 }, skipLocationChange: environment.SkipLocationChange });
        } else if (values?.entitytypeid == 3) {
          this.router.navigate([`case-management/case-management-view`], { queryParams: { entityId: values?.id, entityTypeId: values?.entitytypeid, selectedIndex: 1 }, skipLocationChange: environment.SkipLocationChange });
        }
        this.commonService.setLoaderShownProperty(false);
      }
    })
  }
  //Load Mapping Details
  onLoadMappingDetails() {
    this.mapping_entityName = [];
    this.mapping_entity = [];
    this.crudService.get_path(`${appModels.ENTITIES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.MAPPING}/${this.currentRouterValues?.entityId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.mappingResults = response;
      this.mappingResults?.forEach((res: any) => {
        this.mapping_id = res;
        this.mapping_entityName.push({ entityname: res?.referenceId, id: res?.id, entitytypeid: res?.entityTypeId });
      })
      this.commonService.setLoaderShownProperty(false);
    })
  }
  //Case Management Info
  caseManagementInfo() {
    this.router.navigate([`case-management/case-management-info`], { queryParams: { entityId: this.currentRouterValues?.entityId, entityTypeId:this.currentRouterValues?.entityTypeId, selectedIndex: this.currentRouterValues?.selectedIndex , menuName: this.currentRouterValues?.menuName} });
  }
  //Edit Clicked
  onEditClicked() {
    const methodName = 'Edit ' + this.labelNames[this.currentRouterValues?.selectedIndex].name;
    this.router.navigate(['case-management-form/edit-form'], { queryParams: { entityID: this.currentRouterValues?.entityId, pagetitle: this.labelNames[this.currentRouterValues?.selectedIndex].name, entityTypeID: this.currentRouterValues?.entityTypeId, methodName: methodName, menuName: "Case-Management", selectedIndex: this.currentRouterValues?.selectedIndex, isEdit: true,  dynamicApiUrl: this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl  }, skipLocationChange: environment.SkipLocationChange });
  }

  //Delete Dialog
  openDeleteDialog() {
    this.DeletedIds = [];
    this.DeletedIds.push({ "id": + parseInt(this.currentRouterValues?.entityId) })
    const moduleName = this.labelNames[this.currentRouterValues?.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: "Case Management View",
        moduleName: moduleName,
        api: `${appModels.ENTITIES +`/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}` +`/${appModels.DELETEINBATCH}`}`,
        id: this.DeletedIds,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        this.cancel();
        this.DeletedIds = [];
      }
    });
  }
  //Load Comment Fields

  onLoadCommentFields() {
    this.commentsFields = [];
    this.replyFields = [];
    this.crudService.get_path(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_DROPDOWN}/${this.currentRouterValues?.entityTypeId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      response?.forEach((res: any) => {
        if (res?.componentName == this.attributeTypeValue?.COMMENTS) {
          this.commentsFields.push(res);
        }
        else if (res?.componentName == this.attributeTypeValue?.REPLY) {
          this.replyFields.push(res);
        }
      })

      this.commentForm = this.createCommentFormGroup();
      this.replyForm = this.createReplyFormGroup();
      this.getComments();
      this.commonService.setLoaderShownProperty(false);
    })
  }
  //Comments
  getComments() {
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/edit`, { params: { entityTypeId: this.currentRouterValues?.entityTypeId, entityId: this.currentRouterValues?.entityId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (response?.length > 0) {
        response?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.COMMENTS) {
            this.commentListsAttributeValue = JSON.parse(res?.attributeValue);
            this.commentsList = res;
          }
          if (res?.componentName == this.attributeTypeValue?.REPLY) {
            this.replyListsAttributeValue = JSON.parse(res?.attributeValue);
            this.replyList = res;
          }
          if (res?.componentName == this.attributeTypeValue?.UPLOAD) {
            if(res?.attributeValueId != null){
              this.getAttachment(res?.attributeValueId);
            }
          }
        })
      }
      this.commonService.setLoaderShownProperty(false);
    });
  }
  //Create Comment FormGroup
  createCommentFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.commentsFields?.forEach((column: any) => {
      group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
    });
    return group;
  }
  //Create Reply FormGroup
  createReplyFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.replyFields?.forEach((column: any) => {
      group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
    });
    return group;
  }
  // Post Comments

  postComments() {
    this.commentRequest = [];

    this.commentFormObj = [];
    Object.keys(this.commentForm.value).forEach((key) => {
      const currentControl = this.commentForm.controls[key];
      if (!currentControl.pristine && !currentControl.untouched) {
        this.commentFormObj.push({ [key]: this.commentForm.value[key] });
      }
    });
    this.commentsFields.map((res: any) => {
      this.commentFormObj.map((forms: any) => {
        if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
          res.attributeValue = Object.values(forms).toString();
          this.commentFormValues = res;
        }
      })
    })
    let currentDate = new Date();
    this.updateComment = { comments: this.commentFormValues?.attributeValue, createdBy: this.currentUser?.userName, CreatedOn: this.datepipe.transform(currentDate, 'yyyy-MM-ddTHH:mm:ss') }
    this.commentListsAttributeValue == null ? this.commentListsAttributeValue = [] : this.commentListsAttributeValue;
    this.commentListsAttributeValue.push(this.updateComment);
    this.commentRequest.push({
      "entityTypeId": this.currentRouterValues?.entityTypeId,
      "entityId": this.currentRouterValues?.entityId,
      "entityTypeAttributeId": this.commentFormValues?.id,
      "id": this.commentsList?.attributeValueId ? this.commentsList?.attributeValueId : "",
      "attributeValue": JSON.stringify(this.commentListsAttributeValue),
      "isKey": this.commentFormValues?.isKey,
    })
    this.crudService.update(`${appModels.ATTRIBUTE_VALUE}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}`, this.commentRequest)
      .pipe(untilDestroyed(this)).subscribe(res => {
        this.snackbarService.openSnackbar(ResponseMessage.SUCCESS_FULLY, ResponseMessageTypes.SUCCESS);
        this.commentForm.reset();
        this.getComments();
      })
  }
  // cancel
  cancel() {
    this.router.navigate([`../case-management/case-mangement-list`], { queryParams: { tab: this.currentRouterValues?.selectedIndex } });
  }
  //taskTemplate
  taskTemplate() {
    this.router.navigate([`case-management/task-template`], { queryParams: { entityId: this.currentRouterValues?.entityId, entityTypeId: this.currentRouterValues?.entityTypeId, selectedIndex: this.currentRouterValues?.selectedIndex, menuName:this.currentRouterValues?.menuName } });
  }
  //reply

  reply(index: any) {
    this.showReplyField == false ? this.showReplyField = true : this.showReplyField = false;
    this.replyText.replyListIndex = index;
  }
  //postReply
  
  postReply(comments: any) {
    this.replyRequest = [];
    this.replyFormObj = [];
    Object.keys(this.replyForm.value).forEach((key) => {
      const currentControl = this.replyForm.controls[key];
      if (!currentControl.pristine && !currentControl.untouched) {
        this.replyFormObj.push({ [key]: this.replyForm.value[key] });
      }
    });
    this.replyFields.map((res: any) => {
      this.replyFormObj.map((forms: any) => {
        if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
          res.attributeValue = Object.values(forms).toString();
          this.replyFormValues = res;
        }
      })
    })
    this.updateReply = { reply: this.replyFormValues?.attributeValue, comments }
    this.replyListsAttributeValue == null ? this.replyListsAttributeValue = [] : this.replyListsAttributeValue;
    this.replyListsAttributeValue.push(this.updateReply);
    this.replyRequest.push({
      "entityTypeId": this.currentRouterValues?.entityTypeId,
      "entityId": this.currentRouterValues?.entityId,
      "entityTypeAttributeId": this.replyFormValues?.id,
      "id": this.replyList?.attributeValueId ? this.replyList?.attributeValueId : "",
      "attributeValue": JSON.stringify(this.replyListsAttributeValue),
      "isKey": this.replyFormValues?.isKey
    })
    this.crudService.update(`${appModels.ATTRIBUTE_VALUE}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}`, this.replyRequest)
      .pipe(untilDestroyed(this)).subscribe(res => {
        this.snackbarService.openSnackbar(ResponseMessage.SUCCESS_FULLY, ResponseMessageTypes.SUCCESS);
        this.replyForm.reset();
        this.getComments();
        this.showReplyField = false;
      })
  }

  escalate(){
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '600px',
      height: '300px',
      data: {
       escalate:true,
       moduleName:this.labelNames[this.currentRouterValues?.selectedIndex].name
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if(data != undefined){ 
        this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.ESCALATE}/${this.currentRouterValues?.entityId}`, { params: { entityType: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
          this.snackbarService.openSnackbar(ResponseMessage.SUCCESS_FULLY, ResponseMessageTypes.SUCCESS);
          this.commonService.setLoaderShownProperty(false);
        })
      }
    })
  }

  getAttachment(attributeValueID:any){
    this.crudService.get_params(`${appModels.ATTRIBUTE_VALUE}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${attributeValueID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.attachmentList = JSON.parse(response?.attributeValue);
    })
  }

  downloadFile(files:any){
    const dialogRef = this.dialog.open(PreviewComponent, {
      width: '40vw',
      height: '40vw',
      data: {files:files ? files : null,
            download:true,
            dynamicAPIurl:this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl},
      disableClose: true
    });
    
    dialogRef.afterClosed().subscribe((data) => {
      this.commonService.setLoaderShownProperty(false);
    });
  }
}
