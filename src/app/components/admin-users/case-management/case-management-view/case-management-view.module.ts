import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaseManagementViewRoutingModule } from './case-management-view-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { CaseManagementViewComponent } from './case-management-view.component';

@NgModule({
  declarations: [CaseManagementViewComponent],
  imports: [
    CommonModule,
    CaseManagementViewRoutingModule,
    SharedModule
  ]
})
export class CaseManagementViewModule { }
