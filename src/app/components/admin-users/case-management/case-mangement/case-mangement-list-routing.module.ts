import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CaseMangementListComponent} from 'src/app/components/admin-users/case-management/case-mangement/case-mangement-list.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'case-mangement-list', component: CaseMangementListComponent}
])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseManagementListRoutingModule { }
