import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { CaseManagementListRoutingModule } from './case-mangement-list-routing.module';
import { CaseMangementListComponent } from './case-mangement-list.component';

@NgModule({
  declarations: [CaseMangementListComponent],
  imports: [
    CommonModule,
    CaseManagementListRoutingModule,
    SharedModule
  ]
})
export class CaseManagementListModule { }
