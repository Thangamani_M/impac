import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog} from '@angular/material/dialog';
import { CrudService, appModels, ResponseMessageTypes, SnackbarService, CommonService, ResponseMessage, AttributeType, caseManagement_List_View_TabList, defaultSortingBy,AuthenticationService,ConstantValue ,caseManagement_Public_List_TabList,menuNameList} from 'src/app/services';
import { DeleteComponent,FilterComponent } from 'src/app/components/shared';
import { environment } from '../../../../../environments/environment';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-case-mangement-list',
  templateUrl: './case-mangement-list.component.html',
  styleUrls: ['./case-mangement-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated

})
export class CaseMangementListComponent implements OnInit {
/** Case Management List Variable Declaration */
  selectedIndex:number = 0;
  defaultSortby = defaultSortingBy;
  labelNames: Array<any>;
  displayedColumns: Array<any> = [];
  dataSource = new MatTableDataSource<any>([]);
  entityTypeID: number;
  entityID: number;
  pageSize:number = 10;
  pageNo:number = 0;
  searchKey = '';
  sorting: any;
  pageEvent: any;
  selectedRow: any;
  deletedIds: Array<any> = [];
  attributeTypeValue = AttributeType;
  openFilterDialogs:boolean = false;
  customTableColumnSize: number;
  selected: boolean = true;
  menuName:string = menuNameList?.CASEMANAGEMENT;
  defaultSelectDisplayedColumns: string[] = ['select'];
  defaultActionDisplayedColumns: string[] = ['Action'];
  searchByChartValue: any;
  memberstate:any;
  isFilterExport:boolean = false;
  filterExportRequest:any;
  currentUser:any;
  checked:boolean;
  archiveValue:any;


  constructor(private router: Router,private activeRoute: ActivatedRoute, private crudService: CrudService, private snackbarService: SnackbarService, public dialog: MatDialog, private commonService: CommonService,private authenticationService: AuthenticationService) {
    this.activeRoute.queryParamMap.subscribe((params: any) => {
      this.selectedIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    })
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.archiveValue = localStorage.getItem('archive');
    this.checked = this.archiveValue == "true" ? true : false;
    if(this.currentUser.roleName == ConstantValue.Value){
      this.labelNames = caseManagement_Public_List_TabList;
    }
    else{
      this.labelNames= caseManagement_List_View_TabList;
    }
    this.onSelectedTab(this.selectedIndex);
  }
  selectedMemberId(event?: any) {
    this.memberstate = event;
  }
  chartValues(event?:any,value?:any){
    this.searchByChartValue = event;
  }

  onSelectedTab(event: any) {
    this.selectedIndex = event;
    this.searchKey = '';
    this.displayedColumns = [];
    this.dataSource = new MatTableDataSource<any>([]);
    this.selectedRow != undefined ? this.selectedRow.clear() : this.selectedRow;
    this.selectedIndex == 0 ? this.selected = true : this.selected = false;
    this.router.navigate([`../case-mangement-list`], { relativeTo: this.activeRoute, queryParams: { tab: this.selectedIndex } });
    if (this.selectedIndex != 0) {
      this.getDisplayedColumns();
    }
    this.commonService.setbreadcrumb([{menuName:this.menuName, title: this.labelNames[this.selectedIndex]?.name }]);
  }

  // DisplayedColumns
  getDisplayedColumns() {
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (response.length > 0) {
        response?.map((columnNames: any) => {
          this.displayedColumns.push(columnNames.entityTypeAttributeName);
          this.customTableColumnSize = this.commonService.setTableColumnSize(response.length);
        })
        if(this.selectedIndex == 4){
            this.displayedColumns = this.displayedColumns;
            this.customTableColumnSize = 1;
        }
        else{
          this.displayedColumns = this.defaultSelectDisplayedColumns.concat(this.displayedColumns);
          this.displayedColumns = this.displayedColumns.concat(this.defaultActionDisplayedColumns);
        }
        this.searchByChartValue == undefined && this.getDataSource() || this.searchByChartValue != undefined && this.filterByChartValue();
        this.searchByChartValue = undefined;
      }
    })
  }

      // Filter based on Incident, Case and Event chartvalues in Dashboard.
      filterByChartValue(event?: any, sortBy?: any) {
        this.pageEvent = event;
        if (this.pageEvent) {
          let { pageSize, pageIndex, length } = this.pageEvent['params'];
          this.pageSize = pageSize;
          this.pageNo = pageIndex;
        }
        else {
          this.pageNo = 0;
        }
  
        sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;
    
        this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, status:this.searchByChartValue,memberState:this.memberstate } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
          this.dataSource = new MatTableDataSource(response);
          this.commonService.setLoaderShownProperty(false);
        })
      }
    // DataSource
    getDataSource(event?: any, searchKey?: any, sortBy?: any, archive?:any) {
      this.pageEvent = event;
      if (this.pageEvent) {
        let { pageSize, pageIndex, length } = this.pageEvent['params'];
        this.pageSize = pageSize;
        this.pageNo = pageIndex;
      }
      else {
        this.pageNo = 0;
      }

      searchKey = searchKey ? searchKey : '';
      sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;
      archive == undefined ? archive = this.archiveValue : archive;

      let curdservice = this.selectedIndex == 1 ? this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, searchKey: searchKey, archive:archive} }).pipe(untilDestroyed(this)) :     this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, searchKey: searchKey} }).pipe(untilDestroyed(this))

      curdservice.subscribe((response: any) => {
        this.dataSource = new MatTableDataSource(response);
        this.commonService.setLoaderShownProperty(false);
      })
    }
  //  Sorted
  onSorted(eve: any) {
    this.sorting = eve;
    if (this.sorting.direction == 'asc' || this.sorting.direction == 'desc') {
      this.getDataSource(this.pageEvent, this.searchKey, this.sorting);
    }
  }
  // PageChanged
  onPageChanged(event: any) {
    this.pageEvent = event;
    this.getDataSource(event, this.searchKey);
  }
  // CreateEditClicked
  onCreateEditClicked(values?: any) {
    if (values) {
      this.entityID = values.EntityId;
      const methodName = 'Edit '+this.labelNames[this.selectedIndex].name;
      this.router.navigate(['case-management-form/edit-form'], {queryParams: { entityID: this.entityID,pagetitle :this.labelNames[this.selectedIndex].name,entityTypeID: this.entityTypeID,methodName:methodName,menuName:this.menuName,selectedIndex:this.selectedIndex,dynamicApiUrl:this.labelNames[this.selectedIndex].dynamicAPIUrl,isEdit:true},skipLocationChange:environment.SkipLocationChange});
    }
    else {
      const methodName = 'Create ' + this.labelNames[this.selectedIndex].name;
      this.router.navigate(['case-management-form/create-form'], { queryParams: { entityTypeID: this.entityTypeID, pagetitle: this.labelNames[this.selectedIndex].name, methodName: methodName, menuName: this.menuName, selectedIndex: this.selectedIndex, dynamicApiUrl: this.labelNames[this.selectedIndex].dynamicAPIUrl }, skipLocationChange: environment.SkipLocationChange });
    }
  }
  // Selected
  onSelected(event: any) {
    this.deletedIds = [];
    this.selectedRow = event;
    this.selectedRow._selected?.forEach((element: any) => {
      this.deletedIds.push({ "id": + parseInt(element.EntityId) });
    });
  }
  //DeleteDialog
  openDeleteDialog() {
    if (this.deletedIds == undefined || this.deletedIds.length == 0) {
      this.snackbarService.openSnackbar(ResponseMessage.SELECT_ITEM, ResponseMessageTypes.WARNING);
      return;
    }
    const moduleName = this.labelNames[this.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: this.selectedRow,
        moduleName: moduleName,
        api: `${appModels.ENTITIES +`/${this.labelNames[this.selectedIndex].dynamicAPIUrl}` +`/${appModels.DELETEINBATCH}`}`,
        id: this.deletedIds,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        data.clear();
        this.getDataSource();
        this.deletedIds = [];
      }
    });
  }

 // filterDialog
  openfilterDialog() {
    this.getDataSource()
    this.openFilterDialogs = true;
    const dialogRef = this.dialog.open(FilterComponent, {
      width: '600px',
      height: '300px',
      data:{selectedIndex:this.selectedIndex},
      disableClose: true,
      panelClass: 'custom-dialogbox'
    }); 
    dialogRef.afterClosed().subscribe((data) => {
      if(data != undefined){
        if (data.res !== undefined) {
            this.dataSource =new MatTableDataSource(data.res);
        }
        if(data?.newfordata != undefined){
          this.exportFilterData(data.newfordata.get('filterPayloadRequest'));
        }
    }
  });
  }
  
  exportFilterData(value:any){
    this.isFilterExport = true;
    this.filterExportRequest = value;
  }
  
  exportFile(entityname:any,entityId:any){
    if(this.isFilterExport){
        this.commonService.onFilterByFileDownload(this.filterExportRequest,this.labelNames[this.selectedIndex].name,this.labelNames[this.selectedIndex].value);
    }
    else{
      let selectedRecordIds: any = [];
      if(this.selectedRow?._selected?.length > 0){
        this.selectedRow != undefined && this.selectedRow?._selected?.forEach((res:any) =>{
          selectedRecordIds.push(res.EntityId);
        })
        this.commonService.onFileDownload(entityname,entityId,selectedRecordIds);
      }
      else{
        this.commonService.onFileDownload(entityname,entityId);
      }
    }
  }

  onChangeArchive(){
    this.archiveValue = !this.checked;
    localStorage.setItem('archive',this.archiveValue);
    this.getDataSource(this.pageEvent, this.searchKey, this.sorting, this.archiveValue);
  }
}