import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { PublicIncidentViewRoutingModule } from './public-incident-view-routing.module';
import { PublicIncidentViewComponent } from './public-incident-view.component';

@NgModule({
  declarations: [PublicIncidentViewComponent],
  imports: [
    CommonModule,
    PublicIncidentViewRoutingModule,
    SharedModule
  ]
})
export class PublicIncidentViewModule { }
