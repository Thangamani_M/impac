import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PublicIncidentViewComponent} from 'src/app/components/admin-users/case-management/public-incident-view/public-incident-view.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'public-incident-view', component: PublicIncidentViewComponent}
])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicIncidentViewRoutingModule { }