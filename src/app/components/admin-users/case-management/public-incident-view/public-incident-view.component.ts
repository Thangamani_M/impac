import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels, SnackbarService, CommonService, ResponseMessageTypes,ResponseMessage,caseManagement_Public_List_TabList} from 'src/app/services';
import { MatDialog } from '@angular/material/dialog';
import { PreviewComponent,DialogComponent } from 'src/app/components/shared';
import { FormGroup, FormBuilder } from '@angular/forms';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-public-incident-view',
  templateUrl: './public-incident-view.component.html'
})
export class PublicIncidentViewComponent implements OnInit {

  currentRouterValues: any;
  labelNames: Array<any> = caseManagement_Public_List_TabList;
  templateForm: FormGroup;
  section0:any;
  section1:any;
  section2:any;
  section3:any;
  editViewPublicIncident:any = [];
  objectsList:any;
  objectFileList:any;


  constructor(private router: Router, private activeRoute: ActivatedRoute, private crudService: CrudService, private commonService: CommonService,private snackbarService: SnackbarService, public dialog: MatDialog,private formBuilder: FormBuilder) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
   }

  ngOnInit(): void {
    this.onGetPublicIncidentFormControlNames();
  }
  onGetPublicIncidentFormControlNames(){
    this.editViewPublicIncident = [];
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.VIEW}/${this.currentRouterValues?.entityId}`, { params: { entityTypeId: this.currentRouterValues?.entityTypeId  } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      response?.forEach((result:any) =>{        
        this.section0 = {'attributeName':Object.keys(result[0]), 'attributeValue':Object.values(result[0])};
        this.section1 = {'attributeName':Object.keys(result[1]), 'attributeValue':Object.values(result[1])};
        this.section2 = {'attributeName':Object.keys(result[2]), 'attributeValue':Object.values(result[2])};
        this.section3 = {'attributeName':Object.keys(result[3]), 'attributeValue':Object.values(result[3])};
        this.objectsList = result[4];
        this.objectFileList = JSON.parse(this.objectsList?.["File Description"]);
      })
      this.templateForm = this.createPublicIncidentFormGroup();
    })
  }


  createPublicIncidentFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.section0.attributeName.forEach((res:any,index:any) =>{
      group.addControl(res, this.formBuilder.control(this.section0.attributeValue[index]))
    })
    this.section1.attributeName.forEach((res:any,index:any) =>{
      group.addControl(res, this.formBuilder.control(this.section1.attributeValue[index]))
    })
    this.section2.attributeName.forEach((res:any,index:any) =>{
      group.addControl(res, this.formBuilder.control(this.section2.attributeValue[index]))
    })
    this.section3.attributeName.forEach((res:any,index:any) =>{
      group.addControl(res, this.formBuilder.control(this.section3.attributeValue[index]))
    })
   
    return group;
  }

  downloadPublicIncidentFile(files: any){
    const dialogRef = this.dialog.open(PreviewComponent, {
      width: '40vw',
      height: '40vw',
      data: {files:files ? files : null,
            download:true,
            dynamicAPIurl:this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl},
      disableClose: true
    });
    
    dialogRef.afterClosed().subscribe((data) => {
      this.commonService.setLoaderShownProperty(false);
    });
  }

  cancel() {
    this.router.navigate([`../case-management/case-mangement-list`], { queryParams: { tab: this.currentRouterValues?.selectedIndex } });
  }

  escalate(){
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '600px',
      height: '300px',
      data: {
       escalate:true,
       moduleName:this.labelNames[this.currentRouterValues?.selectedIndex].name
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
        if(data != undefined){
          this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.currentRouterValues?.selectedIndex].dynamicAPIUrl}/${appModels.ESCALATE}/${this.currentRouterValues?.entityId}`, { params: { entityType: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
                this.snackbarService.openSnackbar(ResponseMessage.SUCCESS_FULLY, ResponseMessageTypes.SUCCESS);
                this.commonService.setLoaderShownProperty(false);
              })
        }
    });
  }
}
