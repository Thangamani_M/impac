import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaseManagementInfoRoutingModule } from './case-management-info-routing.module';


import { SharedModule } from 'src/app/components/shared/shared.module';
import { CaseManagementInfoComponent } from './case-management-info.component';

@NgModule({
  declarations: [CaseManagementInfoComponent],
  imports: [
    CommonModule,
    CaseManagementInfoRoutingModule,
    SharedModule
  ]
})
export class CaseManagementInfoModule { }
