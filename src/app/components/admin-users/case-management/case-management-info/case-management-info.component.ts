import { Component, OnInit } from '@angular/core';
import { CrudService, appModels, ResponseMessageTypes,ResponseMessage,SnackbarService,CommonService, AttributeType, caseManagement_Info_TabList,menuNameList } from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";
import { Router, ActivatedRoute } from '@angular/router';
import { SelectionModel} from '@angular/cdk/collections';
import { FormBuilder, FormGroup,Validators } from "@angular/forms";
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';

export interface Mapping {
  entityId:any;
  referenceId: any;
  entityTypeAttributeId: any;
}

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-case-management-info',
  templateUrl: './case-management-info.component.html',
  styleUrls: ['./case-management-info.component.scss']
})
export class CaseManagementInfoComponent implements OnInit {

  entityTypeID:number;
  selectedIndex:number = 0;
  labelNames: Array<any> = caseManagement_Info_TabList;
  searchKey:string = '';
  pageSize: number = 10;
  pageNo:number = 0;
  pageLength:number = 0;
  paginationResult:any;
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource<any>([]);
  defaultSelectDisplayedColumns:string[] = ['select'];
  selection = new SelectionModel<any>(true, []);
  caseManagementForm:FormGroup;
  viewList:Array<any>;
  mappingFieldsResults:Array<any>;
  mappingField : Array<any> =[];
  createMappingValues ={};
  mappingIdValidation:boolean = false;
  caseFormObj:Array<any> = [];
  caseFormValues:Array<any> = [];
  mappingsName:Array<any> =[];

  selectable:boolean = true;
  removable:boolean = true;
  addOnBlur:boolean = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  mappings: Mapping[] = [];
  attributeTypeValue =  AttributeType;
  currentRouterValues: any;

  constructor(private crudService: CrudService,private activeRoute: ActivatedRoute,private router: Router,private formBuilder: FormBuilder,private snackbarService: SnackbarService, private commonService : CommonService) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
   }
   
  ngOnInit(): void {
    this.viewDetails();
    this.mappingFields();
    let url = this.currentRouterValues?.menuName == menuNameList?.CASEMANAGEMENT ? '/case-management/case-mangement-list' : '';
    this.commonService.setbreadcrumb([{ title: this.labelNames[this.currentRouterValues?.selectedIndex - 1].name, relativeRouterUrl: url, queryParams: { tab: this.currentRouterValues?.selectedIndex} }, { title: this.labelNames[this.currentRouterValues?.selectedIndex - 1].name + ' Mapping' }]);
  }

//Details
  viewDetails(){
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.VIEW}/${this.currentRouterValues?.entityId}`,{ params: { entityTypeId: this.currentRouterValues?.entityTypeId} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.viewList = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }
//Mapping Fields
  mappingFields(){
    this.mappingField =[];
    this.crudService.get_path(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_DROPDOWN}/${this.currentRouterValues?.entityTypeId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.mappingFieldsResults = response;
      this.mappingFieldsResults?.forEach((attribute: any) => {
        if (attribute?.isKey == false && attribute?.isDynamic == false && attribute?.isCreate == false) {
          this.mappingField.push(attribute);
        }
        else if (attribute?.isKey == false && attribute?.isDynamic == true &&  attribute?.isCreate == false) {
          this.mappingField.push(attribute);
        }
        else if (attribute?.isKey == true && attribute?.isDynamic == false && attribute?.isCreate == false) {
          this.mappingField.push(attribute);
        }
        this.caseManagementForm = this.createCaseManagementMappingForm();
     })
     this.commonService.setLoaderShownProperty(false);
    })
  }
//Create Form
  createCaseManagementMappingForm() {
    const group = this.formBuilder.group({});
    this.mappingField?.forEach((column: any) => {
     if(column?.componentName != this.attributeTypeValue?.TEXT_FIELD) {
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
    }
    });
    return group;
  }
//Selected Tab
  onSelectedTab(event: any) {
    this.displayedColumns = [];
    this.dataSource = new MatTableDataSource<any>([]);
    this.selectedIndex = event;
    if(this.searchKey != undefined){
      this.search(this.searchKey)
    }
  }
//Displayed Columns
  getDisplayedColumns() {
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (response.length > 0) {
        this.displayedColumns = [];
        response?.map((columnNames: any) => {
            this.displayedColumns.push(columnNames.entityTypeAttributeName);
        })
      }
      this.displayedColumns = this.defaultSelectDisplayedColumns.concat(this.displayedColumns);
      this.getDataSource(this.searchKey);
    })
}
//Data Source
  getDataSource(value:any,event?:any){
    if (event) {
      let { pageSize, pageIndex, length } = event['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
      this.searchKey = value;
      this.entityTypeID = this.labelNames[this.selectedIndex].value;
      this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.MAPPING_SEARCH}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo,searchKey:this.searchKey} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.dataSource = new MatTableDataSource<any>(response.Entities);
        this.paginationResult = response;
        this.pageLength = response.totalItems;
        this.commonService.setLoaderShownProperty(false);
      })
  }
//search
search(searchValue:any){
  this.searchKey = searchValue;
  this.getDisplayedColumns();

}
//Page Change
onPageChanged(event:any,pagination?:any) {
  this.getDataSource(this.searchKey,event);
}
//Selected
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataSource.data.length;
  return numSelected === numRows;
}
//Master Toggle
masterToggle() {
  this.isAllSelected() ? this.selection.clear() : this.dataSource.data?.forEach((row: any) =>
    this.selection.select(row?.EntityId));
}
//Row Checked
onRowChecked() {
  this.mappings = [];
  this.mappingsName = [];
  let unselectedData;
  let { selected } = this.selection;
  let textFieldID = this.mappingField?.filter((item:any) => item?.componentName == this.attributeTypeValue?.TEXT_FIELD);
  this.selection.selected.forEach((element:any)=>{
    this.mappingsName.push(element['Incident Name'] ? element['Incident Name'] : element['Case Name'] ? element['Case Name'] : element['Event Name']);
    this.mappings.push({entityId:element['Entity_Id'],referenceId: element['Incident ID'] ? element['Incident ID'] + '-' + "Incident"  : element['Case ID'] ? element['Case ID'] + '-' + "Case"  : element['Event ID'] + '-' + "Event", entityTypeAttributeId: textFieldID[0]?.id});
  })
  if (selected.length > 0)
    unselectedData = this.dataSource.data.filter((i:any) => !selected.includes(i));
}

//Cancel
cancel(){
  this.router.navigate([`case-management/case-management-view`],{queryParams: {entityId:this.currentRouterValues?.entityId,entityTypeId:this.currentRouterValues?.entityTypeId,selectedIndex: this.currentRouterValues?.selectedIndex, menuName:this.currentRouterValues?.menuName} });
}
//Submit

submit(){
  if (this.mappings.length == 0) {
    this.mappingIdValidation = true;
    this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
    return;
  }
  else{
      this.caseFormObj = [];
      this.caseFormValues = [];
      Object.keys(this.caseManagementForm.value).forEach((key) => {
        const currentControl = this.caseManagementForm.controls[key];
        if (!currentControl.pristine && !currentControl.untouched) {
          this.caseFormObj.push({ [key]: this.caseManagementForm.value[key] });
        }
      });
      this.mappingFieldsResults.map((res: any) => {
        this.caseFormObj.map((forms: any) => {
          if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
            res.attributeValue = Object.values(forms).toString();
            this.caseFormValues.push(res);
          }
        })
      })
      if(this.caseFormValues.length >0){
        this.caseFormValues.forEach((res:any)=>{
          this.createMappingValues = {"entityMappingId":this.mappings,"entityTypeAttributeId":res?.id,"description":res?.attributeValue}
        })
        this.submitMapping();
      }
      else{
          this.createMappingValues = {"entityMappingId":this.mappings}
          this.submitMapping();
      }
  }
}

  submitMapping(){
    this.crudService.post(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.MAPPING}/${this.currentRouterValues?.entityId}`, this.createMappingValues).pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
      this.cancel();
  })
  }

//add
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    // Add our Mapping ID
    if (value) {
      this.mappings.push({entityId:'',referenceId: value, entityTypeAttributeId:''});
    }
    // Clear the input value
    event.chipInput!.clear();
  }
//remove
  remove(mapping: Mapping): void {
    const index = this.mappings.indexOf(mapping);
    if (index >= 0) {
      this.mappings.splice(index, 1);
      this.mappingsName.splice(index, 1);
    }
  }
}
