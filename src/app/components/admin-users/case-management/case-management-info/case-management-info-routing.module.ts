import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { CaseManagementInfoComponent } from './case-management-info.component';

const routes: Routes = [
  Shared.childRoutes([
    { path: 'case-management-info', component: CaseManagementInfoComponent,data: {title:"Mapping"} }
])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseManagementInfoRoutingModule { }
