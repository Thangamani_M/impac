import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskTemplateRoutingModule } from './task-template-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { TaskTemplateComponent } from './task-template.component';


@NgModule({
  declarations: [TaskTemplateComponent],
  imports: [
    CommonModule,
    TaskTemplateRoutingModule,
    SharedModule
  ]
})
export class TaskTemplateModule { }
