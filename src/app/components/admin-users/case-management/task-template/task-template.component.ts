import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels, SnackbarService, CommonService,AttributeType, DynamicAPIEndPoints, caseManagement_List_View_TabList,menuNameList } from 'src/app/services';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-task-template',
  templateUrl: './task-template.component.html',
  styleUrls: ['./task-template.component.scss']
})
export class TaskTemplateComponent implements OnInit {

  taskTemplateForm: FormGroup;
  viewList: any;
  taskTemplateList: any = [];
  dynamicApiUrl = DynamicAPIEndPoints?.TASK_TEMPLATE_URL;
  attributeTypeValue = AttributeType;
  labelNames : Array<any> = caseManagement_List_View_TabList;
  currentRouterValues : any;

  constructor(private activeRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,private crudService: CrudService,private commonService: CommonService) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.viewDetails();
    let url = this.currentRouterValues?.menuName == menuNameList?.CASEMANAGEMENT ? '/case-management/case-mangement-list' : '';
    this.commonService.setbreadcrumb([{ title: this.labelNames[this.currentRouterValues?.selectedIndex]?.name, relativeRouterUrl: url, queryParams: { tab: this.currentRouterValues?.selectedIndex } }, { title:'Task Template' }]);
  }
  // Details
  viewDetails() {
    this.crudService.get_params(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.VIEW}/${this.currentRouterValues?.entityId}`, { params: { entityTypeId: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.viewList = response;
      response?.forEach((viewRes: any) => {
        if (viewRes?.hasTaskTemplate == false) {
          this.onLoadTaskTemplateDetails(viewRes?.caseFormId);
        }
      })
      this.commonService.setLoaderShownProperty(false);
    })
  }
  //Load Task Template Details
  onLoadTaskTemplateDetails(value: any) {
    if (value != null) {
      this.crudService.get_params(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${this.dynamicApiUrl}/${appModels.ENTITY}/${value}`, { params: { entityTypeId: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.taskTemplateList = response;
        this.taskTemplateForm = this.createFormGroup();
        this.commonService.setLoaderShownProperty(false);
      })
    }
  }

  // Create Form
  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.taskTemplateList?.forEach((column: any) => {
      if (column?.isTitle == false) {
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
      }
    });
    return group;

  }

  cancel() {
    this.router.navigate([`case-management/case-management-view`], { queryParams: { entityId: this.currentRouterValues?.entityId, entityTypeId:this.currentRouterValues?.entityTypeId, selectedIndex: this.currentRouterValues?.selectedIndex, menuName:this.currentRouterValues?.menuName } });
  }

  // Change Start
  onChangeStart() {
    this.commonService.onChangeTime(this.taskTemplateForm);
  }
  
}
