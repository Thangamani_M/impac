import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { TaskTemplateComponent } from './task-template.component';

const routes: Routes = [
  Shared.childRoutes([
    { path: 'task-template', component: TaskTemplateComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskTemplateRoutingModule { }
