import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/** Custom Component,Service */
import { SkillsListComponent } from './skills-list.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'skills-list', component: SkillsListComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkillsListRoutingModule { }
