import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkillsListRoutingModule } from './skills-list-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { SkillsListComponent } from './skills-list.component';

@NgModule({
  declarations: [SkillsListComponent],
  imports: [
    CommonModule,
    SkillsListRoutingModule,
    SharedModule
  ]
})
export class SkillsListModule { }
