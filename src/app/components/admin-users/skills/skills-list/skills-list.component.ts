import { Component, OnInit } from '@angular/core';
import { CrudService, appModels, CommonService, skillsListTabList, defaultSortingBy,menuNameList, AuthenticationService, ConstantValue, EntityTypeId } from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-skills-list',
  templateUrl: './skills-list.component.html'
})
export class SkillsListComponent implements OnInit {

  pageSize:number = 10;
  pageNo:number = 0;
  defaultSortby = defaultSortingBy;
  labelNames: Array<any> = skillsListTabList;
  entityTypeID: number;
  entityID: number;
  displayedColumns: string[] =[];
  dataSource = new MatTableDataSource<any>([]);
  searchKey:string;
  sorting:any;
  pageEvent:any;
  customTableColumnSize:number;
  defaultActionDisplayedColumns:string[] = ['search'];
  menuName:string = menuNameList?.SKILLS;
  selectedMemeberstateId:number;
  currentUserMemberState:any;
  currentUserDefaultMemberState:any;
  memberstates: any;
  countryId:any;
  memberStatePageSize:number = 30;
  

  constructor(private crudService: CrudService,private commonService: CommonService,private authenticationService: AuthenticationService) {
    this.commonService.setbreadcrumb([{title:this.labelNames[0].name}]);
  }

  ngOnInit(): void {
    this.currentUserMemberState = this.authenticationService.currentUserValue.memberState;
    this.currentUserDefaultMemberState = this.authenticationService.currentUserValue.defaultMemberState;
    this.getmemberstate(this.authenticationService.currentUserValue.roleName)
  }
  getmemberstate(currentUserRole?:any){
    this.crudService.get_params(`${appModels.ENTITY_TYPE}/${appModels.DASHBOARD}/${ appModels.GETALLWITHPAGINATION}/${EntityTypeId?.MEMBER_STATE}`, { params: { pageSize: this.memberStatePageSize, pageNo: this.pageNo, sortBy: this.defaultSortby?.active, direction: this.defaultSortby?.direction, searchKey: ''} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if(currentUserRole == ConstantValue.Value){
          this.memberstates = response['Entities'];
          if(this.currentUserDefaultMemberState != null){
            let userMemberState = response['Entities']?.find((name: any) => name?.EntityId == this.currentUserDefaultMemberState);
            this.selectedMemeberstateId = userMemberState?.EntityId;
          }
          else{
            let userMemberState = response['Entities']?.find((name: any) => name?.['Member State'] == this.currentUserMemberState);
            this.selectedMemeberstateId = userMemberState?.EntityId;
          }

      }
      else{
          response['Entities'].forEach((val:any) =>{
          if(val['Member State'] == this.currentUserMemberState){
            this.memberstates = [val];
          }
        })
        this.selectedMemeberstateId = this.memberstates[0]?.EntityId;
      }
      this.onChangeMemberState(this.selectedMemeberstateId,this.memberstates);
    })
  }
  onChangeMemberState(value:any,memberStateName?:any){
    let MemberStateAll = memberStateName?.find((name: any) => name?.EntityId == value);
    if(MemberStateAll?.['Member State'] != 'ALL'){
      this.countryId = value;
    }
    else{
      this.countryId = '';
    }
    this.getDisplayedColumns();
  }

  getDisplayedColumns() {
    this.displayedColumns = [];
    this.entityTypeID = this.labelNames[0]?.value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[0].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (response.length > 0) {
        this.customTableColumnSize = this.commonService.setTableColumnSize(response.length);
        response?.map((columnNames: any) => {
          this.displayedColumns.push(columnNames.entityTypeAttributeName);
        })
        this.displayedColumns = this.displayedColumns.concat(this.defaultActionDisplayedColumns);
        this.getDataSource();
      }
    })
  }

  getDataSource(event?: any, searchKey?: any, sortBy?: any) {
    this.pageEvent = event;
    if (this.pageEvent) {
      let { pageSize, pageIndex, length } = this.pageEvent['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else{
      this.pageNo = 0;
    }
    searchKey = searchKey ? searchKey : '';
    sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;

    this.crudService.get_params(`${appModels.SKILLS}/${this.labelNames[0].dynamicAPIUrl}`, { params: { entityTypeId:this.entityTypeID,pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction,searchKey:searchKey,memberState:this.countryId} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      response['Entities'] = response['Entities'].map((items:any) =>
      Object.assign({},items,{ Status: items["status"]}));
      this.dataSource = new MatTableDataSource(response);
      this.commonService.setLoaderShownProperty(false);
    })
}

  onSearched(event: any) {
    this.searchKey = event;
    this.getDataSource(this.pageEvent, this.searchKey)
  }

  onSorted(eve: any) {
    this.sorting = eve;
    if (this.sorting.direction == 'asc' || this.sorting.direction == 'desc') {
      this.getDataSource(this.pageEvent,this.searchKey,this.sorting);
    }
  }

  onPageChanged(event: any) {
    this.pageEvent = event;
    this.getDataSource(event,this.searchKey);
  }
}
