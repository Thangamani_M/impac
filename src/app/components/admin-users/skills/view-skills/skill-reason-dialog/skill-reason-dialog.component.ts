import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { appModels, CrudService, CommonService, ResponseMessageTypes, ResponseMessage, SnackbarService, TagList, AttributeType, DynamicAPIEndPoints } from 'src/app/services';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';


import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-skill-reason-dialog',
  templateUrl: './skill-reason-dialog.component.html',
  styleUrls: ['./skill-reason-dialog.component.scss']
})
export class SkillReasonDialogComponent implements OnInit {
  modalForm: FormGroup = new FormGroup({});
  formResponse: any;
  entityTypeId: number;
  modalFormObj: any = [];
  searchTagName: Array<any> = [];
  modalFormValues: Array<any> = [];
  tagListId: number = TagList.TAG_LIST_ID;
  tagLists: any;
  updateFormValues: any = [];
  searchKey: string;
  RadioDisplayedID: any;
  RadioButtonHide: boolean = true;
  adminUpdateField: any;
  dynamicAPIUrl: string = DynamicAPIEndPoints?.SKILL_SET_URL;
  attributeTypeValue = AttributeType;
  multiDropDownSelect: boolean = false;
  multiDropdownKey : any;



  
  constructor(private crudService: CrudService, private router: Router, private commonService: CommonService, private snackbarService: SnackbarService, public dialog: MatDialog, private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<SkillReasonDialogComponent>) {
    this.formResponse = data;
    this.entityTypeId = data.entityTypeID;
    this.RadioDisplayedID = this.formResponse?.RadioDisplayedAttributesID;
    if (this.formResponse.RadioResponse.length > 0) {
      this.RadioButtonHide = false;
    }
  }

  ngOnInit(): void {
    this.modalForm = this.createFormGroup();
    this.tagList();
    this.adminUpdateField = this.formResponse.adminUpdateFields;
    this.skillsetValue();
  }


  skillsetValue(){
    this.adminUpdateField.forEach((element: any) => {
      if (element.componentName == this.attributeTypeValue?.CHECK_BOX) {
        this.modalForm?.get(element?.entityTypeAttributeName)?.setValue(+element?.attributeValue);
      } else if (element.componentName == this.attributeTypeValue?.TEXT_BOX || element.componentName == this.attributeTypeValue?.TEXT_FIELD) {
        this.modalForm?.get(element?.entityTypeAttributeName)?.setValue(element?.attributeValue);
      } else if (element.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN) {
        this.multiDropdownKey = element.entityTypeAttributeName;
        this.searchTagName = [];
        element.attributeValue?.split(',').forEach((element: any) => {
          this.searchTagName.push(+element)
        });
      }
    });
  }

  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.formResponse?.adminUpdateFields?.forEach((column: any) => {
      if(column?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN){
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(this.searchTagName));
      }
      else if(column?.isRequired == true){
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue, [Validators.required]));
      }
      else{
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue));
      }
    })
    return group;
  }

  tagList() {
    this.crudService.get_path(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/${appModels.ENTITY_TYPE_ID}/${this.tagListId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.tagLists = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }
  
  getActiveTag(id: any) {
    return this.searchTagName?.indexOf(id) != -1;
  }
  searchTags(element: any) {
    if (this.getActiveTag(element.id)) {
      this.searchTagName.splice(element, 1);
    } else {
      this.searchTagName.push(element.id);
    }
  }

  updateSkills() {
    if (this.modalForm.invalid || this.searchTagName.length == 0) {
      this.multiDropDownSelect = true;
      this.modalForm.markAllAsTouched();
      this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
      return;
    }
    // else if (this.modalForm.pristine) {
    //   this.snackbarService.openSnackbar(ResponseMessage.NO_CHANGES, ResponseMessageTypes.WARNING);
    //   return;
    // }
    this.modalFormObj = [];
    this.modalFormValues = [];
    Object.keys(this.modalForm.value)?.forEach((key) => {
        this.modalFormObj.push({ [key]: this.modalForm.value[key] });
        if(key == this.multiDropdownKey){
          this.modalForm.value[key]= this.searchTagName;
        }
    });
    this.adminUpdateField?.forEach((res: any) => {
      this.modalFormObj?.forEach((forms: any) => {
        if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
          res.attributeValue = Object.values(forms).toString();
          this.modalFormValues.push(res);
        }
      })
    })
    this.update();
  }

  update() {
    this.updateFormValues = [];
    this.modalFormValues?.forEach((res: any) => {
     if(res.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN){
       res.attributeValue = this.searchTagName.toString();
     }
      this.updateFormValues.push({
        "entityTypeId": this.formResponse.entityTypeID,
        "entityId": this.formResponse.ReasonEntityId,
        "entityTypeAttributeId": res.entityTypeAttributeId,
        "id": res.attributeValueId ? res.attributeValueId : "",
        "attributeValue": res.attributeValue,
        "isKey": res.isKey
      })
    })
    
    this.crudService.update(this.formResponse.api, this.updateFormValues)
    .pipe(untilDestroyed(this)).subscribe(res => {
        this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
        this.dialogRef.close('SkillsUpdate');
        this.commonService.setLoaderShownProperty(false);
      })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}