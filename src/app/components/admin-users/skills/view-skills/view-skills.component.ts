import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, CommonService, SnackbarService, appModels, AuthenticationService, ResponseMessageTypes, ResponseMessage, AttributeType, DynamicAPIEndPoints, defaultSortingBy, EntityTypeId, previewFileType,menuNameList } from 'src/app/services';
import { PreviewComponent } from 'src/app/components/shared';
import { SkillReasonDialogComponent } from './skill-reason-dialog/skill-reason-dialog.component';
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog} from '@angular/material/dialog';
import { forkJoin } from 'rxjs';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-view-skills',
  templateUrl: './view-skills.component.html',
  styleUrls: ['./view-skills.component.scss']
})
export class ViewSkillsComponent implements OnInit {

  @Output() onPageChanged = new EventEmitter();

  entityTypeID:number= EntityTypeId?.SKILLS;
  entityID: number;
  displayedColumns: string[] = [];
  dataSource: any =[];
  userDetails: any;
  radioresponse: Array<any>;
  responsedata: Array<any>;
  currentUser: Array<any>;
  RadioDisplayedAttributesID: any;
  updatevalue: any;
  reasonViewstatus: any;
  ReasonEntityId: any;
  disabled: boolean;
  pageSize: number = 10;
  pageNo: number = 0;
  defaultSortby = defaultSortingBy;
  dropDownValue: any = [];
  dynamicAPIUrl = DynamicAPIEndPoints?.SKILLS_URL;
  adminUpdateFields: any = [];
  attributeTypeValue = AttributeType;
  currentRouterValues: any;
  totalData:any;
  pageEvent:any;

  constructor(private authenticationService: AuthenticationService, private commonService: CommonService, public dialog: MatDialog, private activeRoute: ActivatedRoute, private crudService: CrudService,private snackbarService: SnackbarService, private router: Router) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.viewSkills();
    let url = this.currentRouterValues?.menuName == menuNameList?.SKILLS ? '/skills/skills-list' : '';
    this.commonService.setbreadcrumb([{ title: this.currentRouterValues?.menuName, relativeRouterUrl: url}, { title: "View User Skills" }]);
  }
  //Preview Name
  getPreviewName(control: any) {
    return control ? JSON.parse(control)[0]?.fileName : 'sample.pdf'
  }
  //preview
  preview(skillsFormControl: any) {
    if(skillsFormControl.Upload != null && JSON.parse(skillsFormControl.Upload)[0]?.fileName?.split('.').pop() == previewFileType?.JPG ||  JSON.parse(skillsFormControl.Upload)[0]?.fileName?.split('.').pop() == previewFileType?.PNG || JSON.parse(skillsFormControl.Upload)[0]?.fileName?.split('.').pop() == previewFileType?.JPEG){
      const dialogRef = this.dialog.open(PreviewComponent, {
        width: '40vw',
        height: '40vw',
        data: skillsFormControl ? skillsFormControl : null,
        disableClose: true
      });
      
      dialogRef.afterClosed().subscribe(() => {
        this.commonService.setLoaderShownProperty(false);
      });

    } else if (skillsFormControl != null) {
      this.commonService.downloadFile(JSON.parse(skillsFormControl.Upload)[0],this.dynamicAPIUrl);
    }
  }

  //Reason POP Up
  openReasonPopEdit(opendata: any, name: any) {
    this.reasonViewstatus = name?.Status
    this.ReasonEntityId = name?.EntityId;
    this.currentUser = this.authenticationService.currentUserValue;
    if (this.RadioDisplayedAttributesID != undefined) {
      if (opendata == "Open") {
        this.getForkJoinRequests();
      } else {
        this.disabled = true;
      }
    } else {
      this.snackbarService.openSnackbar(ResponseMessage.PERMISSION_DENIED, ResponseMessageTypes.WARNING);
      return
    }
  }
// Fork Join
  getForkJoinRequests(): void {
    this.adminUpdateFields = [];
    const tabName = "Reason"
    forkJoin([
      this.crudService.get_params(`${appModels.COMPONENT_VALUE}/${this.dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${this.RadioDisplayedAttributesID}`).pipe(untilDestroyed(this)),
      this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.dynamicAPIUrl}/${appModels.HEADERS}`, {
        params: {
          entityTypeId: this.entityTypeID
        }
      }).pipe(untilDestroyed(this))
    ]).subscribe((response: any) => {
      this.commonService.setLoaderShownProperty(false);
      response.forEach((respObj: any, ix: number) => {
        if (respObj) {
          switch (ix) {
            case 0:
              this.radioresponse = respObj;
              break;
            case 1:
              this.responsedata = respObj;
              this.crudService.get_params(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/edit`, {
                params: {
                  entityTypeId: this.entityTypeID, entityId: this.ReasonEntityId
                }
              }).pipe(untilDestroyed(this)).subscribe((response: any) => {
                response.forEach((editResponse: any) => {
                  this.responsedata.forEach((headerResponse: any) => {
                    if (headerResponse?.entityTypeAttributeId == editResponse?.entityTypeAttributeId && headerResponse?.isAdmin == true) {
                      this.adminUpdateFields.push(editResponse);
                      if (this.adminUpdateFields.componentName == this.attributeTypeValue?.CHECK_BOX) {
                        this.updatevalue = this.adminUpdateFields.attributeValue
                      }
                    }
                  })
                })
                this.openeditDialog(this.responsedata, this.adminUpdateFields);
                this.commonService.setLoaderShownProperty(false);
              })
              break;
          }
        }
      });
    })
  }

  //Reason Dialog
  openeditDialog(response: any, adminUpdateFields: any) {
    const dialogRef = this.dialog.open(SkillReasonDialogComponent, {
      width: '50vw',
      height: 'auto',
      data: {
        result: response,
        headers: "Reason",
        api: appModels.ATTRIBUTE_VALUE +`/${this.dynamicAPIUrl}`,
        RadioResponse: this.radioresponse,
        entityTypeID: this.entityTypeID,
        ReasonEntityId: this.ReasonEntityId,
        reasonViewstatus: this.reasonViewstatus,
        RadioDisplayedAttributesID: this.RadioDisplayedAttributesID,
        adminUpdateFields: this.adminUpdateFields,
        updatevalue: this.updatevalue,
        userID: this.currentRouterValues?.userId
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.viewSkills();
      }
    });
  }
  //view Skills
  viewSkills() {
    this.displayedColumns = [];
    this.crudService.get_path(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/${appModels.USER}/${appModels.DETAILS}/${this.currentRouterValues?.userId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.userDetails = response;
      response?.forEach((res: any) => {
        this.entityID = res?.Entity_Id;
      });
      this.getDisplayedColumns();
      this.commonService.setLoaderShownProperty(false);
    })
  }
  //Displayed Columns
  getDisplayedColumns() {
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      let dropdownAPI: any = [];
      if (response.length > 0) {
        response?.map((columnNames: any) => {
          if(columnNames?.componentName != this.attributeTypeValue?.PREVIEW){
            this.displayedColumns.push(columnNames.entityTypeAttributeName);
          }
          if (columnNames.componentName == this.attributeTypeValue?.CHECK_BOX) {
            this.RadioDisplayedAttributesID = columnNames.entityTypeAttributeId;
          }
          if (columnNames?.componentName == this.attributeTypeValue?.DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${columnNames.entityTypeAttributeId}`));
          }
        })
      }
      this.onLoadMultipleDropDown(dropdownAPI);
      this.getDataSource();
      this.commonService.setLoaderShownProperty(false);
    })
  }

  // Load Multiple DropDown
  onLoadMultipleDropDown(Api: any) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownValue = results.flat(1);
      this.commonService.setLoaderShownProperty(false);
    })
  }

  //Data Source
  getDataSource(event?: any, searchKey?: any, sortBy?: any) {
    this.pageEvent = event;
    if (event) {
      let { pageSize, pageIndex, length } = this.pageEvent['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else {
      this.pageNo = 0;
    }
    searchKey = searchKey ? searchKey : '';
    sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;
    this.crudService.get_params(`${appModels.SKILLS}/${this.dynamicAPIUrl}/${appModels.USER}/${this.entityID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, searchKey: searchKey, status:'' } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.dataSource = new MatTableDataSource(response.Entities);
      this.totalData = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onPageChange(event:any) {
    this.getDataSource(event)
  }
  
  //cancel
  cancel() {
    this.router.navigate([`skills/skills-list`]);
  }

}