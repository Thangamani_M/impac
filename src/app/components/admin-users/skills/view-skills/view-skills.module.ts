import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewSkillsRoutingModule } from './view-skills-routing.module';


/** Custom Component,Service */
import { SharedModule } from 'src/app/components/shared/shared.module';
import { ViewSkillsComponent } from './view-skills.component';
import { SkillReasonDialogComponent } from './skill-reason-dialog/skill-reason-dialog.component';

@NgModule({
  declarations: [ViewSkillsComponent, SkillReasonDialogComponent],
  imports: [
    CommonModule,
    ViewSkillsRoutingModule,
    SharedModule
  ]
})
export class ViewSkillsModule { }
