import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


/** Custom Component,Service */
import { ViewSkillsComponent } from './view-skills.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'view-skills', component: ViewSkillsComponent }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewSkillsRoutingModule { }
