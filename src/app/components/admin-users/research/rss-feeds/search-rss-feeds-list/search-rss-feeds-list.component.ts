import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels, CommonService, TagList, Rss_Feeds_Filter, DynamicAPIEndPoints, defaultSortingBy,ResponseMessage,menuNameList, ConstantValue} from 'src/app/services';
import { environment} from '../../../../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-search-rss-feeds-list',
  templateUrl: './search-rss-feeds-list.component.html',
  styleUrls: ['./search-rss-feeds-list.component.scss']
})
export class SearchRssFeedsListComponent implements OnInit {

  searchKey: string = '';
  searchTagName:Array<any> =[];
  searchFilterName:Array<any> =[];
  previousSearchKey:any;
  pageSize:number = 10;
  pageNo:number = 0;
  paginationResult:any;
  defaultSortby = defaultSortingBy;
  rssFeedsLists:any;
  addMediaHtml:Array<any> = [];
  tagLists:any;
  tagListId: number = TagList.TAG_LIST_ID;
  filterLists:any;
  dynamicAPIUrl = DynamicAPIEndPoints?.RSS_FEEDS_URL; 
  searchTextTag:string = '';
  searchFilter:string = '';
  rssSearchTextTag:any;
  currentRouterValues:any;

  constructor(private router: Router,private activeRoute: ActivatedRoute,private crudService: CrudService,private commonService: CommonService, public sanitizer: DomSanitizer) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.searchKey = params?.searchKey;
      this.previousSearchKey = params?.searchKey;
      this.currentRouterValues = params;
    })
   }

  ngOnInit(): void {
    this.onLoadRssFeeds();
    let url = this.currentRouterValues?.menuName == menuNameList?.RSSFEEDS ? '/rss-feeds/rss-feeds-list' : '';
    this.commonService.setbreadcrumb([{title:this.currentRouterValues?.pagetitle, relativeRouterUrl: url}]);
  }

  onLoadRssFeeds(){
    this.onSearchRss_Feeds(this.searchKey);
    this.filterList();
    this.tagList();
  }

  onCreateClicked() {
      const methodName = ConstantValue?.CreateResearch;
      this.router.navigate(['rss-feeds-form/create-form'], {queryParams: { entityTypeID: this.currentRouterValues?.entityTypeid,pagetitle :menuNameList?.RSSFEEDS, methodName:methodName,menuName:menuNameList?.RSSFEEDS,dynamicApiUrl:this.dynamicAPIUrl},skipLocationChange:environment.SkipLocationChange});
  }

  onKeyupSearch(event:any){
    this.searchTextTag = '';
    this.searchKey = event.target.value;
  }
  
  onSearchRss_Feeds(searchkeyValue?:any,searchTagValue?:any,event?:any){
    this.addMediaHtml = [];
    this.searchKey = searchkeyValue;

    this.rssSearchTextTag = searchTagValue == undefined  && this.searchTagName.length>0 && this.searchKey != '' && this.searchTagName?.toString().concat(','+this.searchKey) || this.searchTextTag == '' && this.searchTagName.length == 0 &&  this.searchKey || this.searchTagName.length > 0 && this.searchKey == '' && this.searchTagName?.toString() || this.searchTextTag;

    if(event){
      let { pageSize,pageIndex,length } = event['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    this.crudService.get_params(`${appModels.RSS_FEEDS}/${this.dynamicAPIUrl}/${appModels.GET_RSS_DATA}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy:this.defaultSortby.active, direction: this.defaultSortby.direction,entityTypeId:this.currentRouterValues?.entityTypeid,rssKeyText:this.rssSearchTextTag, rssKeyFilterText:this.searchFilter} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.rssFeedsLists = response.Entities;
      this.paginationResult = response;

    /** Load Rss-Feeds Add Media Data */
      if(this.rssFeedsLists.length >0){
        this.rssFeedsLists.forEach((element:any) => {
          this.addMediaHtml.push(this.sanitizer.bypassSecurityTrustHtml(element['Add Media'] != undefined && element['Add Media'] || ResponseMessage?.NO_DATA));
        });
        this.commonService.setLoaderShownProperty(false);
      }
      this.commonService.setLoaderShownProperty(false);
    })

  }

 
  onPageChanged(event:any){
    this.onSearchRss_Feeds(this.searchKey,this.searchTextTag,event);
  }

  tagList(){
    this.crudService.get_path(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/${appModels.ENTITY_TYPE_ID}/${this.tagListId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.tagLists = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }

  getActiveTag(id:any){
    return this.searchTagName?.indexOf(id) != -1;
  }
  
  searchTags(element:any){
     if(this.getActiveTag(element.id)){
        this.searchTagName.splice(element,1);
      }else{
        this.searchTagName.push(element.id);
      }
      this.searchTextTag = this.searchTagName.length>0 && this.searchKey != '' && this.searchKey.concat(','+this.searchTagName?.toString()) || this.searchTagName.length>0 && this.searchKey.concat(this.searchTagName?.toString()) || this.searchKey;
      
      this.onSearchRss_Feeds(this.searchKey,this.searchTextTag);
  }

  filterList(){
    this.crudService.get_path(`${appModels.COMPONENT_VALUE}/${this.dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${Rss_Feeds_Filter.FILTER_ID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.filterLists = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }
  
  getActiveFilter(id:any){
    return this.searchFilterName?.indexOf(id) != -1;
  }

  filterTags(element:any){
    if(this.getActiveFilter(element.entityId)){
      this.searchFilterName.splice(element,1);
    }else{
      this.searchFilterName.push(element.entityId);
    }
    this.searchFilter = this.searchFilterName.length>0 && this.searchFilterName?.toString() || '';
    this.onSearchRss_Feeds(this.searchKey);
  }

  rss_feeds_details(value:any){
    this.router.navigate(['rss-feeds/rss-feeds-details'], {queryParams: { searchKey:this.previousSearchKey,entityTypeid:this.currentRouterValues?.entityTypeid,entityid:value?.Entity_Id,pagetitle :menuNameList?.RSSFEEDS, menuName:menuNameList?.RSSFEEDS},skipLocationChange:environment.SkipLocationChange});
  }

  cancel() {
    this.router.navigate([`rss-feeds/rss-feeds-list`]);
  }

}
