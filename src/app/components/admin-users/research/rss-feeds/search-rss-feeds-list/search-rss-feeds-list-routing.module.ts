import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchRssFeedsListComponent } from './search-rss-feeds-list.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'search-rss-feeds-list', component: SearchRssFeedsListComponent,data: {title:"RSS-Feeds"} }
])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRssFeedsListRoutingModule { }
