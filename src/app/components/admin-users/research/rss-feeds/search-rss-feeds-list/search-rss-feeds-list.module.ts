import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRssFeedsListRoutingModule } from './search-rss-feeds-list-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { SearchRssFeedsListComponent } from './search-rss-feeds-list.component';



@NgModule({
  declarations: [SearchRssFeedsListComponent],
  imports: [
    CommonModule,
    SearchRssFeedsListRoutingModule,
    SharedModule
  ]
})
export class SearchRssFeedsListModule { }
