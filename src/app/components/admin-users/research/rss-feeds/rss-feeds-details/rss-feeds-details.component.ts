import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels, DynamicAPIEndPoints, CommonService, ResponseMessage,menuNameList} from 'src/app/services';
import { environment} from '../../../../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-rss-feeds-details',
  templateUrl: './rss-feeds-details.component.html',
  styleUrls: ['./rss-feeds-details.component.scss']
})
export class RssFeedsDetailsComponent implements OnInit {

  rssFeedsDetail:any =[];
  addMedia: any;
  dynamicApiUrl: string = DynamicAPIEndPoints?.RSS_FEEDS_URL;
  currentRouterValues:any;

  constructor(private router: Router,private activeRoute: ActivatedRoute,private crudService: CrudService,public sanitizer: DomSanitizer, private commonService: CommonService) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
   }

  ngOnInit(): void {
    this.rssFeedsDetails();
    let url = this.currentRouterValues?.menuName == menuNameList?.RSSFEEDS ? '/rss-feeds/rss-feeds-list' : '';
    this.commonService.setbreadcrumb([{title:this.currentRouterValues?.pagetitle, relativeRouterUrl: url}, {title:"Detail"}]);
  }

  rssFeedsDetails(){
    this.crudService.get_params(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.VIEW}/${this.currentRouterValues?.entityid}`,{ params: { entityTypeId: this.currentRouterValues?.entityTypeid} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.rssFeedsDetail = response;
        this.addMedia =this.sanitizer.bypassSecurityTrustHtml(this.rssFeedsDetail[0]?.['Add Media'] != null && this.rssFeedsDetail[0]?.['Add Media'] || ResponseMessage?.NO_DATA);
        this.commonService.setLoaderShownProperty(false);
    })
  }

  cancel(){
    this.router.navigate(['rss-feeds/search-rss-feeds-list'], {queryParams: {searchKey:this.currentRouterValues?.searchKey,entityTypeid:this.currentRouterValues?.entityTypeid,pagetitle :menuNameList?.RSSFEEDS, menuName:menuNameList?.RSSFEEDS},skipLocationChange:environment.SkipLocationChange});
  }
}
