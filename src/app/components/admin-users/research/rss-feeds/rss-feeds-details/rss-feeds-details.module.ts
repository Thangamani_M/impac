import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RssFeedsDetailsRoutingModule } from './rss-feeds-details-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { RssFeedsDetailsComponent } from './rss-feeds-details.component';


@NgModule({
  declarations: [RssFeedsDetailsComponent],
  imports: [
    CommonModule,
    RssFeedsDetailsRoutingModule,
    SharedModule
  ]
})
export class RssFeedsDetailsModule { }
