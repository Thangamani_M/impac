import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RssFeedsDetailsComponent } from './rss-feeds-details.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'rss-feeds-details', component: RssFeedsDetailsComponent,data: {title:"RSS-Feeds"} }
])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RssFeedsDetailsRoutingModule { }
