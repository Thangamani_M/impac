import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RssFeedsListRoutingModule } from './rss-feeds-list-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { RssFeedsListComponent } from 'src/app/components/admin-users/research/rss-feeds/rss-feeds-list/rss-feeds-list.component';

@NgModule({
  declarations: [RssFeedsListComponent],
  imports: [
    CommonModule,
    RssFeedsListRoutingModule,
    SharedModule
  ]
})
export class RssFeedsListModule { }
