import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RssFeedsListComponent } from 'src/app/components/admin-users/research/rss-feeds/rss-feeds-list/rss-feeds-list.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'rss-feeds-list', component: RssFeedsListComponent, data: { title: "RSS-Feeds" } }
  ])
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RssFeedsListRoutingModule { }
