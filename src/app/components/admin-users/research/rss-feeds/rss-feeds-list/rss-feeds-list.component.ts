import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { appModels, CrudService,CommonService, rss_FeedsTabList, Rss_Feeds_Filter,menuNameList, ConstantValue } from 'src/app/services';
import { environment} from '../../../../../../environments/environment';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-rss-feeds-list',
  templateUrl: './rss-feeds-list.component.html',
  styleUrls: ['./rss-feeds-list.component.scss']
})
export class RssFeedsListComponent implements OnInit {

  @ViewChild("focusSearch") searchFocusEl: ElementRef;

  entityTypeID:number;
  entityID:number;
  searchKey:string = '';
  labelNames:Array<any> = rss_FeedsTabList;
  contentTypeLists:Array<any> = [];

  constructor(private router: Router, private commonService: CommonService,private crudService: CrudService) { }

  ngOnInit(): void {
    this.onLoadContentType();
    this.onFocusSearchField();
    this.commonService.setbreadcrumb([{title:this.labelNames[0].name}]);
  }

  
  onFocusSearchField(){
    setTimeout(()=>{
      this.searchFocusEl?.nativeElement?.focus();
    },100);
  }

  onLoadContentType(){
    this.entityTypeID = this.labelNames[0].value;
    this.crudService.get_path(`${appModels.COMPONENT_VALUE}/${this.labelNames[0].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${Rss_Feeds_Filter.FILTER_ID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.contentTypeLists = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }

  search(searchValue:any){
    this.searchKey = searchValue ? searchValue : '';
    this.router.navigate(['rss-feeds/search-rss-feeds-list'], {queryParams: { searchKey:this.searchKey,entityTypeid:this.entityTypeID,pagetitle :menuNameList?.RSSFEEDS, menuName:menuNameList?.RSSFEEDS},skipLocationChange:environment.SkipLocationChange});
  }

  onCreateClicked() {
      const methodName = ConstantValue?.CreateResearch;
      this.router.navigate(['rss-feeds-form/create-form'], {queryParams: { entityTypeID: this.entityTypeID,pagetitle :menuNameList?.RSSFEEDS, methodName:methodName,menuName:menuNameList?.RSSFEEDS,dynamicApiUrl:this.labelNames[0].dynamicAPIUrl},skipLocationChange:environment.SkipLocationChange});
  }

}
