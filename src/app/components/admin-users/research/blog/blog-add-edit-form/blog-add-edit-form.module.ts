import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { RouterModule } from '@angular/router';
import { Shared } from 'src/app/services/app/shared.service';
import { CreateEditFormModule } from '../../../admin-users-create-edit/create-edit-form/create-edit-form.module';
import { CreateEditFormComponent } from '../../../admin-users-create-edit/create-edit-form/create-edit-form.component';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    CreateEditFormModule,
    RouterModule.forChild([
        Shared.childRoutes([
            { path: 'create-form', component: CreateEditFormComponent ,data: {title:"create form"} },
            { path: 'edit-form', component: CreateEditFormComponent ,data: {title:" edit form"} }
        ])
    ])
  ]
})
export class BlogAddEditFormModule { }
