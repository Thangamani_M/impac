import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {BlogListComponent} from 'src/app/components/admin-users/research/blog/blog-list/blog-list.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'blog-list', component: BlogListComponent,data: {title:"Blog"} }
])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogListRoutingModule { }
