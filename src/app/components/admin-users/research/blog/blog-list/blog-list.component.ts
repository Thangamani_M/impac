import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudService, appModels, CommonService, TagList, blogListTabList, defaultSortingBy ,menuNameList} from 'src/app/services';
import { environment } from '../../../../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {

  entityTypeID: number;
  entityID: number;
  searchKey = '';
  pageSize:number = 10;
  pageNo:number = 0;
  paginationResult:any;
  defaultSortby = defaultSortingBy;
  labelNames: Array<any> = blogListTabList;
  tagListId = TagList.TAG_LIST_ID;
  tagLists: any;
  searchTagName: Array<any> = [];
  blogLists: any;
  imageSource: Array<any> = [];
  blogDataHtml: Array<any> = [];
  searchTextTag = '';
  blogSearchTextTag: any;


  constructor(private router: Router, private commonService: CommonService, private crudService: CrudService, public sanitizer: DomSanitizer) { }
 
  ngOnInit(): void {
    this.onLoadBlog();
    this.commonService.setbreadcrumb([{ title: this.labelNames[0].name }]);
  }

  onLoadBlog(){
    this.blogList();
    this.tagList();
  }

  blogList(event?: any) {
    this.entityTypeID = this.labelNames[0]?.value;
    if (event) {
      let { pageSize, pageIndex, length } = event['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    this.crudService.get_params(`${appModels.ENTITY_TYPE +`/${this.labelNames[0].dynamicAPIUrl}`}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: this.defaultSortby.active, direction: this.defaultSortby.direction,searchKey:this.searchKey} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.blogLists = response.Entities;
      this.paginationResult = response;
      this.onLoadImage();
      this.commonService.setLoaderShownProperty(false);
    }) 
  }


  search(searchkeyValue: any, searchTagValue?:any,event?:any) {
    this.searchKey = searchkeyValue;

    this.blogSearchTextTag = searchTagValue == undefined  && this.searchTagName.length>0 && this.searchKey != '' && this.searchTagName?.toString().concat(','+this.searchKey) || this.searchTextTag == '' && this.searchTagName.length == 0 &&  this.searchKey || this.searchTagName.length > 0 && this.searchKey == '' && this.searchTagName?.toString() || this.searchTextTag;

    if (event) {
      let { pageSize, pageIndex, length } = event['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    this.crudService.get_params(`${appModels.BLOG}/${this.labelNames[0].dynamicAPIUrl}/${appModels.GET_BLOG_DATA}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: this.defaultSortby.active, direction: this.defaultSortby.direction, entityTypeId: this.entityTypeID, blogKeyText: this.blogSearchTextTag} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.blogLists = response.Entities;
      this.paginationResult = response;

      this.blogLists = this.blogLists.map((items:any) =>
        Object.assign({},items,{"Thumb Image": items['Thumb Image'] ? items['Thumb Image'] :null}));

      this.onLoadImage();
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onPageChanged(event: any) {
    if(!this.searchKey){
      this.blogList(event);
    }
    else{
      this.search(this.searchKey,this.searchTextTag,event);
    }
  }
  
  onLoadImage(){
    this.blogDataHtml = []; 
    this.imageSource = [];
    /** Load Image */
    if (this.blogLists.length > 0) {
      this.blogLists.forEach((element: any,index:any) => {
        this.blogDataHtml.push(element['Blog Data']);
        if (JSON.parse(element['Thumb Image']) != null) {
          let files = JSON.parse(element['Thumb Image'])[0];
          if(files?.id != undefined || null){
            this.crudService.get_path(`${appModels.ENTITIES}/${this.labelNames[0].dynamicAPIUrl}/${appModels.DOWNLOAD_FILE}/${files?.id}`).pipe(untilDestroyed(this)).subscribe((res: any) => {
              this.imageSource.push({"image":this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png;base64, ${res[files?.fileName]}`),"index":index});
              this.commonService.setLoaderShownProperty(false);
              this.imageSource = this.removeDuplicates(this.imageSource, "index");
            })
        }
        }
        else if (JSON.parse(element['Thumb Image']) == null){
          this.imageSource.push({"image":null,"index":index});
        }
      });
    }     
  }

  removeDuplicates(myArray:any, Prop:any) {
    return myArray.filter((obj:any, pos:any, arr:any) => {
      return arr.map((mapObj:any)  => mapObj[Prop]).indexOf(obj[Prop]) === pos;
    });
  }

  onCreateEditClicked(values?: any) {
    if (values) {
      this.entityID = values.EntityId ? values.EntityId : values.Entity_Id;
      const methodName = 'Edit Blog';
      this.router.navigate(['blog-form/edit-form'], { queryParams: { entityID: this.entityID, pagetitle: menuNameList?.BLOG, entityTypeID: this.entityTypeID, methodName: methodName, isEdit: true, menuName: menuNameList?.BLOG, dynamicApiUrl: this.labelNames[0].dynamicAPIUrl }, skipLocationChange: environment.SkipLocationChange });
    }
    else {
      const methodName = 'Create New Blog';
      this.router.navigate(['blog-form/create-form'], { queryParams: { entityTypeID: this.entityTypeID, pagetitle: menuNameList?.BLOG, methodName: methodName, menuName: menuNameList?.BLOG, dynamicApiUrl: this.labelNames[0].dynamicAPIUrl }, skipLocationChange: environment.SkipLocationChange });
    }
  }

  tagList() {
    this.crudService.get_path(`${appModels.ENTITIES}/${this.labelNames[0].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ID}/${this.tagListId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.tagLists = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }

  getActiveTag(id: any) {
    return this.searchTagName?.indexOf(id) != -1;
  }
  
  searchTags(element: any) {
    if (this.getActiveTag(element.id)) {
      this.searchTagName.splice(element, 1);
    } else {
      this.searchTagName.push(element.id);
    }
    this.searchTextTag = this.searchTagName.length>0 && this.searchKey != '' && this.searchKey.concat(','+this.searchTagName?.toString()) || this.searchTagName.length>0 && this.searchKey.concat(this.searchTagName?.toString()) || this.searchKey;
      
    this.search(this.searchKey,this.searchTextTag);
  }

}
