import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { CreateEditFormComponent } from './create-edit-form.component';


const routes: Routes = [
  Shared.childRoutes([
    { path: 'create-form', component: CreateEditFormComponent ,data: {title:"create form"} },
    { path: 'edit-form', component: CreateEditFormComponent ,data: {title:" edit form"} }
])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateEditFormRoutingModule { }
