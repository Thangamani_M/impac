import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, appModels, CommonService, AttributeType, EntityTypeId,menuNameList } from 'src/app/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-create-edit-form',
  templateUrl: './create-edit-form.component.html'
})
export class CreateEditFormComponent implements OnInit {

  templateForm: FormGroup;
  formResponse: Array<any> = [];
  dropDownResponse: Array<any> = [];
  RoleEntityTypeID: number = EntityTypeId?.ROLE;
  rolesResult:Array<any>;
  attributeTypeValue = AttributeType;
  currentRouterValues: any;
  approvalRoleResponse:Array<any> = [];
  notifyRoleResponse:Array<any> = [];

  constructor(private activeRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService, private datepipe: DatePipe,private commonService : CommonService,public dialog: MatDialog) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })
  }

  ngOnInit(): void {
    this.onGetFormControlNames();
    !this.currentRouterValues?.isEdit && this.currentRouterValues?.menuName == menuNameList?.POLICIES && this.getAllRole();
    let url = this.currentRouterValues?.menuName == menuNameList?.CASEMANAGEMENT ? '/case-management/case-mangement-list' : this.currentRouterValues?.menuName == menuNameList?.POLICIES ? '/policies-legislation/policies-legislation-list' : this.currentRouterValues?.menuName == menuNameList?.BLOG ? '/blog/blog-list' : this.currentRouterValues?.menuName == menuNameList?.RSSFEEDS ? '/rss-feeds/rss-feeds-list' : '';
    this.commonService.setbreadcrumb([{ title:this.currentRouterValues?.pagetitle, relativeRouterUrl: url, queryParams: { tab: this.currentRouterValues?.selectedIndex} }, { title: this.currentRouterValues?.methodName }]);
  }
  // Form Control Names
  onGetFormControlNames() {
    if (this.currentRouterValues?.isEdit) {
      this.crudService.get_params(`${appModels.ENTITIES +`/${this.currentRouterValues?.dynamicApiUrl}`}/edit`, { params: { entityTypeId:this.currentRouterValues?.entityTypeID, entityId: this.currentRouterValues?.entityID} }).pipe(untilDestroyed(this)).subscribe((response: any) => {

        response?.forEach((data:any) =>{
          //Approver step in Policy & Legislation
          if(data?.groupId == 1){
            this.approvalRoleResponse.push(data);
          }
          //Notify step in Policy & Legislation
          else if(data?.groupId == 2){
            this.notifyRoleResponse.push(data);
          }
          else{
            this.formResponse.push(data);
          }
        })
        
        let dropdownAPI: any = [];
        this.formResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN ||res?.componentName == this.attributeTypeValue?.CHECK_BOX) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.entityTypeAttributeId}`));
          }
        })
        //Approver step in Policy & Legislation
        this.approvalRoleResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.entityTypeAttributeId}`));
          }
        })
        //Notify step in Policy & Legislation
        this.notifyRoleResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.entityTypeAttributeId}`));
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI, true);
        this.templateForm = this.createFormGroup();
      })
    }
    else {
      this.crudService.get(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_DROPDOWN}/${this.currentRouterValues?.entityTypeID}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
        response?.forEach((data:any) =>{
          //Approver step in Policy & Legislation
          if(data?.groupId == 1){
            this.approvalRoleResponse.push(data);
          }
          //Notify step in Policy & Legislation
          else if(data?.groupId == 2){
            this.notifyRoleResponse.push(data);
          }
          else{
            this.formResponse.push(data);
          }
        })
        let dropdownAPI: any = [];
        this.formResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.id}`));
          }
        })
        //Approver step in Policy & Legislation
        this.approvalRoleResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.id}`));
          }
        })
        //Notify step in Policy & Legislation
        this.notifyRoleResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.currentRouterValues?.dynamicApiUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.id}`));
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI);
        this.templateForm = this.createFormGroup();
      })
    }
  }

  //Load Multiple DropDown
  onLoadMultipleDropDown(Api: any, isEdit = false) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownResponse = results.flat(1);
      if (isEdit == true) {
        this.formResponse?.forEach((res: any) => {
          if (res.componentName == this.attributeTypeValue?.DROP_DOWN) {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(+parseInt(res?.attributeValue));
          }
          else if(res.componentName == this.attributeTypeValue?.CHECK_BOX){
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(+parseInt(res?.attributeValue));
          }
          else if(res.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN){
            var splitRes = res?.attributeValue?.split(",");
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(splitRes);
          }
          else if (res.componentName == this.attributeTypeValue?.DATE_TIME) {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(this.datepipe.transform(res?.attributeValue, 'yyyy-MM-ddTHH:mm:ss'));
          }
          else {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(res?.attributeValue);
          }
        })
        //Approver step in Policy & Legislation
        this.approvalRoleResponse?.forEach((res: any) => {
          if (res.componentName == this.attributeTypeValue?.DROP_DOWN) {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(+parseInt(res?.attributeValue));
          }
          else if(res.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN){
            var splitRes = res?.attributeValue?.split(",");
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(splitRes);
          }
          else {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(res?.attributeValue);
          }
        })
        //Notify step in Policy & Legislation
        this.notifyRoleResponse?.forEach((res: any) => {
          if (res.componentName == this.attributeTypeValue?.DROP_DOWN) {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(+parseInt(res?.attributeValue));
          }
          else if(res.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN){
            var splitRes = res?.attributeValue?.split(",");
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(splitRes);
          }
          else {
            this.templateForm.get(res?.entityTypeAttributeName)?.setValue(res?.attributeValue);
          }
        })
      }
      this.commonService.setLoaderShownProperty(false);
    })
  }

  getAllRole() {
    this.crudService.get_params(`${appModels.ROLE}/${this.currentRouterValues?.dynamicApiUrl}`, { params: { entityTypeId: this.RoleEntityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.rolesResult = response;
      this.commonService.setLoaderShownProperty(false);
    })
  }

  // Create Form
  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.formResponse?.forEach((column: any) => {
      if (column?.isCreate == true) {
        if(column?.componentName == AttributeType?.MULTI_DROP_DOWN){
          if(column?.isRequired == true){
            group.addControl(column?.entityTypeAttributeName, this.formBuilder.control([column?.attributeValue], Validators.required));
          }
          else{
            group.addControl(column?.entityTypeAttributeName, this.formBuilder.control([column?.attributeValue]));
          }
        }
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
      }
    });
    //Approver step in Policy & Legislation
    this.approvalRoleResponse?.forEach((column: any) => {
      if (column?.isCreate == true) {
        if(column?.componentName == AttributeType?.MULTI_DROP_DOWN){
          if(column?.isRequired == true){
            group.addControl(column?.entityTypeAttributeName, this.formBuilder.control([column?.attributeValue], Validators.required));
          }
          else{
            group.addControl(column?.entityTypeAttributeName, this.formBuilder.control([column?.attributeValue]));
          }
        }
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
      }
    });
    //Notify step in Policy & Legislation
    this.notifyRoleResponse?.forEach((column: any) => {
      if (column?.isCreate == true) {
        if(column?.componentName == AttributeType?.MULTI_DROP_DOWN){
          if(column?.isRequired == true){
            group.addControl(column?.entityTypeAttributeName, this.formBuilder.control([column?.attributeValue], Validators.required));
          }
          else{
            group.addControl(column?.entityTypeAttributeName, this.formBuilder.control([column?.attributeValue]));
          }
        }
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
      }
    });
    
    return group;
  }

}
