import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Shared } from "src/app/services";
import { ViewPoliciesComponent } from './view-policies.component';


const routes: Routes = [
  Shared.childRoutes([
    { path: 'view-policies', component: ViewPoliciesComponent },

  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewPoliciesRoutingModule { }
