import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels,SnackbarService, ResponseMessageTypes, AuthenticationService, CommonService, ResponseMessage, viewPolicyTabList,menuNameList, AttributeType,policyLegislationListTabList, ConstantValue, supportedFileType} from 'src/app/services';
import { environment } from '../../../../../environments/environment';
import { MatDialog} from '@angular/material/dialog';
import { DeleteComponent,PreviewComponent} from 'src/app/components/shared';
import { FormGroup, FormBuilder,FormArray } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { SelectionModel } from '@angular/cdk/collections';
import { forkJoin } from 'rxjs';
import { PolicyReasonPopupComponent } from './policy-reason-popup/policy-reason-popup.component';
import { MatTableDataSource } from "@angular/material/table";

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-view-policies',
  templateUrl: './view-policies.component.html',
  styleUrls: ['./view-policies.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ViewPoliciesComponent implements OnInit {

  displayedColumns: string[] = ['version', 'submittedOn', 'approvedBy', 'approvedOn'];
  dataSource = new MatTableDataSource<any>([]);
  selectedIndex:number = 0;
  currentUser: any;
  radioresponse: any;
  policyInfolistStatus: any;
  disabled: boolean;
  reasonresponse: any;
  labelNames: Array<any> = viewPolicyTabList;
  pageSize: number = 10;
  pageNo: number = 0;
  paginationResult:any;
  policyInfolist: any;
  uploadedFileList: any;
  DeletedIds: Array<any> = [];
  versionlist: any;
  policyVersionForm: FormGroup;
  selection = new SelectionModel<any>(true, []);
  compareVersionDetails: Array<any> = [];
  versionCompare: Array<any> = [];
  compareHtmlFile: any;
  showCompareVersion: boolean = false;
  policyDocument:any;
  currentRouterValues:any;
  showEditRichTextEditor:boolean = false;
  editorResponse:any;
  config: any = this.commonService.editorConfiguration;
  attributeTypeValue = AttributeType;
  editorValue:any;
  updateDocumentTextValues:any = [];
  showEditFiles:boolean = false;
  updateFileDescription:any = [];
  editoDescResponse:any;
  editorDescValue:any;
  showDeleteIcons= false;
  maxFilesUpload: Number = 5;
  fileList: File[] = [];
  selectedFile: any;
  listOfFiles: any[] = [];
  selectable = true;
  removable = true;
  progress = 0;
  hideProgressBar:boolean = true;

  constructor(private router: Router, private activeRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService, private snackbarService: SnackbarService, public dialog: MatDialog, private authenticationService: AuthenticationService, private datepipe: DatePipe, private commonService: CommonService, public sanitizer: DomSanitizer) {
    this.activeRoute.queryParams.subscribe((params: any) => {
      this.currentRouterValues = params;
    })

  }
  
  ngOnInit(): void {
    this.createPolicyVersionForm();
    this.onSelectedTab(this.selectedIndex);
  }

  onSelectedTab(event: any) {
    this.selectedIndex = event;
    if (this.selectedIndex == 0) {
      this.policyInfo();
      this.showCompareVersion = false;
    }
    else if (this.selectedIndex == 1) {
      this.getApprovalHistory();
      this.showCompareVersion = false;
    }
    else if (this.selectedIndex == 2) {
      this.getVersionList();
      this.getPolicyVersionArray.clear();
      this.selection.clear();
      this.showCompareVersion = false;
    }
    
    let url =  this.currentRouterValues?.menuName == menuNameList?.POLICIES ? '/policies-legislation/policies-legislation-list' :'';
    this.commonService.setbreadcrumb([{ title: this.currentRouterValues?.menuName, relativeRouterUrl: url, queryParams: { tab: this.currentRouterValues?.selectedIndex} }, { title: this.labelNames[this.selectedIndex]?.name }]);
  }

 
  policyInfo() {
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.VIEW}/${this.currentRouterValues?.entityId}`, { params: { entityTypeId: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.policyInfolist = response;
      this.policyInfolist.forEach((res: any) => {
        this.policyInfolistStatus = res.Status;
        if (res['Upload Files here']) {
          this.uploadedFileList = JSON.parse(res['Upload Files here']);
        }
        this.policyDocument = res?.['Policy Document Text'] != null && this.sanitizer.bypassSecurityTrustHtml(res?.['Policy Document Text']) || ResponseMessage?.NO_DATA;
      })
      this.commonService.setLoaderShownProperty(false);
    })
  }

   
  download(files: any) {
    const dialogRef = this.dialog.open(PreviewComponent, {
      width: '40vw',
      height: '40vw',
      data: {files:files ? files : null,
            download:true,
            dynamicAPIurl:this.labelNames[this.selectedIndex].dynamicAPIUrl},
      disableClose: true
    });
    
    dialogRef.afterClosed().subscribe((data) => {
      this.commonService.setLoaderShownProperty(false);
    });
  }


  openDeleteDialog() {
    this.DeletedIds = [];
    this.DeletedIds.push({ "id": + parseInt(this.currentRouterValues?.entityId) })
    const moduleName = this.labelNames[this.currentRouterValues?.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: "Policy Info",
        moduleName: moduleName,
        api: `${appModels.ENTITIES}/${appModels.DELETEINBATCH}`,
        id: this.DeletedIds,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        this.cancel();
        this.DeletedIds = [];
      }
    });
  }

  onEditClicked() {
    const methodName = 'Edit Policies';
    this.router.navigate(['policies-form/edit-form'], { queryParams: { entityID: this.currentRouterValues?.entityId, pagetitle: this.labelNames[this.currentRouterValues?.selectedIndex].name, entityTypeID: this.currentRouterValues?.entityTypeId, methodName: methodName, menuName: menuNameList?.POLICIES, selectedIndex: this.currentRouterValues?.selectedIndex, isEdit: true, dynamicApiUrl:this.labelNames[this.selectedIndex].dynamicAPIUrl }, skipLocationChange: environment.SkipLocationChange });
  }

  cancel() {
    this.router.navigate([`../policies-legislation/policies-legislation-list`], { queryParams: { tab: this.currentRouterValues?.selectedIndex } });
  }

  onPageChanged(event: any) {
    this.getVersionList(event);
  }

  getVersionList(event?: any) {
    if (event) {
      let { pageSize, pageIndex, length } = event['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
      this.getPolicyVersionArray.clear();
    }
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.POLICY_VERSION}`, { params: { entityId: this.currentRouterValues?.entityId, entityTypeId: this.currentRouterValues?.entityTypeId, pageSize: this.pageSize, pageNo: this.pageNo, } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.versionlist = response?.Entities;
      this.paginationResult = response;
      this.versionlist?.forEach((column: any) => {
        const versionObj = [{ entityTypeAttributeName: "version", attributeValue: column?.version }, { entityTypeAttributeName: "submittedOn", attributeValue: this.datepipe.transform(column?.submittedOn, 'yyyy-MM-dd,h:mm:ss a') },
        { entityTypeAttributeName: "approvedBy", attributeValue: column?.approvedBy },
        { entityTypeAttributeName: "status", attributeValue: column?.status },
        { entityTypeAttributeName: "entity_type_attribute_id", attributeValue: column?.entityTypeAttributeId },
        { entityTypeAttributeName: "entity_id", attributeValue: column?.entityId },
        { entityTypeAttributeName: "id", attributeValue: column?.attributeValueId },
        { entityTypeAttributeName: "entity_type_id", attributeValue: this.currentRouterValues?.entityTypeId }];
        this.createFormGroup(versionObj);
      })
      this.policyVersionForm.disable();

      this.commonService.setLoaderShownProperty(false);
    })
  }

  createFormGroup(argGroup: any) {
    const group = this.formBuilder.group({});
    argGroup?.forEach((column: any) => {
      group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
    });
    this.getPolicyVersionArray.push(group);
  }

  get getPolicyVersionArray(): FormArray {
    return this.policyVersionForm.get('policyVersionArray') as FormArray;
  }

  createPolicyVersionForm() {
    this.policyVersionForm = this.formBuilder.group({
      policyVersionArray: this.formBuilder.array([])
    });
  }

  onRowChecked() {
    this.compareVersionDetails = [];
    if (this.selection.selected.length <= 2) {
      this.selection.selected.forEach((res: any) => {
        this.compareVersionDetails.push(res.value);
      })
    }
  }


  getcheckboxDisable(i: number) {
    let isDisable = true;
    this.compareVersionDetails.forEach((res: any) => {
      if (res?.version == this.getPolicyVersionArray?.controls[i]?.value?.version) {
        isDisable = false;
      }
    })
    return this.selection.selected.length == 2 && isDisable
  }


  compareVersion() {
    this.versionCompare = [];
    if (this.compareVersionDetails.length == 0 || this.compareVersionDetails.length < 2) {
      this.snackbarService.openSnackbar(ResponseMessage.COMPARE_ITEM, ResponseMessageTypes.WARNING);
    }
    else {
      this.compareVersionDetails.forEach((res: any) => {
        this.versionCompare.push({ entityTypeAttributeId: res?.entity_type_attribute_id, attributeValue: res?.version, entityId: res?.entity_id, id: res?.id, entityTypeId: res?.entity_type_id })
      })

      this.crudService.post_version(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.CREATE_POLICY_VERSION}`, this.versionCompare)
        .pipe(untilDestroyed(this)).subscribe((res: any) => {
          this.showCompareVersion = true;
          this.compareHtmlFile = res;
          this.selection.clear();
          this.commonService.setLoaderShownProperty(false);
        })

    }
  }
  openReasonPopEdit(status:any) {
    this.currentUser = this.authenticationService.currentUserValue;
    if (this.currentRouterValues?.RadioDisplayedAttributesID != undefined) {
        this.getForkJoinRequests(status);
    } else {
      this.snackbarService.openSnackbar(ResponseMessage.PERMISSION_DENIED, ResponseMessageTypes.WARNING);
      return
    }
  }

  getForkJoinRequests(selectedStatus:any): void {
    const tabName = "Policy Status";
    forkJoin([
      this.crudService.get_params(`${appModels.COMPONENT_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${this.currentRouterValues?.RadioDisplayedAttributesID}`).pipe(untilDestroyed(this))
        .pipe(untilDestroyed(this))
    ]).subscribe((response: any) => {
      this.commonService.setLoaderShownProperty(false);
      response.forEach((respObj: any, ix: number) => {
        if (respObj) {
          switch (ix) {
            case 0:
              this.radioresponse = respObj;
              this.openeditDialog(this.radioresponse, tabName, appModels.ATTRIBUTE_VALUE +`/${this.labelNames[this.selectedIndex].dynamicAPIUrl}`,selectedStatus);
              break;
          }
        }
      });
    })
  }
  //Reason Dialog
  openeditDialog(response: any, header: any, api: any,selectedStatus:any) {
    const dialogRef = this.dialog.open(PolicyReasonPopupComponent, {
      width: '50vw',
      height: 'auto',
      data: {
        result: response,
        headers: header,
        api: api,
        entityID: this.currentRouterValues?.entityId,
        entityTypeId: this.currentRouterValues?.entityTypeId,
        key: this.currentRouterValues?.key,
        policyInfolistStatus: this.policyInfolistStatus,
        selectedStatusName:selectedStatus,
        moduleName:policyLegislationListTabList[this.currentRouterValues?.selectedIndex].name
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.reasonresponse = data;
      }
    });
  }

  getApprovalHistory() {
    this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.APPROVED_POLICY}`, { params: { entityId: this.currentRouterValues?.entityId, entityTypeId: this.currentRouterValues?.entityTypeId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      response.length > 0 ? this.dataSource = new MatTableDataSource<any>(response) : this.dataSource.filteredData.length = 0;
      this.commonService.setLoaderShownProperty(false);
    })
  }
  
  onKeyuptextarea(val: any) {
    this.commonService.onKeyUpTextarea(val);
  }

  editDocumentText(type?:any){
      this.crudService.get_params(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/edit`, { params: { entityTypeId:  this.currentRouterValues?.entityTypeId, entityId:this.currentRouterValues?.entityId} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        response?.forEach((result:any) => {
          if(result?.componentName == AttributeType?.RICH_TEXT_EDITOR){
            this.showEditRichTextEditor = true;
            this.showEditFiles = false;
            this.editorResponse = result;
            this.editorValue = result?.attributeValue;
            this.commonService.setLoaderShownProperty(false);
          }
          else if(result?.componentName == AttributeType?.UPLOAD && type){
            this.showEditRichTextEditor = false;
            this.showEditFiles = true;
            this.showDeleteIcons = true;
            this.editorResponse = result;
            this.commonService.setLoaderShownProperty(false);
          }
          else if(result?.componentName == AttributeType?.TEXT_BOX && result?.isKey == false && type){
            this.showEditRichTextEditor = false;
            this.showEditFiles = true;
            this.editoDescResponse = result;
            this.editorDescValue = result?.attributeValue;
            this.commonService.setLoaderShownProperty(false);
          }
        })
      })
  }

  updateDocumentText(updateValues:any){
    this.updateDocumentTextValues = [];
    this.updateDocumentTextValues.push({
      "entityTypeId": this.currentRouterValues?.entityTypeId,
      "entityId": this.currentRouterValues?.entityId,
      "entityTypeAttributeId":  this.editorResponse.entityTypeAttributeId,
      "id":  this.editorResponse.attributeValueId ?  this.editorResponse.attributeValueId : "",
      "attributeValue": updateValues,
      "isKey": this.editorResponse.isKey
    })
    this.crudService.update(`${appModels.ATTRIBUTE_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}`, this.updateDocumentTextValues)
    .pipe(untilDestroyed(this)).subscribe(res => {
      this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
      this.commonService.setLoaderShownProperty(false);
      this.onSelectedTab(this.selectedIndex);
      this.showEditRichTextEditor = false;
    }) 
  }

  cancelDocumentText(){
    this.showEditRichTextEditor = false;
  }

  onFileSelected(val:any){
    setInterval(()=> {
      if(this.progress < 100.000){
        this.progress = this.progress + 0.1;
        this.hideProgressBar = true;
      }
      else{
        this.progress = 100;
        this.hideProgressBar = false;
      }
    }, 0);
    let FileData = val.target.files;
    const supportedExtensions = supportedFileType;
    for (let i = 0; i < FileData.length; i++) {
      this.selectedFile = FileData[i];
      const path = this.selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === this.selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(this.selectedFile);
          this.listOfFiles.push(this.selectedFile);
        }
        this.progress = 0;
        if (this.fileList.length == this.maxFilesUpload) {
          this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
          return;
        }
      } else { 
        this.snackbarService.openSnackbar(ResponseMessage.FILE_TYPE, ResponseMessageTypes.WARNING);
      }
    }
  }

  uploadDocumentFiles(values:any){
    this.updateFileDescription = [];
    this.updateFileDescription.push({
      "entityTypeId": this.currentRouterValues?.entityTypeId,
      "entityId": this.currentRouterValues?.entityId,
      "entityTypeAttributeId":  this.editoDescResponse.entityTypeAttributeId,
      "id":  this.editoDescResponse.attributeValueId ?  this.editoDescResponse.attributeValueId : "",
      "attributeValue": values,
      "isKey": this.editoDescResponse.isKey
    })
    if(this.editoDescResponse?.attributeValue != values){
      this.crudService.update(`${appModels.ATTRIBUTE_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}`, this.updateFileDescription)
      .pipe(untilDestroyed(this)).subscribe(res => {
        this.commonService.setLoaderShownProperty(false);
        this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
        this.onSelectedTab(this.selectedIndex);
        this.showEditFiles = false;
        this.showDeleteIcons = false;
      }) 
    }

    let formData = new FormData();
    if(this.listOfFiles.length >0){
      this.listOfFiles.forEach((files: any) => {
        formData.append('supportingDocuments', files);
      })
    
      formData.append('entityId', this.currentRouterValues?.entityId);
      formData.append('entityTypeId', this.currentRouterValues?.entityTypeId);
      formData.append('entityTypeAttributeId', this.editorResponse.entityTypeAttributeId);
      formData.append('isTemplateAttribute', "false");

      this.crudService.edit_upload_File(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.EDIT_FILE}`,formData).pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
        this.commonService.setLoaderShownProperty(false);
          this.onSelectedTab(this.selectedIndex);
          this.showEditFiles = false;
          this.showDeleteIcons = false;
          this.listOfFiles = [];
      })
    }
    if(this.editoDescResponse?.attributeValue == values && this.listOfFiles.length == 0){
      this.snackbarService.openSnackbar(ResponseMessage.NO_CHANGES, ResponseMessageTypes.WARNING);
    }
  }

  cancelDocumentFiles(){
    this.showEditFiles = false;
    this.showDeleteIcons = false;
  }

  deleteFiles(files:any){
    const moduleName = ConstantValue?.PolicyFile;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: ConstantValue?.PolicyFile,
        moduleName: moduleName,
        api: `${appModels.ENTITIES}/${files?.entityTypeName}/${appModels.DELETE_FILE}`,
        id: files.id,
        fileLength:this.uploadedFileList.length,
        fileDesc:this.editoDescResponse,
        fileDescApi:`${appModels.ATTRIBUTE_VALUE}/${files?.entityTypeName}/${appModels.DELETEINBATCH}`,
        entityId:this.currentRouterValues?.entityId
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        this.onSelectedTab(this.selectedIndex);
        this.showEditFiles = false;
        this.showDeleteIcons = false;
      }
    });
    
  }

  removeSelectedFile(index: any, files: any, type?:any) {
    if (files?.id) {
      this.crudService.delete(`${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.DELETE_FILE}/${files.id}`)
        .pipe(untilDestroyed(this)).subscribe((res: any) => {
          type == undefined && this.listOfFiles.splice(index, 1);
          this.commonService.setLoaderShownProperty(false);
          this.snackbarService.openSnackbar(ResponseMessage.DELETE, ResponseMessageTypes.SUCCESS);
        })
    }
    else {
      type == undefined && this.listOfFiles.splice(index, 1);
      this.fileList.splice(index, 1);
    }
  }
  downloadFiles(files: any) {
    this.commonService.downloadFile(files,this.labelNames[this.selectedIndex].dynamicAPIUrl);
  }
}
