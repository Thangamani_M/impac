import { Component, OnInit, Inject } from '@angular/core';
import { CrudService, CommonService, ResponseMessageTypes, SnackbarService, ResponseMessage} from 'src/app/services';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-policy-reason-popup',
  templateUrl: './policy-reason-popup.component.html',
  styleUrls: ['./policy-reason-popup.component.scss']
})
export class PolicyReasonPopupComponent implements OnInit {
  entityTypeId: number;
  updateFormValues: any = [];
  policyInfolistStatus: any;
  selectesStatusList:any;

  constructor(private crudService: CrudService, private router: Router, private commonService: CommonService, private snackbarService: SnackbarService, public dialog: MatDialog,  @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<PolicyReasonPopupComponent>) {
    this.entityTypeId =  this.data.entityTypeID;
    this.policyInfolistStatus = this.data.policyInfolistStatus;
    this.data?.result?.forEach((res:any) => {
      if(res?.componentValue == this.data?.selectedStatusName){
        this.selectesStatusList = res;
      }
    })
  }

  ngOnInit(): void {
  }

  update() {
    this.updateFormValues = [];
      this.updateFormValues.push({
        "entityTypeId": this.data.entityTypeId,
        "entityId": this.data.entityID,
        "entityTypeAttributeId": this.selectesStatusList.entityTypeAttributeId,
        "id": "",
        "attributeValue": this.selectesStatusList.entityId,
        "isKey": this.data.key
      })
    this.crudService.update(this.data.api, this.updateFormValues)
    .pipe(untilDestroyed(this)).subscribe(res => {
        this.dialogRef.close({ result: res, updatevalue: this.updateFormValues });
        this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
        this.router.navigate([`policies-legislation/policies-legislation-list`]);
        this.commonService.setLoaderShownProperty(false);
      })
  }
  //close
  onNoClick(): void {
    this.dialogRef.close();
  }

}
