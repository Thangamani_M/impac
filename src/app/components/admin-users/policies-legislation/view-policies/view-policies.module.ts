import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewPoliciesRoutingModule } from './view-policies-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { ViewPoliciesComponent } from './view-policies.component';
import { PolicyReasonPopupComponent } from './policy-reason-popup/policy-reason-popup.component';


@NgModule({
  declarations: [ViewPoliciesComponent, PolicyReasonPopupComponent],
  imports: [
    CommonModule,
    ViewPoliciesRoutingModule,
    SharedModule
  ]
})
export class ViewPoliciesModule { }
