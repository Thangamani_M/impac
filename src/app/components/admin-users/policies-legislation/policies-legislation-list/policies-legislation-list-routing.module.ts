import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PoliciesLegislationListComponent} from 'src/app/components/admin-users/policies-legislation/policies-legislation-list/policies-legislation-list.component';
import { Shared } from "src/app/services";


const routes: Routes = [
  Shared.childRoutes([
    { path: 'policies-legislation-list', component: PoliciesLegislationListComponent },
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoliciesLegislationListRoutingModule { }
