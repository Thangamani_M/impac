import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PoliciesLegislationListRoutingModule } from './policies-legislation-list-routing.module';
import { PoliciesLegislationListComponent} from 'src/app/components/admin-users/policies-legislation/policies-legislation-list/policies-legislation-list.component';
import { SharedModule } from 'src/app/components/shared/shared.module';

@NgModule({
  declarations: [PoliciesLegislationListComponent],
  imports: [
    CommonModule,
    PoliciesLegislationListRoutingModule,
    SharedModule
  ]
})
export class PoliciesLegislationListModule { }
