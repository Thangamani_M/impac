import { Component, OnInit } from '@angular/core';
import { DialogComponent, DeleteComponent } from 'src/app/components/shared';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudService, appModels, ResponseMessageTypes, SnackbarService,CommonService, ResponseMessage, AttributeType, policyLegislationListTabList, defaultSortingBy, menuNameList } from 'src/app/services';
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../../../environments/environment';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-policies-legislation-list',
  templateUrl: './policies-legislation-list.component.html'
})
export class PoliciesLegislationListComponent implements OnInit {

  pageSize:number = 10;
  pageNo:number = 0;
  defaultSortby = defaultSortingBy;
  labelNames: Array<any> = policyLegislationListTabList;
  selectedIndex:number = 0;
  displayedColumns: Array<any> = [];
  dataSource = new MatTableDataSource<any>([]);
  entityTypeID: number;
  entityID: number;
  selectedRow: any;
  deletedIds: Array<any> = [];
  searchKey = '';
  sorting: any;
  pageEvent: any;
  customTableColumnSize: any;
  RadioDisplayedAttributesID: any;
  entityTypeAttribute: any;
  iskey: any;

  defaultSelectDisplayedColumns: string[] = ['select'];
  defaultActionDisplayedColumns: string[] = ['Action','search'];
  attributeTypeValue = AttributeType;
  displayedColumnswidth: string[] = [];

  
  constructor(private router: Router,private snackbarService: SnackbarService,private activeRoute: ActivatedRoute, private crudService: CrudService, public dialog: MatDialog, private commonService :CommonService) {
    this.activeRoute.queryParamMap.subscribe((params: any) => {
      this.selectedIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    })
  }

  ngOnInit(): void {
    this.onSelectedTab(this.selectedIndex);
  }

  onSelectedTab(event: any) {
    this.selectedIndex = event;
    this.searchKey = '';
    this.displayedColumns = [];
    this.dataSource = new MatTableDataSource<any>([]);
    this.selectedRow != undefined ? this.selectedRow.clear() : this.selectedRow;
    this.router.navigate([`../policies-legislation-list`], { relativeTo: this.activeRoute, queryParams: { tab: this.selectedIndex } });
    this.getDisplayedColumns();
    this.commonService.setbreadcrumb([{ title: this.labelNames[this.selectedIndex]?.name }]);
  }

  getDisplayedColumns() {
    this.displayedColumnswidth = [];
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if (response.length > 0) {
        if(this.selectedIndex == 0){
            this.customTableColumnSize = 1;
        }else{
            this.customTableColumnSize = this.commonService.setTableColumnSize(response.length);
        }
        response?.map((columnNames: any) => {
          this.displayedColumns.push(columnNames.entityTypeAttributeName);
          if (columnNames.componentName == this.attributeTypeValue?.CHECK_BOX) {
            this.RadioDisplayedAttributesID = columnNames.entityTypeAttributeId;
            this.iskey = columnNames.isKey
            this.entityTypeAttribute = columnNames.entityTypeAttributeName
          }
        })
        this.displayedColumns = this.defaultSelectDisplayedColumns.concat(this.displayedColumns);
        this.displayedColumns = this.displayedColumns.concat(this.defaultActionDisplayedColumns);
        this.getDataSource();
      }
    })
  }
  getDataSource(event?: any, searchKey?: any, sortBy?: any) {
    this.pageEvent = event;
    if (this.pageEvent) {
      let { pageSize, pageIndex, length } = this.pageEvent['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else {
      this.pageNo = 0;
    }
    searchKey = searchKey ? searchKey : '';
    sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;

    this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${this.entityTypeID}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction,searchKey:searchKey} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.dataSource = new MatTableDataSource(response);
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onSearched(event: any) {
    this.searchKey = event;
    this.getDataSource(this.pageEvent, this.searchKey)
  }

  onSorted(eve: any) {
    this.sorting = eve;
    if (this.sorting.direction == 'asc' || this.sorting.direction == 'desc') {
      this.getDataSource(this.pageEvent, this.searchKey, this.sorting);
    }
  }

  onPageChanged(event: any) {
    this.pageEvent = event;
    this.getDataSource(event, this.searchKey);
  }


  onCreateEditClicked(values?: any) {
      if(values){
        this.entityID = values.EntityId;
          const methodName = 'Edit '+this.labelNames[this.selectedIndex].name;
          this.router.navigate(['policies-form/edit-form'], {queryParams: { entityID: this.entityID,pagetitle :this.labelNames[this.selectedIndex].name,entityTypeID: this.entityTypeID,methodName:methodName,selectedIndex:this.selectedIndex,dynamicApiUrl:this.labelNames[this.selectedIndex].dynamicAPIUrl,isEdit:true,menuName:menuNameList?.POLICIES},skipLocationChange:environment.SkipLocationChange});
      }
      else{
        const methodName = 'Create '+this.labelNames[this.selectedIndex].name;
        this.router.navigate(['policies-form/create-form'], {queryParams: { entityTypeID: this.entityTypeID,pagetitle :this.labelNames[this.selectedIndex].name,methodName:methodName,selectedIndex:this.selectedIndex,menuName:menuNameList?.POLICIES,dynamicApiUrl:this.labelNames[this.selectedIndex].dynamicAPIUrl},skipLocationChange:environment.SkipLocationChange});
      }
  }

  openCreateEditDialog(response: any, header: any, api: any, isEdit = false, entityId?: any) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '50vw',
      height: 'auto',
      data: {
        result: response,
        headers: header,
        api: api,
        edit: isEdit,
        entityId: entityId
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.getDataSource()
      }
    });
  }

  onSelected(event: any) {
    this.deletedIds = []
    this.selectedRow = event;
    this.selectedRow._selected?.map((element: any) => {
      this.deletedIds.push({ id: element.EntityId });
    });
  }

  // Delete POP UP
  openDeleteDialog() {
    if (this.deletedIds == undefined || this.deletedIds.length == 0) {
      this.snackbarService.openSnackbar(ResponseMessage.SELECT_ITEM, ResponseMessageTypes.WARNING);
      return;
    }
    const moduleName = this.labelNames[this.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        result: this.selectedRow,
        id: this.deletedIds,
        moduleName: moduleName,
        api: `${appModels.ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.DELETEINBATCH}`,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        data.clear();
        this.getDataSource();
        this.deletedIds = [];
      }
    });
  }

  exportFile(entityname:any,entityId:any){
    let selectedRecordIds: any = [];
    if(this.selectedRow?._selected?.length > 0){
      this.selectedRow != undefined && this.selectedRow?._selected?.forEach((res:any) =>{
        selectedRecordIds.push(res.EntityId);
      })
      this.commonService.onFileDownload(entityname,entityId,selectedRecordIds);
    }
    else{
      this.commonService.onFileDownload(entityname,entityId);
    }
  }
}


