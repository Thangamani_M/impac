import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyProfileRoutingModule } from './my-profile-routing.module';

/** Custom Component,Service */
import { MyProfileComponent } from 'src/app/components/admin-users/my-profile/my-profile/my-profile.component';
import { SharedModule } from 'src/app/components/shared/shared.module';

@NgModule({
  declarations: [MyProfileComponent],
  imports: [
    CommonModule,
    MyProfileRoutingModule,
    SharedModule
  ]
})
export class MyProfileModule { }
