import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CrudService, appModels, AuthenticationService, CommonService, AttributeType, myProfileTabList } from 'src/app/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
  encapsulation: ViewEncapsulation.Emulated

})
export class MyProfileComponent implements OnInit {

  selectedIndex: number = 0;
  currentUser: any;
  userId: number;
  labelNames: Array<any> = myProfileTabList;
  entityTypeID: number;
  profileForm: FormGroup;
  userDetails: any;
  formResponse: any;
  dropDownResponse: Array<any> = [];
  isDisabled: boolean = false;
  attributeTypeValue = AttributeType;


  constructor(private router: Router, private formBuilder: FormBuilder, private authService: AuthenticationService, private crudService: CrudService, private commonService: CommonService) {
    this.currentUser = this.authService.currentUserValue;
    this.userId = this.currentUser.userId;
    this.commonService.setbreadcrumb([{ title: "My Profile" }]);
  }

  ngOnInit(): void {
    this.onSelectedTab(this.selectedIndex);
  }

  onSelectedTab(event: any) {
    if (event == 1) {
      this.router.navigate(['/profile/my-skillset']);
      return;
    }
    this.selectedIndex = event;
    this.getDisplayedColumns();
    this.getUserDetails();
  }

  getDisplayedColumns() {
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.formResponse = response;
        let dropdownAPI: any = [];
        this.formResponse?.forEach((res: any) => {
          if (res?.componentName == this.attributeTypeValue?.DROP_DOWN) {
            dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.entityTypeAttributeId}`));
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI);
        this.profileForm = this.createFormGroup();
         this.isDisabled = true;
      this.commonService.setLoaderShownProperty(false);
    })
  }

   // Load Multiple DropDown
   onLoadMultipleDropDown(Api: any, isEdit = false) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownResponse = results.flat(1);
      this.formResponse?.forEach((res: any) => {
        if (res.componentName == this.attributeTypeValue?.DROP_DOWN) {
          this.profileForm.get(res?.entityTypeAttributeName)?.setValue(+parseInt(this.userDetails?.[res?.entityTypeAttributeName]));
        }
        else {
          this.profileForm.get(res?.entityTypeAttributeName)?.setValue(this.userDetails?.[res?.entityTypeAttributeName]);
        }
      })
      this.commonService.setLoaderShownProperty(false);
    })
  }

  getUserDetails() {
    this.crudService.get(`${appModels.USER_MANAGEMENT}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${this.userId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.userDetails = response[0];
      this.commonService.setLoaderShownProperty(false);
    })
  }

  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.formResponse?.forEach((column: any) => {
      group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
    });
    return group;
  }

}
