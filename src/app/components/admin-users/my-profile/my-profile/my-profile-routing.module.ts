import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MyProfileComponent } from 'src/app/components/admin-users/my-profile/my-profile/my-profile.component';
import { Shared } from "src/app/services";

const routes: Routes = [
  Shared.childRoutes([
    { path: 'my-profile', component: MyProfileComponent },
    { path: 'my-skillset', loadChildren: () => import('./../my-skill-set/my-skill-set.module').then(m => m.MySkillSetModule) }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyProfileRoutingModule { }
