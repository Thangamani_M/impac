import { Component, OnInit,ViewEncapsulation,ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { appModels, CrudService, SnackbarService, ResponseMessageTypes, ResponseMessage,AuthenticationService, CommonService, AttributeType, mySkillsetTabList, defaultSortingBy, supportedFileType, previewFileType,menuNameList } from 'src/app/services';
import { MatDialog } from '@angular/material/dialog';
import { DeleteComponent,PreviewComponent } from 'src/app/components/shared';
import { forkJoin } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-my-skill-set',
  templateUrl: './my-skill-set.component.html',
  styleUrls: ['./my-skill-set.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
 
})
export class MySkillSetComponent implements OnInit {
  selectedIndex: number = 1;
  currentUser: any;
  userId: number;
  labelNames: Array<any> = mySkillsetTabList;
  skillSetForm: FormGroup = new FormGroup({});
  entityTypeID: number;
  displayedColumns: string[] = [];
  dropDownResponse: Array<any>;
  skillList: Array<any> = [];
  listOfSkills:any;
  pageSize:number = 10;
  pageNo:number = 0;
  paginationResult:any;
  defaultSortby = defaultSortingBy;
  searchKey = '';
  formData = new FormData();
  DeletedIds:Array<any> =[];
  attributeTypeValue = AttributeType;
  getSupportingDocuments : any;
  objRequest:any = [];
  selectedStatus:string = 'All';
  pageEvent: any;
  defaultActionDisplayedColumns: string[] = ['Action'];
  fileLists:any = [];


  constructor(private router: Router, private formBuilder: FormBuilder, private crudService: CrudService,private commonService: CommonService, public dialog: MatDialog, private snackbarService: SnackbarService,private authenticationService: AuthenticationService,public sanitizer: DomSanitizer,private readonly changeDetectorRef: ChangeDetectorRef,private datepipe: DatePipe) {
    this.commonService.setbreadcrumb([{title:"My Profile"}]);
  }

  ngAfterViewChecked(): void { 
    this.changeDetectorRef.detectChanges();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService?.currentUserValue;
    this.userId = this.currentUser?.userId;
    this.createSkillsetForm();
    this.onSelectedTab(this.selectedIndex);
  }

  onSelectedTab(event: any) {
    if (event == 0) {
      this.router.navigate(['/profile/my-profile']);
      return;
    }
    this.selectedIndex = event;
    this.getDisplayedColumns();
  }

  getDisplayedColumns() {
    this.skillList = [];
    this.displayedColumns = [];
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      let dropdownAPI: any = [];
      response?.forEach((res: any) => {
        res?.isAdmin == false &&  res?.componentName != this.attributeTypeValue?.TEXT_BOX && res?.componentName != this.attributeTypeValue?.PREVIEW && this.displayedColumns.push(res.entityTypeAttributeName);
        res?.componentName == this.attributeTypeValue?.TEXT_FIELD && this.displayedColumns.push(res.entityTypeAttributeName);
        res?.componentName == this.attributeTypeValue?.CHECK_BOX && this.displayedColumns.push(res.entityTypeAttributeName);
        res?.isAdmin == true && res?.componentName == this.attributeTypeValue?.TEXT_BOX && this.displayedColumns.push(res.entityTypeAttributeName);
       
        res?.componentName == this.attributeTypeValue?.DROP_DOWN &&  dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${res.entityTypeAttributeId}`));

        if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.UPLOAD || res?.componentName == this.attributeTypeValue?.PREVIEW || res?.componentName == this.attributeTypeValue?.DATE_TIME || res?.componentName == this.attributeTypeValue?.TEXT_AREA || res?.componentName == this.attributeTypeValue?.TEXT_FIELD ) {
          this.skillList.push(res);
        }
      })
      this.displayedColumns = this.displayedColumns.concat(this.defaultActionDisplayedColumns);

      this.createSkillsetForm();
      this.selectedStatus == 'Open' && this.createFormArray(this.skillList);
      this.getSkillset();
      this.onLoadMultipleDropDown(dropdownAPI);
      this.commonService.setLoaderShownProperty(false);
    })
  }

   // Load Multiple DropDown
   onLoadMultipleDropDown(Api: any) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownResponse = results.flat(1);
      this.commonService.setLoaderShownProperty(false);
    })
  }

  createFormArray(argGroup: any) {
    const group = this.formBuilder.group({});
    argGroup?.forEach((column: any) => {
      column?.componentName == this.attributeTypeValue?.CHECK_BOX &&
      group.addControl(column?.entityTypeAttributeName, this.formBuilder.control('Open')) || 
      column?.componentName != this.attributeTypeValue?.PREVIEW && group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue,Validators.required)) ||
      group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
    });
    this.getskillSetsArray.push(group);

  }

  createSkillsetForm() {
    this.skillSetForm = this.formBuilder.group({
      skillSetsArray: this.formBuilder.array([])
    });
  }

  get getskillSetsArray(): FormArray {
    return this.skillSetForm.get('skillSetsArray') as FormArray;
  }

  addGroup(i:any) {
    this.objRequest = [];
    if (this.skillSetForm.invalid) {
      this.skillSetForm.markAllAsTouched();
      this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
      return;
    }
    let formData = new FormData();
    let jsonObj: any = {};
    let jsonObjDateType: any = {};
    let jsonObjTextAreaType: any = {};
    let jsonObjTextField: any = {};
    let jsonObjTextBox: any = {};
    formData.append('entityTypeId', this.labelNames[this.selectedIndex]?.value);
    Object.entries(this.getskillSetsArray?.getRawValue()[i])?.forEach(([key, value]: any, index) => {
      const entityTypeAttribute = this.skillList?.find((attr: any) => attr?.entityTypeAttributeName == key);

      if (entityTypeAttribute?.componentName == this.attributeTypeValue?.UPLOAD) {
        formData.append('entityTypeAttributeId', entityTypeAttribute?.entityTypeAttributeId);
        formData.append('supportingDocuments', value?.files[0]);
      } 
      else if (entityTypeAttribute?.componentName == this.attributeTypeValue?.DROP_DOWN) {
        jsonObj['entityTypeAttributeId'] = entityTypeAttribute?.entityTypeAttributeId;
        jsonObj['attributeValue'] = value;
        this.objRequest.push(jsonObj);
      }
      else if (entityTypeAttribute?.componentName == this.attributeTypeValue?.DATE_TIME) {
        jsonObjDateType['entityTypeAttributeId'] = entityTypeAttribute?.entityTypeAttributeId;
        jsonObjDateType['attributeValue'] = this.datepipe.transform(value, 'yyyy-MM-dd');
        this.objRequest.push(jsonObjDateType);
      }
      else if (entityTypeAttribute?.componentName == this.attributeTypeValue?.TEXT_AREA) {
        jsonObjTextAreaType['entityTypeAttributeId'] = entityTypeAttribute?.entityTypeAttributeId;
        jsonObjTextAreaType['attributeValue'] = value;
        this.objRequest.push(jsonObjTextAreaType);
      }
      else if (entityTypeAttribute?.componentName == this.attributeTypeValue?.TEXT_FIELD) {
        jsonObjTextField['entityTypeAttributeId'] = entityTypeAttribute?.entityTypeAttributeId;
        jsonObjTextField['attributeValue'] = value;
        this.objRequest.push(jsonObjTextField);
      }
      else if (entityTypeAttribute?.componentName == this.attributeTypeValue?.TEXT_BOX) {
        jsonObjTextBox['entityTypeAttributeId'] = entityTypeAttribute?.entityTypeAttributeId;
        jsonObjTextBox['attributeValue'] = value;
        this.objRequest.push(jsonObjTextBox);
      }
    }); 
    formData.append('skillsetDto', JSON?.stringify(this.objRequest));
    const supportedExtensions = supportedFileType;
    this.getSupportingDocuments = formData.get('supportingDocuments');
    const path = this.getSupportingDocuments?.name?.split('.');
    const extension = path[path?.length - 1];
    supportedExtensions.includes(extension.toLowerCase()) &&this.crudService.post(`${appModels.   ENTITIES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.SKILLSET_UPLOAD}`,   formData).pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
        this.getDisplayedColumns();
        this.commonService.setLoaderShownProperty(false);
    }) ||  this.snackbarService.openSnackbar(ResponseMessage.FILE_TYPE, ResponseMessageTypes.WARNING);
  }

  removeGroup(i: number,id:any) {
    this.DeletedIds = [];
    if(id != undefined){
      this.DeletedIds.push({ "id": + parseInt(id) });
      this.openDeleteDialog();
    }
  }

  openDeleteDialog() {
    const moduleName = this.labelNames[this.selectedIndex].name;
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: { result: "skills",
        moduleName: moduleName,
        api: `${appModels.ENTITIES +`/${this.labelNames[this.selectedIndex].dynamicAPIUrl}` +`/${appModels.DELETEINBATCH}`}`,
        id: this.DeletedIds,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        this.getDisplayedColumns();
        this.DeletedIds = [];
      }
    });
  }

  cancel() {
    this.router.navigate([`../case-management/case-mangement-list`], { queryParams: { tab: 0 } });
  }

  getSkillset(event?:any,sortBy?:any){
    this.fileLists = [];
    this.pageEvent = event;
    if (this.pageEvent) {
      let { pageSize, pageIndex, length } = this.pageEvent['params'];
      this.pageSize = pageSize;
      this.pageNo = pageIndex;
    }
    else {
      this.pageNo = 0;
    }
    sortBy == undefined ? sortBy = this.defaultSortby : sortBy = sortBy;
    let statusKey;
    statusKey =  this.selectedStatus == 'All' ? '' : this.selectedStatus;
    let userId = this.currentUser?.userId;
      this.crudService.get_params(`${appModels.SKILLS}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.USER}/${userId}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: sortBy.active, direction: sortBy.direction, searchKey: this.searchKey, status:statusKey}}).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.listOfSkills = response?.Entities;
      this.paginationResult = response;
      this.listOfSkills?.forEach((data:any)=>{
          if(data.Upload){
           let uploadedFiles = Object.assign({}, ...JSON.parse(data.Upload));
           this.fileLists.push(uploadedFiles);
          }
      })
      this.commonService.setLoaderShownProperty(false);
    })
  }

  onPageChanged(event: any) {
    this.getSkillset(event);
    this.getskillSetsArray.clear();
    this.selectedStatus == 'Open' && this.createFormArray(this.skillList);
  }

  onKeyuptextarea(val: any) {
    this.commonService.onKeyUpTextarea(val);
  }
    // key press
  onkeypress(e: any) {
    this.commonService.onkeyUpTextbox(e);
  }
  
  onChangeStatus(val:any){
    this.selectedStatus = val;
    this.getskillSetsArray.clear();
    this.getSkillset();
    this.selectedStatus == 'Open' && this.createFormArray(this.skillList);
  }

  onSortColumn(event:any){
    this.getSkillset(this.pageEvent,event)
  }
  download(files:any){
    const dialogRef = this.dialog.open(PreviewComponent, {
      width: '40vw',
      height: '40vw',
      data: {files:files ? files : null,
            download:false,
            delete:false,
            dynamicAPIurl:this.labelNames[this.selectedIndex].dynamicAPIUrl},
      disableClose: true
    });
    
    dialogRef.afterClosed().subscribe((data) => {
      this.commonService.setLoaderShownProperty(false);
    });
  }
}