import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Custom Component,Service */
import { SharedModule } from 'src/app/components/shared/shared.module';
import { MySkillSetComponent } from './my-skill-set.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [MySkillSetComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
        {path: '', component: MySkillSetComponent}
    ])
  ]
})
export class MySkillSetModule { }
