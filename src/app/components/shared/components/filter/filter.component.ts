import { Component, OnInit, Inject} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { forkJoin } from 'rxjs';
import { CrudService, appModels, CommonService, ResponseMessageTypes, SnackbarService,ResponseMessage, AttributeType, caseManagement_List_View_TabList } from 'src/app/services';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  FilterDisplayesColumns: any = [];
  templateFormObj: any = [];
  FilterForm: FormGroup = new FormGroup({});
  dropDownResponse: any = [];
  formResponse: any;
  public formData = new FormData();
  templateFormValues: any = [];
  entityAttributesValues: any = [];
  createFormValues: any = [];
  labelNames: any = caseManagement_List_View_TabList;
  attributeTypeValue = AttributeType;
  entityTypeID:any;
  selectedIndex: number;

  constructor(private datepipe: DatePipe, private formBuilder: FormBuilder,  private crudService: CrudService, private snackbarService: SnackbarService, private commonService: CommonService,public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<FilterComponent>) {
      this.getFilterColumns(data?.selectedIndex);
  }

  ngOnInit(): void {
  }

   // FilterColumns
   getFilterColumns(index: any) {
    this.selectedIndex = index;
    let dropdownAPI: any = [];
    this.entityTypeID = this.labelNames[this.selectedIndex].value;
    this.crudService.get_params(`${appModels.ENTITY_TYPE_ATTRIBUTES}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.FILTER_HEADERS}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.formResponse = response;
      if (response.length > 0) {
        response?.map((columnNames: any) => {
          if (columnNames.componentName == this.attributeTypeValue?.DROP_DOWN || columnNames.componentName == this.attributeTypeValue?.DATE_TIME) {
            this.FilterDisplayesColumns?.push(columnNames);
            if(columnNames.componentName == this.attributeTypeValue?.DROP_DOWN){
                dropdownAPI.push(this.crudService.get(`${appModels.COMPONENT_VALUE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE_ATTRIBUTE}/${columnNames.entityTypeAttributeId}`));
            }
          }
        })
        this.onLoadMultipleDropDown(dropdownAPI);
        this.FilterForm = this.createFormGroup();
      }
    })
  }
  //Load Multiple DropDown
   onLoadMultipleDropDown(Api: any) {
    forkJoin(Api).subscribe((results: any) => {
      this.dropDownResponse = results.flat(1);
      this.commonService.setLoaderShownProperty(false);
    })
  }
  // Create Form
  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    this.formResponse?.forEach((column: any) => {
      if (column.componentName == this.attributeTypeValue?.DROP_DOWN || column.componentName == this.attributeTypeValue?.DATE_TIME) {
        group.addControl(column.entityTypeAttributeName, this.formBuilder.control(column.attributeValue));
      }
    });
    return group;
  }    
  // Filter
  createFilter() {
    this.templateFormValues = [];
    this.entityAttributesValues = [];
    this.createFormValues = {
      "entityTypeId": "",
      "entityAttributes": "",
    } 
    let newfordata = new FormData();
    this.templateFormObj = [];
    
    if (this.FilterForm.pristine) {
      this.FilterForm.markAllAsTouched();
      this.snackbarService.openSnackbar(ResponseMessage.NO_CHANGES, ResponseMessageTypes.WARNING);
      return;
    }
    Object.keys(this.FilterForm.value).forEach((key) => {
      const currentControl = this.FilterForm.controls[key];
      if (!currentControl.pristine && !currentControl.untouched) {
        this.templateFormObj.push({ [key]: this.FilterForm.value[key] });
      }
    });
    this.formResponse?.forEach((res: any, i: number) => {
      this.templateFormObj?.forEach((forms: any) => {
        if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
          res.attributeValue = Object.values(forms).toString();
          this.templateFormValues.push(res);
        }
      })
    })

    this.templateFormValues?.map((res: any) => {
      if (res?.componentName == this.attributeTypeValue?.DATE_TIME) {
        res.attributeValue = this.datepipe.transform(res?.attributeValue, 'yyyy-MM-ddThh:mm:ss');
      }
      this.entityAttributesValues.push({ 'entityTypeAttributeId': res.entityTypeAttributeId, 'attributeValue': res.attributeValue });
      this.createFormValues.entityTypeId = this.entityTypeID;
    })
    this.createFormValues.entityAttributes = this.entityAttributesValues;
    newfordata.append("filterPayloadRequest", JSON.stringify(this.createFormValues));
    this.crudService.post(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.FILTER}/${this.entityTypeID}`, newfordata)
      .pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.dialogRef.close({res, newfordata});
        // this.dialogRef.close(res);
        this.commonService.setLoaderShownProperty(false);
      })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
