import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CrudService, CommonService, ResponseMessageTypes, SnackbarService, ConstantValue, ResponseMessage} from 'src/app/services';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
  deleteAll:any;
  deleteFileDesc:any = [];
  
  constructor(private snackbarService: SnackbarService,private crudService: CrudService,private commonService: CommonService,public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,public dialogRef: MatDialogRef<DeleteComponent>) {
   }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  delete(){
    this.deleteFileDesc = [];
    this.deleteFileDesc.push({
      "entityId": this.data?.entityId,
      "entityTypeAttributeId":this.data?.fileDesc?.entityTypeAttributeId,
      "attributeValue":this.data?.fileDesc?.attributeValue,
      "id":this.data?.fileDesc?.attributeValueId
    })
    if(this.data?.moduleName != ConstantValue?.PolicyFile){
      this.deleteAll = JSON.stringify(this.data.id);
      this.deleteAll != undefined && this.crudService.delete_all(this.data.api,this.deleteAll)
      .pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.commonService.setLoaderShownProperty(false);
          this.snackbarService.openSnackbar(res.result, ResponseMessageTypes.SUCCESS);
          this.dialogRef.close(this.data.result);
      }) || this.dialogRef.close('');
    }
    else{
      this.data?.id != undefined && this.crudService.delete(`${this.data.api}/${this.data?.id}`)
      .pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.commonService.setLoaderShownProperty(false);
        this.snackbarService.openSnackbar(ResponseMessage.DELETE, ResponseMessageTypes.SUCCESS);
        this.dialogRef.close("delete");
      })  || this.dialogRef.close('');

      if(this.data?.fileLength == 1){
        this.crudService.delete_all(this.data?.fileDescApi,JSON.stringify(this.deleteFileDesc))
        .pipe(untilDestroyed(this)).subscribe((res: any) => {
        })
      }
    } 
  }

}
