import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { helpPageDetails} from "../../../../services";

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<HelpComponent>) { }

  helpDetails = helpPageDetails;

  ngOnInit(): void {
  }

  openLink(value:any){
    window.open(value);
  }

/** Close the Dialog Function*/
  onNoClick(): void {
    this.dialogRef.close();
  }

}
 