import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef} from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { SelectionModel} from '@angular/cdk/collections';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from '@angular/router';
import { CrudService, appModels, CommonService, SnackbarService,ResponseMessageTypes, ResponseMessage, AttributeType, DynamicAPIEndPoints,EntityTypeId,defaultSortingBy, adminTabList } from 'src/app/services';
import { PreviewComponent} from 'src/app/components/shared';
import { MatDialog} from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { StatusChangeDialogComponent } from '../status-change-dialog/status-change-dialog.component';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Output() onPageChanged = new EventEmitter();
  @Output() onCreateEditClicked = new EventEmitter();
  @Output() onRowCheck = new EventEmitter();
  @Output() onSearched = new EventEmitter();
  @Output() onSorted = new EventEmitter();

  @Input() observableData:any = [];
  @Input() displayedColumns: string[] = [];
  @Input() isToggle:any;
  @Input() entityTypeId:number;
  @Input() selectedIndex:number;
  @Input() searchKeyValue:string;
  @Input() customTableColumnSize : number;
  @Input() displayedColumnswidth :Array<any> = [];
  @Input() menuName:any = [];
  @Input() RadioDisplayedAttributesID :any;
  @Input() iskey :any;
  @Input() entityTypeAttribute :any;
  @Input() selectedTabName: any;
  @ViewChild("focusSearch") searchFocusEl: ElementRef;

  pageSize: number = 10;
  pageNo: number = 0;
  pageDetails:any;

  tableFormGroup:FormGroup = new FormGroup({});
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  showSearchFields:boolean = false;
  fileList:any = [];
  searchKey = '';
  defaultSortby = defaultSortingBy;

   dynamicUserAPIUrl:string = DynamicAPIEndPoints?.USER_MANAGEMENT_URL;
   dynamicPolicyAPIUrl:string = DynamicAPIEndPoints?.POLICIES_URL;
   attributeTypeValue = AttributeType;

   showPolicyTitleField:boolean = false; 
   showEditTextIcon:boolean = true; 
   showEditFileIcon:boolean = true; 
   editPolicy:any;
   updatePolicyValues:any = [];
   uploadFiles:any = [];
   formData = new FormData();

   constructor(private formBuilder: FormBuilder,private router: Router,private crudService: CrudService, private commonService: CommonService, private snackbarService: SnackbarService, public dialog: MatDialog) { }
 
  ngOnInit(): void {
    this.tableFormGroup = this.createFormGroup();
  }

  ngOnChanges() {
    this.tableFormGroup = this.createFormGroup();
    this.onloadDataSource();
    if(this.showSearchFields){
      this.onFocusSearchField();
    }
  }
  onloadDataSource(){
    this.dataSource = new MatTableDataSource(this.observableData?.filteredData?.Entities);
    this.getUploadFileList();
  }
  onPageChange(event:any) {
    this.pageDetails = event;
    let { pageSize,pageIndex,length } = event;
    this.pageSize = pageSize;
    this.pageNo = pageIndex;
    let params = event['params'];
    this.onPageChanged.emit({ params });
  }

  onSortColumn(event:any){
    this.onSorted.emit(event);
  }

  onFocusSearchField(){
    setTimeout(()=>{
      this.searchFocusEl?.nativeElement?.focus();
    },100);
  }

  search(){
    this.onSearched.emit(this.searchKeyValue)
  }

  showSearch(){
    this.showSearchFields = true;
    this.onFocusSearchField();
  }

  clear(){
    this.showSearchFields = true;
    this.searchKeyValue = '';
    this.onSearched.emit(this.searchKeyValue);
    this.tableFormGroup.reset();
    this.createFormGroup();
    this.onFocusSearchField();
  }

  close(){
    this.showSearchFields = false;
    this.searchKeyValue = '';
    this.onSearched.emit(this.searchKeyValue);
    this.tableFormGroup.reset();
    this.createFormGroup();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data?.forEach((row: any) =>
      this.selection.select(row));
  }

  checkboxLabel(row:any) {
    if (!row) {
      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${this.selection.isSelected(row) ? "deselect" : "select"}
    row ${row.position + 1}`;
  }

  onRowChecked() {
    let unselectedData;
    let { selected } = this.selection;
    this.onRowCheck.emit(this.selection);

    if (selected.length > 0)
      unselectedData = this.dataSource.data.filter((i:any) => !selected.includes(i));
  }

  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
      this.displayedColumns?.forEach(column => {
        if(column != 'select' && column != 'Action' && column != 'search' && column != 'Toggle'){
             group.addControl(column, this.formBuilder.control(""));
        }
      });
    return group;
  }

  onEditDeleteClick(row:any) {
      this.onCreateEditClicked.emit(row);
  }

  auditLogDetails(event:any){
    this.router.navigate([`audit-log/audit-log-details`],{queryParams: { EntityID: event.Entity_Id,EntityTypeId:event.Entity_Type_Id,menuName:this.menuName} });
  }


  entityID:any;
  onChangeStatus(event:any,value:any,entityTypeId:any){
    this.entityID = value.EntityId;
    const dialogRef = this.dialog.open(StatusChangeDialogComponent, {
      width: '600px',
      height: '300px',
      data: {
        dynamicAPIurl : this.dynamicUserAPIUrl,
        entityTypeID : this.entityTypeId,
        entityId: this.entityID,
        event:event,
        moduleName:this.selectedTabName
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data !== undefined) {
        if(value.Status != !event){
          value.Status = !event;
        }
        else{
          value.Status = value.Status.toString();
        }
      }
    });
  }


  caseManagementView(event:any){
    let isEscalate = false;
     if(this.entityTypeId == 3){
        isEscalate = true;
     }
    this.router.navigate([`case-management/case-management-view`],{queryParams: {entityId:event.EntityId, entityTypeId:this.entityTypeId,selectedIndex:this.selectedIndex, menuName:this.menuName, isEscalate: isEscalate} });
  }

  policiesView(event:any){
    this.router.navigate([`policies/view-policies`],{queryParams: {entityId:event.EntityId, entityTypeId:this.entityTypeId,selectedIndex:this.selectedIndex,RadioDisplayedAttributesID:this.RadioDisplayedAttributesID,key:this.iskey,status:this.entityTypeAttribute, menuName: this.menuName} });
  }

  skillsView(event:any){
    let User_Id = event?.User_Id;
    this.router.navigate([`skills/view-skills`],{queryParams: {userId:User_Id, menuName: this.menuName} });
  }
  publicIncidentView(event:any){
    this.router.navigate([`case-management/public-incident-view`],{queryParams: {entityId:event.EntityId, entityTypeId:this.entityTypeId,selectedIndex:this.selectedIndex, menuName:this.menuName} });
  }

  getUploadFileList(){
    this.fileList = [];
    this.dataSource.filteredData.forEach(element => {
      if(element?.['Upload Files here']  != null){
          this.fileList.push(JSON.parse(element?.['Upload Files here']));
      } 
      else{
        this.fileList.push(element?.['Upload Files here']);
      }
    });
  }

  getPolicyList(){
    if(this.pageDetails?.params != undefined){
      this.pageSize =  this.pageDetails.params.pageSize;
      this.pageNo = this.pageDetails.params.pageIndex;
    }
    else{
      this.pageSize = 10;
      this.pageNo = 0;
    }
      this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.dynamicPolicyAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${EntityTypeId?.POLICIES}`, { params: { pageSize:  this.pageSize, pageNo: this.pageNo, sortBy: this.defaultSortby.active, direction: this.defaultSortby.direction,searchKey:this.searchKey} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.dataSource = new MatTableDataSource(response?.Entities);
        this.getUploadFileList();
        this.commonService.setLoaderShownProperty(false);
      })
  }
  download(files:any){
    const dialogRef = this.dialog.open(PreviewComponent, {
      width: '40vw',
      height: '40vw',
      data: {files:files ? files : null,
            download:true,
            delete:true,
            dynamicAPIurl:this.dynamicPolicyAPIUrl},
      disableClose: true
    });
    
    dialogRef.afterClosed().subscribe((data) => {
       if(data == "delete"){
          this.getPolicyList();
       }
      this.commonService.setLoaderShownProperty(false);
    });
  }

  viewRoles_Permission(id:any){
    this.router.navigate(['roles-permission/view-roles-permission'], { queryParams: { roleId: id, pagetitle:adminTabList[3].name}, skipLocationChange: environment.SkipLocationChange });
  }
}
