import { Component, OnInit, Inject } from '@angular/core';
import { CrudService,CommonService,SnackbarService,ResponseMessageTypes,ResponseMessage,AttributeType, appModels } from 'src/app/services';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-status-change-dialog',
  templateUrl: './status-change-dialog.component.html',
  styleUrls: ['./status-change-dialog.component.scss']
})
export class StatusChangeDialogComponent implements OnInit {
  updateFormValues:any = [];
  editResponse:any;
  attributeTypeValue = AttributeType;

  constructor(private crudService: CrudService,private commonService: CommonService, private snackbarService: SnackbarService, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<StatusChangeDialogComponent>) { }

  ngOnInit(): void {
  }
  update(){
    this.updateFormValues = [];
    this.crudService.get_params(`${appModels.ENTITIES}/${this.data.dynamicAPIurl}/edit`, { params: { entityTypeId: this.data.entityTypeID, entityId: this.data.entityId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      this.editResponse = response;

      this.editResponse.forEach((element:any) =>{
        if(element.componentName == this.attributeTypeValue?.TOGGLE){
          this.updateFormValues.push({
            "entityTypeId":this.data.entityTypeID,
            "entityId": this.data.entityId ,
            "entityTypeAttributeId": element.entityTypeAttributeId,
            "id": element.attributeValueId ? element.attributeValueId : "",
            "attributeValue": this.data.event,
            "isKey": element.isKey
          })
        }
      })
      if(this.updateFormValues.length >0){
        this.crudService.update(`${appModels.ATTRIBUTE_VALUE}/${this.data.dynamicAPIurl}`, this.updateFormValues)
          .pipe(untilDestroyed(this)).subscribe(res => {
              this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
              this.commonService.setLoaderShownProperty(false);
              this.dialogRef.close();
        })
      }

    })
  }

  onNoClick(): void {
    this.dialogRef.close(this.data.event);
  }
}
