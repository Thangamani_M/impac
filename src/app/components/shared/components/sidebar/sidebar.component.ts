import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { CrudService, appModels, CommonService, defaultSortingBy, DynamicAPIEndPoints, EntityTypeId,sideBarRouting} from 'src/app/services';
import { DomSanitizer } from '@angular/platform-browser';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
/** Sidebar Component */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  pageSize:number = 10;
  pageNo: number = 0;
  searchKey: string = '';
  sidebarMenuList: any;
  sidebarSubmenu:any;
  activeRouterURL: any;
  menuRoutingUrls = sideBarRouting;
  constructor(private router: Router,private crudService: CrudService,private commonService : CommonService,public sanitizer: DomSanitizer) {
   }


  ngOnInit(): void {
    this.getSidebarMenus();
  }

  getSidebarMenus(){
    this.activeRouterURL = this.router.url.split('?')[0];
    this.commonService.getSidebar().subscribe((sidebaritems:any) => {
      sidebaritems == null && this.onLoadSidebarMenu();
        this.sidebarMenuList = sidebaritems?.Entities;
        this.sidebarMenuList?.forEach((submenu:any) => {
          if(submenu?.['SideBar Options'] != null){
            this.sidebarSubmenu = JSON.parse(submenu?.['SideBar Options']);        
          }
        });
    });
  }

  onLoadSidebarMenu(){
    this.crudService.get_params(`${appModels.ENTITY_TYPE}/${DynamicAPIEndPoints.MENU_URL}/${ appModels.GETALLWITHPAGINATION}/${EntityTypeId.SIDEBAR}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: defaultSortingBy.active, direction:defaultSortingBy.direction,searchKey:this.searchKey} }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      response['Entities'] = response['Entities'].map((items:any) =>
      Object.assign({},items,{"SideBar Icon": this.sanitizer.bypassSecurityTrustHtml(items["SideBar Icon"])}));
      this.commonService.setSidebar(response);
    });
  }
}
