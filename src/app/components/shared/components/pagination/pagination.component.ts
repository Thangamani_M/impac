import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CommonService} from 'src/app/services';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  constructor( private commonService: CommonService) { }

  @Input() paginationList:any;
  @Output() onPageChanged = new EventEmitter();

  pageSize:number = 10;
  pageNo:number = 0;
  pageLength: number = 0;
  totalPages: number;
  pagination: string | any[] | HTMLCollectionOf<Element> | undefined;
  pageSizeoptions:number[];
  
  ngOnInit(): void {
  }

  ngOnChanges() {
    this.onloadPagination();
  }

  onloadPagination(){
    this.pageLength = this.paginationList?.totalItems;
    this.totalPages = this.paginationList?.totalPages;
    this.pageNo = this.paginationList?.currentPage;
    let pageNumber = (this.paginationList?.Entities?.length == 0) ? `0 of 0` : `${this.pageNo + 1} of ${this.totalPages}`;
      let rows = `Page ${pageNumber}`;
      this.pagination = document.getElementsByClassName('mat-paginator-range-label');
      if (this.pagination !== undefined || null) {
        if (this.pagination.length >= 1)
          this.pagination[0].innerHTML = rows;
      }
      this.pageSizeoptions= this.commonService.pageSizeOption(this.pageLength);
  }

  onPageChange(event:any,pagination?:any) {
    let { pageSize,pageIndex,length } = event;
    this.pageSize = pageSize;
    this.pageLength = length;
    this.pageNo = pageIndex;
    let params = event;
    this.onPageChanged.emit({ params });
  }

}
