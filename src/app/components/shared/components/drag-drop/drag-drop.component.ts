import { Component, OnInit, Input } from '@angular/core';
import { CrudService, appModels, ResponseMessageTypes, SnackbarService, CommonService, ResponseMessage, AttributeType, DynamicAPIEndPoints } from 'src/app/services';
import { DndDropEvent, DropEffect } from 'ngx-drag-drop';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DeleteComponent } from 'src/app/components/shared';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.scss']
})
export class DragDropComponent implements OnInit {

  constructor(private crudService: CrudService, private commonService: CommonService,private snackbarService: SnackbarService, private router: Router, public dialog: MatDialog) {
  }
  ngOnInit(): void {
  }

  showDragDropTemplateForm: boolean = false;
  modelFields: Array<any> = [];
  model: any = {
    theme: {
      bgColor: "ffffff",
      textColor: "555555",
      bannerImage: ""
    },
    attributes: this.modelFields
  };
  attributeTypeValue = AttributeType;
  propertyModelValue: Array<any> = [];
  lastArrayOfPropertyModel:any;

  @Input() templateForms: any;
  @Input() dynamicApiUrl: string;
  @Input() formResponse:any;
  @Input() selectedIndex: number;
  @Input() selectedCaseTypeId: number;

  templateFormValues:Array<any> =[];
  templateFormObj:Array<any> =[];
  entityAttributesValues: any;
  createFormValues: any;
  createTemplateValue: any = [];
  value: any = {
    "value":""
  };
  isSelected : any;
  
  //Load Form
  onLoadForm() {
    this.showDragDropTemplateForm = true;
    this.onloadFormElement();
  }

  //load Form Element
  onloadFormElement() {
    this.crudService.get_params(`${appModels.COMPONENT}/${this.dynamicApiUrl}`, { params: { pageNo: 0, pageSize: 10, sortBy: 'createdOn', direction: 'asc' } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      let multipleValue = {"value":"Option1"};
      response['Component'] = response['Component'].map((items:any) =>
      Object.assign({},items,{ componentVal: [multipleValue]}));
      this.modelFields = response['Component'];
      this.commonService.setLoaderShownProperty(false);
    })
  }

  /** Drag & Drop Function (CaseForm,Investigation) */
  onDragged(item: any, list: any[], effect: DropEffect) {
    if (effect === "move") {
      const index = list.indexOf(item);
      list.splice(index, 1);
    }
  }
  // Drop
  onDrop(event: DndDropEvent, list?: any[]) {
    this.propertyModelValue = [];
    if (list && (event.dropEffect === "copy" || event.dropEffect === "move")) {
      let index = event.index;
      this.isSelected = index;
      if (typeof index === "undefined") {
        index = list.length;
      }
      this.propertyModelValue.push(event.data);
      list.splice(index, 0, event.data);
    }
  }

  onSelectedForms(item:any, index?:any){
    this.isSelected = index;
    this.propertyModelValue = [];
    this.propertyModelValue.push(item);
  }

  // Remove Field
  deleteField(i: any) {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        moduleName: '',
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      data !=undefined &&  this.model.attributes.splice(i, 1);
      this.propertyModelValue = [];
      this.lastArrayOfPropertyModel = this.model.attributes[this.model.attributes.length-1];
      if(this.lastArrayOfPropertyModel !== undefined){
        this.onSelectedForms(this.lastArrayOfPropertyModel,this.model.attributes.length-1);
      }
    });

  }

  deleteProperty(componentValue:any, index:any){
    if(this.propertyModelValue[0]?.componentVal.length == 1){
      return;
    }
    else{
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        moduleName: '',
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      data !=undefined && componentValue.splice(index, 1);
    });
  }
  }
  
  // Template
  submitTemplate() {  
    if (this.templateForms.invalid) {
      this.templateForms.markAllAsTouched();
      this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
      return;
    }
    else  {
      this.templateFormObj = [];
      this.templateFormValues = [];
      Object.keys(this.templateForms.value).forEach((key) => {
        const currentControl = this.templateForms.controls[key];
        if (!currentControl.pristine && !currentControl.untouched) {
          this.templateFormObj.push({ [key]: this.templateForms.value[key] });
        }
      });
      this.formResponse.map((res: any) => {
        this.templateFormObj.map((forms: any) => {
          if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
            res.attributeValue = Object.values(forms).toString();
            this.templateFormValues.push(res);
          }
        })
      })
      }
        this.create();
  }
  
  create() {
    this.entityAttributesValues = [];
    this.createTemplateValue = [];
    this.templateFormValues?.map((res: any) => {
      this.entityAttributesValues.push({ 'entityTypeAttributeId': res.id, 'attributeValue': res.attributeValue });
      this.createFormValues = {
        "entityTypeId": this.templateFormValues[0]?.entityTypeId,
        "entityAttributes": this.entityAttributesValues
      }
    })
    this.model.attributes.length > 0 &&
    this.model.attributes.forEach((res: any) =>{
      this.createTemplateValue.push( {
        "entityTypeAttributeName": res?.entityTypeAttributeName,
        "entityTypeId": this.templateFormValues[0]?.entityTypeId,
        "componentId":res?.id,
        "isRequired":res?.isRequired ? res?.isRequired : false,
        "isVisible":true,
        "isDynamic":false,
        "isKey":false,
        "componentVal":res?.componentName == AttributeType?.DROP_DOWN || res?.componentName == AttributeType?.CHECK_BOX ? JSON.stringify(res?.componentVal) : ''
      })
    })

    this.crudService.post(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.CREATE}`, this.createFormValues).pipe(untilDestroyed(this)).subscribe((res: any) => {

      if(this.createTemplateValue.length > 0){
       this.crudService.post(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${DynamicAPIEndPoints?.TASK_TEMPLATE_URL}/${this.templateFormValues[0]?.entityTypeId}/${this.selectedCaseTypeId}`, this.createTemplateValue).pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
              this.cancel();
        })
      }
      else{
        this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
        this.cancel();
      }
      })
  }

  cancel(){
    this.router.navigate([`../configuration/configuration-list`], { queryParams: { tab: this.selectedIndex } });
  }    

  addValue(values:any){
    values.push(this.value);
    this.value={value:""};
  }
}
