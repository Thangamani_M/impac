import { Component, OnInit, Input, ViewEncapsulation, ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router';
import { CrudService, appModels, ResponseMessageTypes, ResponseMessage, SnackbarService, CommonService, AttributeType, supportedFileType,EntityTypeId,ConstantValue,menuNameList, ResearchType } from 'src/app/services';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { forkJoin } from 'rxjs'; 

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-create-edit-page',
  templateUrl: './create-edit-page.component.html',
  styleUrls: ['./create-edit-page.component.scss'],
  encapsulation: ViewEncapsulation.Emulated

})
export class CreateEditPageComponent implements OnInit {

  @Input() entityTypeID: any = [];
  @Input() entityID: any = [];
  @Input() formResponse: Array<any> = [];
  @Input() dropDownValues: Array<any> = [];
  @Input() moduleName: string;
  @Input() method: string;
  @Input() selectedIndex: number;
  @Input() templatePage: any;
  @Input() menuName: string;
  @Input() dynamicApiUrl: string;
  @Input() roleResult:any;
  @Input() approvalRoleResponse:any;
  @Input() notifyRoleResponse:any;

  @Input() templateForm = new FormGroup({});
  templateFormObj: any = [];
  templateFormValues: any = [];
  createFormValues: any;
  updateFormValues: any = [];
  entityAttributesValues: any = [];
  attributename: any;
  selectable = true;
  formvaluecheck: any;
  newTemplateformvaluecheck: any;
  removable = true;
  listOfFiles: any[] = [];
  fileUpload: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  selectedFile: any;
  newtemplateForms = new FormGroup({});
  
  formResponses: any = [];
  editFileList: any = [];
  newtemplateFormObj: any = [];
  newtemplateFormValues: any = [];
  forms_templates: any;
  isUpdateFile = false;
  formData = new FormData();
  customTemplateFormData = new FormData();
  updateforms_templates: any;
  newTemplateResult: any;
  templateMultiResult: any;
  approversNameList: any;
  dragDropTemplate: boolean = false;
  dynamicRoleAPIUrl = menuNameList?.ROLE;

  attributeTypeValue = AttributeType;
  selectedId:any;

  listOfTemplateFiles: any = [];
  allFiles: any = [];
  selectedCaseTypeName:string;
  selectedCountrycode:string = "+";
  RoleEntityTypeID: number = EntityTypeId?.ROLE;
  updateRoleResult: any;
  isSuperAdmin: boolean = true;
  notifyNameList: any;
  selectedType:any = '';
  researchType = ResearchType;
  //Editor
  config: any = this.commonService.editorConfiguration;

  videoConfig: any = this.commonService.editorConfigurationVideoType;
  webpageConfig: any = this.commonService.editorConfigurationWebpageType;
  ebookConfig: any = this.commonService.editorConfigurationEbookType;
  showFileUpload:boolean = true;
  menuNameLists = menuNameList;
  progress = 0;
  hideProgressBar:boolean = true;
  policyFileFields:any = [];
  constructor(private router: Router, private formBuilder: FormBuilder, private snackbarService: SnackbarService, private crudService: CrudService, private datepipe: DatePipe, private readonly changeDetectorRef: ChangeDetectorRef, private commonService:CommonService) {
  }
  //After View Checked
  ngAfterViewChecked(): void { 
    this.changeDetectorRef.detectChanges();
  }

  ngOnInit(): void {
  }

  //on changes
  ngOnChanges() {
    if (this.menuName == menuNameList?.POLICIES && this.moduleName == 'Edit Policies' || this.menuName == menuNameList?.POLICIES && this.moduleName == 'Edit Legislation') {
      //Approver step in Policy & Legislation
      this.approvalRoleResponse?.forEach((approvelRole: any) => {
        if (approvelRole?.entityTypeAttributeName == "Approvers-Role" && approvelRole?.attributeValue != null) {
          this.onChangePoliciesRole(approvelRole.attributeValue,'Approve');
        }
      })
      //Notify step in Policy & Legislation
      this.notifyRoleResponse?.forEach((approvelRole: any) => {
        if (approvelRole?.entityTypeAttributeName == "Notify-Role" && approvelRole?.attributeValue != null) {
          this.onChangePoliciesRole(approvelRole.attributeValue,'Notify');
        }
      })
    }
    this.formResponses = [];
    this.formvaluecheck = this.templateForm?.value
    this.formResponse?.forEach((attribute: any) => {
      if(attribute?.groupId != 3){
      if (attribute?.isKey == false && attribute?.isDynamic == false && attribute?.isCreate == true && attribute?.componentName != this.attributeTypeValue?.RICH_TEXT_EDITOR) {
        this.formResponses.push(attribute);
      }
      else if (attribute?.isKey == false && attribute?.isDynamic == true && attribute?.isCreate == true && attribute?.componentName != this.attributeTypeValue?.RICH_TEXT_EDITOR) {
        this.formResponses.push(attribute);
      }
      else if (attribute?.isKey == true && attribute?.isDynamic == false && attribute?.isCreate == true && attribute?.componentName != this.attributeTypeValue?.RICH_TEXT_EDITOR) {
        this.formResponses.push(attribute);
      }
    }
    })
    this.formResponses?.forEach((attributes: any) => {
      if (attributes?.componentName == this.attributeTypeValue?.DROP_DOWN) {
        this.onChangeEntity(attributes?.attributeValue, this.dropDownValues);
        attributes.entityTypeAttributeName == "Member State" && this.onChangeAdminConfigDropdown(attributes?.entityTypeAttributeName,attributes?.attributeValue);
        if(this.moduleName == 'Edit User' && attributes.entityTypeAttributeName == menuNameList?.ROLE) {
          this.crudService.get_params(`${appModels.ROLE}/${this.dynamicApiUrl}`, { params: { entityTypeId: this.RoleEntityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
            this.updateRoleResult = response;
            this.commonService.setLoaderShownProperty(false);
            this.onChangeUserRole('',attributes?.attributeValue);
          })
        } 
      }
      else if (attributes?.componentName == this.attributeTypeValue?.UPLOAD) {
        this.listOfFiles = [];
        if (attributes?.attributeValue) {
          this.editFileList = JSON.parse(attributes?.attributeValue);
          this.editFileList.forEach((file: any) => {
            this.listOfFiles.push(file);
          })
        }
      }
    })
   // Show File Fields - RSS (if type - Ebook
    if(this.moduleName == ConstantValue?.CreateResearch){
        this.showFileUpload = false;
    }
   // Progressbar - File Upload
    if(this.moduleName.includes("Create")){
      this.hideProgressBar = true;
    }
    else{
      this.hideProgressBar = false;
    }
  // Files Fields - Policy & legislation Module 
    if (this.menuName == menuNameList?.POLICIES){
      this.policyFileFields = [];
      this.formResponse?.forEach(result=>{
        if(result?.groupId == 3){
            this.policyFileFields.push(result);
        }
      })
      this.policyFileFields?.forEach((attributes: any) => {
        if (attributes?.componentName == this.attributeTypeValue?.UPLOAD) {
              this.listOfFiles = [];
              if (attributes?.attributeValue) {
                this.editFileList = JSON.parse(attributes?.attributeValue);
                this.editFileList.forEach((file: any) => {
                  this.listOfFiles.push(file);
                })
              }
            }
      })
    }
  }

  comparer(o1: any, o2: any): boolean {
    if(o1 == o2)
    return true;
    else return false
  }

  //create update
  createUpdate() {
    if (!this.isUpdateFile) {
      if (this.templateForm.invalid || this.newtemplateForms?.invalid) {
        this.templateForm.markAllAsTouched();
        this.newtemplateForms?.markAllAsTouched();
        this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
        return;
      }
      else if (this.templateForm.pristine && this.newtemplateForms?.pristine) {
        this.snackbarService.openSnackbar(ResponseMessage.NO_CHANGES, ResponseMessageTypes.WARNING);
        return;
      }
      else if (this.formvaluecheck != this.templateForm.value && this.templateForm.dirty == this.templateForm.touched || this.newTemplateformvaluecheck != this.newtemplateForms.value && this.newtemplateForms.dirty == this.newtemplateForms.touched ) {
        this.templateFormObj = [];
        this.templateFormValues = [];
        Object.keys(this.templateForm.value).forEach((key) => {
          const currentControl = this.templateForm.controls[key];
          if (!currentControl.pristine && !currentControl.untouched) {
            this.templateFormObj.push({ [key]: this.templateForm.value[key] });
          }
        });
        this.formResponse?.map((res: any) => {
          this.templateFormObj?.map((forms: any) => {
            if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
              res.attributeValue = Object.values(forms).toString();
              this.templateFormValues.push(res);
            }
          })
        })
      //Approver step in Policy & Legislation
        this.approvalRoleResponse?.map((res: any) => {
          this.templateFormObj?.map((forms: any) => {
            if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
              res.attributeValue = Object.values(forms).toString();
              this.templateFormValues.push(res);
            }
          })
        })
      //Notify step in Policy & Legislation
      this.notifyRoleResponse?.map((res: any) => {
        this.templateFormObj?.map((forms: any) => {
          if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
            res.attributeValue = Object.values(forms).toString();
            this.templateFormValues.push(res);
          }
        })
      })

        this.newtemplateFormObj = [];
        this.newtemplateFormValues = [];
        if (this.newtemplateForms?.value) {
          Object.keys(this.newtemplateForms?.value).forEach((key) => {
            const currentControl = this.newtemplateForms?.controls[key];
            if (!currentControl.pristine && !currentControl.untouched) {
              this.newtemplateFormObj.push({ [key]: this.newtemplateForms?.value[key] });
            }
          });
          this.newTemplateResult?.map((res: any) => {
            this.newtemplateFormObj?.map((forms: any) => {
              if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
                res.attributeValue = Object.values(forms).toString();
                this.newtemplateFormValues.push(res);
              }
            })
          })
        }
        if (this.method) {
          this.update();
        }
        else {
          this.create();
        }
      }
    }
    else {
      this.formData.append('supportingDocuments', '');
      this.editFileUpload();
    }
  }
  
  //create
  create() {
    this.entityAttributesValues = [];
    this.forms_templates = [...this.templateFormValues, ...this.newtemplateFormValues];
    this.forms_templates?.map((res: any) => {
      if (res?.componentName == this.attributeTypeValue?.DATE_TIME) {
        res.attributeValue = this.datepipe.transform(res?.attributeValue, 'yyyy-MM-ddTHH:mm:ss');
      }
      res?.componentName != this.attributeTypeValue?.UPLOAD && res?.componentName != this.attributeTypeValue?.MULTI_DROP_DOWN &&
        this.entityAttributesValues.push({ 'entityTypeAttributeId': res.id, 'attributeValue': res.attributeValue });
        res?.componentName == this.attributeTypeValue?.MULTI_DROP_DOWN &&
        this.entityAttributesValues.push({ 'entityTypeAttributeId': res.id, 'attributeValue': res.attributeValue });
      })
/** File upload */
     this.templateFormValues.forEach((result:any) =>{
      if(result?.componentName == this.attributeTypeValue?.UPLOAD){
        this.formData.append('entityTypeId', result?.entityTypeId);
        this.formData.append('entityTypeAttributeId', result?.id);
        this.formData.append('isTemplateAttribute', "false");
        this.listOfFiles.forEach((files: any) => {
          this.formData.append('supportingDocuments', files);
        })
      }
      })
/** Custom File upload */
      this.newtemplateFormValues.forEach((result:any) =>{
        if(result?.componentName == this.attributeTypeValue?.UPLOAD){
          this.customTemplateFormData.append('entityTypeId', this.templateFormValues[0]?.entityTypeId);
          this.customTemplateFormData.append('entityTypeAttributeId', result?.id);
          this.customTemplateFormData.append('isTemplateAttribute', "true");
          this.listOfTemplateFiles.forEach((files: any) => {
            this.customTemplateFormData.append('supportingDocuments', files);
          })
        }
      })
      
      this.createFormValues = {
        "entityTypeId": this.forms_templates[0]?.entityTypeId,
        "entityAttributes": this.entityAttributesValues
      }

    this.crudService.post(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.CREATE}`, this.createFormValues)
      .pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.formData.append('entityId', res[0].entityId);
        this.customTemplateFormData.append('entityId', res[0].entityId);

         if(this.formData.get('entityTypeAttributeId') != null && this.customTemplateFormData.get('entityTypeAttributeId') != null || this.formData.get('entityTypeAttributeId') != null){          
          this.crudService.upload_File(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.UPLOAD_SUPPORT_DOCUMENT}`, this.formData).pipe(untilDestroyed(this)).subscribe((res: any) => {
            if(this.customTemplateFormData.get('entityTypeAttributeId') != null){this.createCustomTemplateFile()} else{this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
            this.cancel();}
          })
         } 
         else if(this.formData.get('entityTypeAttributeId') == null && this.customTemplateFormData.get('entityTypeAttributeId') != null){
          this.createCustomTemplateFile();
         }
        else if(this.formData.get('entityTypeAttributeId') == null && this.customTemplateFormData.get('entityTypeAttributeId') == null){
            this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
            this.cancel();
            }
      })
  }

  /** create custom files */
  createCustomTemplateFile(){   
      this.crudService.upload_File(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.UPLOAD_SUPPORT_DOCUMENT}`, this.customTemplateFormData).pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
        this.cancel();     
    })   
  }

  //Update
  update() {
    this.updateFormValues = [];
    this.updateforms_templates =[...this.templateFormValues, ...this.newtemplateFormValues];
    this.updateforms_templates?.map((res: any) => {
      if (res.componentName == this.attributeTypeValue?.DATE_TIME) {
        res.attributeValue = this.datepipe.transform(res.attributeValue, 'yyyy-MM-ddTHH:mm:ss');
      }
      res?.componentName != this.attributeTypeValue?.UPLOAD && 
        this.updateFormValues.push({
          "entityTypeId": this.entityTypeID,
          "entityId": this.entityID,
          "entityTypeAttributeId": res.entityTypeAttributeId,
          "id": res.attributeValueId ? res.attributeValueId : "",
          "attributeValue": res.attributeValue,
          "isKey": res.isKey
        })
    })
/** File upload */
    this.templateFormValues.forEach((result:any) =>{
      if(result?.componentName == this.attributeTypeValue?.UPLOAD){
        this.formData.append('entityTypeId',  this.entityTypeID);
        this.formData.append('entityTypeAttributeId', result?.entityTypeAttributeId);
        this.formData.append('entityId', this.entityID);
        this.formData.append('isTemplateAttribute', "false");
        this.listOfFiles.forEach((files: any) => {
          !files?.id && this.formData.append('supportingDocuments', files);
        })
      }
      })
/** Custom File upload */
    this.newtemplateFormValues.forEach((result:any) =>{
      if(result?.componentName == this.attributeTypeValue?.UPLOAD){
        this.customTemplateFormData.append('entityTypeId',  this.entityTypeID);
        this.customTemplateFormData.append('entityTypeAttributeId', result?.entityTypeAttributeId);
        this.customTemplateFormData.append('entityId', this.entityID);
        this.customTemplateFormData.append('isTemplateAttribute', "true");
        this.listOfTemplateFiles.forEach((files: any) => {
          !files?.id &&this.customTemplateFormData.append('supportingDocuments', files);
        })
      }
    })

    if(this.updateFormValues.length >0 || this.formData.get('entityTypeAttributeId') == null || this.customTemplateFormData.get('entityTypeAttributeId') == null){
        this.updateForms();
      }
      else{
        this.editFileUpload();
      }
  }
  /**Update Form */
updateForms(){
  this.updateFormValues.length> 0 && 
  this.crudService.update(`${appModels.ATTRIBUTE_VALUE}/${this.dynamicApiUrl}`, this.updateFormValues)
  .pipe(untilDestroyed(this)).subscribe(res => {
    this.editFileUpload();
  }) ||  this.editFileUpload();

}
  
  //Edit File Upload
  editFileUpload() {
    if(this.formData.get('entityTypeAttributeId') != null && this.customTemplateFormData.get('entityTypeAttributeId') != null || this.formData.get('entityTypeAttributeId') != null){
         this.crudService.edit_upload_File(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.EDIT_FILE}`,this.formData).pipe(untilDestroyed(this)).subscribe((res: any) => {
          this.updateCustomTemplateFile();
      })
    }
    else if(this.formData.get('entityTypeAttributeId') == null && this.customTemplateFormData.get('entityTypeAttributeId') != null){
      this.updateCustomTemplateFile();
     }
     else if(this.formData.get('entityTypeAttributeId') == null && this.customTemplateFormData.get('entityTypeAttributeId') == null){
      this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
      this.cancel();
      }
  }

  updateCustomTemplateFile(){
    if(this.customTemplateFormData.get('entityTypeAttributeId') != null){
    this.crudService.edit_upload_File(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.EDIT_FILE}`, this.customTemplateFormData).pipe(untilDestroyed(this)).subscribe((res: any) => {
    this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
    this.cancel();
  }) } else{ this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
    this.cancel();
  }
  }

   //download file
   download(files: any) {
    this.commonService.downloadFile(files,this.dynamicApiUrl);
  }

  // Remove Selected File

  removeSelectedFile(index: any, files: any, type?:any) {
    if (files?.id) {
      this.crudService.delete(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.DELETE_FILE}/${files.id}`)
        .pipe(untilDestroyed(this)).subscribe((res: any) => {
          type == undefined && this.listOfFiles.splice(index, 1) || this.listOfTemplateFiles.splice(index, 1);
          this.commonService.setLoaderShownProperty(false);
          this.snackbarService.openSnackbar(ResponseMessage.DELETE, ResponseMessageTypes.SUCCESS);
          this.isUpdateFile = true;
        })
    }
    else {
      type == undefined && this.listOfFiles.splice(index, 1) || this.listOfTemplateFiles.splice(index, 1);
      this.fileList.splice(index, 1);
    }
  }

  //File Selected
  onFileSelected(file: any, type?:any) {
    setInterval(()=> {
      if(this.progress < 100.000){
        this.progress = this.progress + 0.1;
        this.hideProgressBar = true;
      }
      else{
        this.progress = 100;
        this.hideProgressBar = false;
      }
    }, 0);
    this.isUpdateFile = false;
    let FileData = file.target.files;
 
    const supportedExtensions = supportedFileType;
    for (let i = 0; i < FileData.length; i++) {
      this.selectedFile = FileData[i];
      const path = this.selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === this.selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(this.selectedFile);
          type == undefined && this.listOfFiles.push(this.selectedFile) || this.listOfTemplateFiles.push(this.selectedFile) ;
        }
        this.progress = 0;
        if (this.fileList.length == this.maxFilesUpload) {
          this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
          return;
        }
      } else { 
        this.snackbarService.openSnackbar(ResponseMessage.FILE_TYPE, ResponseMessageTypes.WARNING);
      }
    }
  }
  // Cancel
  cancel() {
    this.menuName == menuNameList?.CASEMANAGEMENT && this.router.navigate([`../case-management/case-mangement-list`], { queryParams: { tab: this.selectedIndex } }) || this.menuName == menuNameList?.POLICIES &&  this.router.navigate([`../policies-legislation/policies-legislation-list`], { queryParams: { tab: this.selectedIndex } }) || this.menuName == menuNameList?.RSSFEEDS &&  this.router.navigate([`rss-feeds/rss-feeds-list`]) || this.menuName == menuNameList?.BLOG && this.router.navigate([`blog/blog-list`]) ||
    this.menuName == menuNameList?.ADMIN && this.router.navigate([`../admin/admin-list`], { queryParams: { tab: this.selectedIndex } }) || 
    this.menuName == menuNameList?.CONFIGURATION &&  this.router.navigate([`../configuration/configuration-list`], { queryParams: { tab: this.selectedIndex } });
  }
  // Change Start
  onChangeStart() {
    this.commonService.onChangeTime(this.templateForm);
  }
  // Change Entity

  onChangeEntity(event: any, dropdownValuesList: any) {
    const newTemplate = dropdownValuesList?.find((element: any) => event == element.entityId);
    if (newTemplate?.hasTemplate == 'true') {
      this.selectedCaseTypeName = newTemplate?.componentValue;
      this.newTemplatelist(event)
    }
    else if (newTemplate?.hasTemplate == 'false') {
      this.newTemplatelist();
    }
    if(this.moduleName == ConstantValue?.CreateResearch){
      this.showFileUpload = this.showFileUpload == true ? true : false;
    }
  }
  //Template list
  newTemplatelist(value?: any) {
    if (value != undefined) {
      if (this.method && this.menuName == menuNameList?.CASEMANAGEMENT) {
        this.crudService.get_params(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${this.dynamicApiUrl}/${appModels.EDIT}`, { params: { entityId: this.entityID, entityMappedId: value, entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
          let dropdownAPI: any = [];
          this.newTemplateResult = response;
          if (this.newTemplateResult?.length > 0) {
            this.newtemplateForms = this.createFormGroup();
            this.newTemplateformvaluecheck = this.newtemplateForms?.value;
            this.newTemplateResult?.forEach((res: any) => {
              if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.CHECK_BOX) {
                dropdownAPI.push(this.crudService.get(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${this.dynamicApiUrl}/${appModels.COMPONENTVALUE}/${this.entityTypeID}/${res.entityTypeAttributeId}`));
                this.newtemplateForms.get(res?.entityTypeAttributeName)?.setValue(+parseInt(res?.attributeValue));
              }
              else if (res?.componentName == this.attributeTypeValue?.UPLOAD) {
                this.listOfTemplateFiles = [];
                  this.editFileList = JSON.parse(res?.attributeValue);
                  this.editFileList?.forEach((file: any) => {
                    this.listOfTemplateFiles.push(file);
                  })
              }
            })
            this.onLoadTemplateMultiValue(dropdownAPI);
            this.commonService.setLoaderShownProperty(false);
          }
          else {
            this.commonService.setLoaderShownProperty(false);
          }
        })
      }
      else if(this.menuName == menuNameList?.CASEMANAGEMENT) {
        this.crudService.get_params(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${this.dynamicApiUrl}/${appModels.ENTITY}/${value}`, { params: { entityTypeId: this.entityTypeID } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
          let dropdownAPI: any = [];

          this.newTemplateResult = response;
          if (this.newTemplateResult.length > 0) {
            this.newtemplateForms = this.createFormGroup();
            this.newTemplateformvaluecheck = this.newtemplateForms?.value;
            this.newTemplateResult?.forEach((res: any) => {
              if (res?.componentName == this.attributeTypeValue?.DROP_DOWN || res?.componentName == this.attributeTypeValue?.CHECK_BOX) {
                dropdownAPI.push(this.crudService.get(`${appModels.ENTITY_TYPE_TEMPLATE_ATTRIBUTES}/${this.dynamicApiUrl}/${appModels.COMPONENTVALUE}/${this.entityTypeID}/${res.id}`));
              }
            })
            this.onLoadTemplateMultiValue(dropdownAPI);
            this.commonService.setLoaderShownProperty(false);
          }
          else {
            this.commonService.setLoaderShownProperty(false);
          }

        })
      }
    }
    else {
      this.newTemplateResult = [];
      this.createFormGroup();
      this.newtemplateForms = new FormGroup({});
    }
  }
  // Load Template DropDown
  onLoadTemplateMultiValue(Api: any, isEdit = false) {
    forkJoin(Api).subscribe((results: any) => {
      this.templateMultiResult = results.flat(1);
      this.commonService.setLoaderShownProperty(false);
    })
  }
  // Create form
  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    if (this.newTemplateResult.length > 0) {
      this.newTemplateResult?.forEach((column: any) => {
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, Validators.required));
        }
      });
    }
    return group;
  }
  // Change Policies Role
 
  onChangePoliciesRole(event: any,fieldType?:any) {
    this.crudService.get_path(`${appModels.ENTITIES}/${this.dynamicApiUrl}/${appModels.POLICY_APPROV_ROLES}/${event}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if(fieldType == 'Approve'){
        this.approversNameList = response;
      }
      else if(fieldType == 'Notify'){
        this.notifyNameList = response;
      }
      this.commonService.setLoaderShownProperty(false);
    })
  }

  // key press
  onkeypress(e: any) {
    this.commonService.onkeyUpTextbox(e);
  }
  // keypressTextField
  onkeypressTextField(e: any) {
    this.commonService.onKeyUpTextField(e);
  }
  //Key up text area
  onKeyuptextarea(val: any) {
    this.commonService.onKeyUpTextarea(val);
  }
  // key press Number
  onkeypressNumber(val: any) {
    this.commonService.onKeyUpNumber(val);
  }

  onChangeAdminConfigDropdown(event:any,value:any){
    this.selectedId = value;
    this.dragDropTemplate = true && !this.method && event == "Case Type";
    if(event == "Member State") {
      let selectedCountryList= this.dropDownValues?.find((element: any) => element.entityId == this.selectedId);
      this.selectedCountrycode = selectedCountryList?.countryCode != undefined && selectedCountryList?.countryCode || this.selectedCountrycode ;
    }
  }
  onChangeUserRole(event:any,value?:any){
    if(event != ''){
      this.templateForm.controls['Member State'].reset();
    }
    if(this.moduleName == 'Create New User' || this.moduleName == 'Edit User'){
      let roleList;
      if(this.roleResult.length > 0) {
        roleList = this.roleResult;
      }
      else{
        roleList = this.updateRoleResult;
      }
      roleList?.forEach((res:any) => {
          if(value == res.id){
            if(res.entityName == ConstantValue.Value){
              this.isSuperAdmin = false;
            }
            else{
              this.isSuperAdmin = true;
            }
          }
      })
    }

  }

  onChangeResearchType(event:any, dropdownValuesList: any){
    const newTemplate = dropdownValuesList?.find((element: any) => event == element.entityId);
      this.selectedType = this.selectedType != undefined && newTemplate?.componentValue || '';
      if(newTemplate?.componentValue == this.researchType?.RSS_FEEDS || newTemplate?.componentValue == this.researchType?.FORUMS || newTemplate?.componentValue == this.researchType?.CASE_STUDY){
        this.selectedType = '';
      }
      if(newTemplate?.componentValue == this.researchType?.EBOOKS){
        this.showFileUpload = true;
      }else{
        this.showFileUpload = false;
      }
      
  }
}