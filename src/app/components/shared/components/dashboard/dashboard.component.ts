import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import 'chart.piecelabel.js';
import { DatePipe } from '@angular/common';
import { CrudService, appModels, CommonService, caseManagement_List_View_TabList, pieChartEntityTypeName,EntityTypeId, defaultSortingBy,AuthenticationService,ConstantValue} from 'src/app/services';
import { Router, ActivatedRoute } from '@angular/router';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public pieChartOptions: any = {
    responsive: true,
    legend: {
      position: 'bottom',
    },
    labels: {
      fontColor: 'black!important'
    },
    pieceLabel: {
      fontColor: 'black',
      render: function (args: any) {
        const label = args.label,
          value = args.value;
        return value;
      }
    }
  };
  public pieChartType: ChartType = 'pie';
  public Legend = true;
  public pieChartColors = [
    {
      backgroundColor: ['#007ACC', '#FF4141', '#EAB027', '#3CC480'],
    },
  ];
  public incidentpieChartLabels: Label[] = [];
  public incidentpieChartData: number[] = [];
  public casepieChartLabels: Label[] = [];
  public casepieChartData: number[] = [];
  public eventpieChartLabels: Label[] = [];
  public eventpieChartData: number[] = [];
  public rssFeedspieChartLabels: Label[] = [];
  public rssFeedspieChartData: number[] = [];
  public blogpieChartLabels: Label[] = [];
  public blogpieChartData: number[] = [];

  labelNames: Array<any> = caseManagement_List_View_TabList;
  selectedIndex:number = 0;
  overallCount: any;
  startDate: any;
  endDate: any;
  todayDate = new Date();
  yearFirstDate = new Date(new Date().getFullYear(), 0, 1);
  @Output() chartValues = new EventEmitter();
  @Output() selectedMemberId = new EventEmitter();
  pieChartEntityType = pieChartEntityTypeName;
  
  pageSize:number = 30;
  pageNo:number = 0;
  searchKey = '';
  defaultSortby = defaultSortingBy;
  memberstates: any;
  countryId:any;
  selectedMemeberstateId:number;
  currentUserMemberState:any;
  currentUserDefaultMemberState:any;
  eventLength:any;
  caseLength:any;
  incidentLength:any;
  RSSLength:any;
  blogLength:any;


  constructor(private router: Router,private activeRoute: ActivatedRoute, private datepipe: DatePipe,private crudService: CrudService, private commonService: CommonService,private authenticationService: AuthenticationService) {
    this.activeRoute.queryParamMap.subscribe((params: any) => {
      this.selectedIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    })
  }

  ngOnInit(): void {
    this.currentUserMemberState = this.authenticationService.currentUserValue.memberState;
    this.currentUserDefaultMemberState = this.authenticationService.currentUserValue.defaultMemberState;
    this.getmemberstate(this.authenticationService.currentUserValue.roleName)
  }

  getmemberstate(currentUserRole?:any){
    this.crudService.get_params(`${appModels.ENTITY_TYPE}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${ appModels.GETALLWITHPAGINATION}/${EntityTypeId?.MEMBER_STATE}`, { params: { pageSize: this.pageSize, pageNo: this.pageNo, sortBy: this.defaultSortby?.active, direction: this.defaultSortby?.direction, searchKey: this.searchKey } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
      if(currentUserRole == ConstantValue.Value){
          this.memberstates = response['Entities'];
          if(this.currentUserDefaultMemberState != null){
            let userMemberState = response['Entities']?.find((name: any) => name?.EntityId == this.currentUserDefaultMemberState);
            this.selectedMemeberstateId = userMemberState?.EntityId;
          }
          else{
            let userMemberState = response['Entities']?.find((name: any) => name?.['Member State'] == this.currentUserMemberState);
            this.selectedMemeberstateId = userMemberState?.EntityId;
          }

      }
      else{
          response['Entities'].forEach((val:any) =>{
          if(val['Member State'] == this.currentUserMemberState){
            this.memberstates = [val];
          }
        })
        this.selectedMemeberstateId = this.memberstates[0]?.EntityId;
      }
      this.onChangeMemberState(this.selectedMemeberstateId,this.memberstates);
    })
  }
  onChangeMemberState(value:any,memberStateName?:any){
    let MemberStateAll = memberStateName?.find((name: any) => name?.EntityId == value);
    if(MemberStateAll?.['Member State'] != 'ALL'){
      this.countryId = value;
    }
    else{
      this.countryId = '';
    }
    this.onLoadDashboard();
  }

  onLoadDashboard(){
    this.startDate = this.datepipe.transform(this.yearFirstDate, 'yyyy-MM-dd');
    this.endDate = this.datepipe.transform(this.todayDate, 'yyyy-MM-dd');
    this.getOverAllCount(this.startDate, this.endDate);
    this.getOverallIncident();
    this.getOverallCase();
    this.getOverallEvent();
    this.getOverallRssFeeds();
    this.getOverallBlog();
  }
     // OverAllCount
     getOverAllCount(startDate?: any, endDate?: any) {
      this.crudService.get_params(`${appModels.DASHBOARD}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.ENTITY_TYPE}/${appModels.COUNT}`, { params: { startDate: startDate, endDate: endDate,countryId:this.countryId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.overallCount = response;
        this.commonService.setLoaderShownProperty(false);
      })
    }
    // OverallIncident
    getOverallIncident() {
      this.incidentpieChartData = [];
      this.incidentpieChartLabels = [];
      this.crudService.get_params(`${appModels.DASHBOARD}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.STATUS}`, { params: { entityType: this.pieChartEntityType?.Incident, startDate: this.startDate, endDate: this.endDate,countryId:this.countryId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        if(response?.length > 0){
          response?.forEach((value: any) => {
            if(value?.count > 0){
              this.incidentpieChartData.push(value?.count);
              this.incidentpieChartLabels.push(value?.status);
            }
            if(this.incidentpieChartData?.length == 0){
              this.incidentLength = 0;
            }
            else{
              this.incidentLength = response?.length;
            }
          })
        }
        else{
          this.incidentLength = 0;
        }
        this.commonService.setLoaderShownProperty(false);
      })
    }     
    // OverallCase
    getOverallCase() {
      this.casepieChartData = [];
      this.casepieChartLabels = [];
      this.crudService.get_params(`${appModels.DASHBOARD}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.STATUS}`, { params: { entityType: this.pieChartEntityType?.Case, startDate: this.startDate, endDate: this.endDate,countryId:this.countryId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        if(response?.length > 0){
          response?.forEach((value: any) => {
            if(value?.count > 0){
              this.casepieChartData.push(value?.count);
              this.casepieChartLabels.push(value?.status);
            }
            if(this.casepieChartData?.length == 0){
              this.caseLength = 0;
            }
            else{
              this.caseLength = response?.length;
            }
          })
        }
        else{
          this.caseLength = 0;
        }
        this.commonService.setLoaderShownProperty(false);
      })
    }
    // OverallEvent
    getOverallEvent() {
      this.eventpieChartData = [];
      this.eventpieChartLabels = [];
      this.crudService.get_params(`${appModels.DASHBOARD}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.STATUS}`, { params: { entityType: this.pieChartEntityType?.Event, startDate: this.startDate, endDate: this.endDate,countryId:this.countryId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        if(response?.length > 0){
          response?.forEach((value: any) => {
            if(value?.count > 0){
              this.eventpieChartData.push(value?.count);
              this.eventpieChartLabels.push(value?.status);
            }
            if(this.eventpieChartData?.length == 0){
              this.eventLength = 0;
            }
            else{
              this.eventLength = response?.length;
            }
          })
        }
        else{
          this.eventLength = 0;
        }
        this.commonService.setLoaderShownProperty(false);
      })
    }

    // OverallRssFeeds
    getOverallRssFeeds() {
      this.rssFeedspieChartData = [];
      this.rssFeedspieChartLabels = [];
      this.crudService.get_params(`${appModels.DASHBOARD}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.STATUS}`, { params: { entityType: this.pieChartEntityType?.RSSFeeds, startDate: this.startDate, endDate: this.endDate,countryId:this.countryId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        if(response?.length > 0){
          response?.forEach((value: any) => {
            if(value?.count > 0){
              this.rssFeedspieChartData.push(value?.count);
              this.rssFeedspieChartLabels.push(value?.status);
            }
            if(this.rssFeedspieChartData?.length == 0){
              this.RSSLength = 0;
            }
            else{
              this.RSSLength = response?.length;
            }
          })
        }
        else{
          this.RSSLength = 0;
        }
        this.commonService.setLoaderShownProperty(false);
      })
    }

    // OverallBlog
    getOverallBlog() {
      this.blogpieChartData = [];
      this.blogpieChartLabels = [];
      this.crudService.get_params(`${appModels.DASHBOARD}/${this.labelNames[this.selectedIndex].dynamicAPIUrl}/${appModels.STATUS}`, { params: { entityType: this.pieChartEntityType?.Blog, startDate: this.startDate, endDate: this.endDate,countryId:this.countryId } }).pipe(untilDestroyed(this)).subscribe((response: any) => {
        if(response?.length > 0){
          response?.forEach((value: any) => {
            if(value?.count > 0){
              this.blogpieChartData.push(value?.count);
              this.blogpieChartLabels.push(value?.status);
            }
            if(this.blogpieChartData?.length == 0){
              this.blogLength = 0;
            }
            else{
              this.blogLength = response?.length;
            }
          })
        }
        else{
          this.blogLength = 0;
        }
        this.commonService.setLoaderShownProperty(false);
      })
    }
    
  // chartClicked
  onPieChartClicked(e: any,tabValue:any) {
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if (activePoints.length > 0) {
        const clickedElementIndex = activePoints[0]._index;
        const label = chart.data.labels[clickedElementIndex];
        this.router.navigate([`case-management/case-mangement-list`], {queryParams: { tab: tabValue} });
        this.selectedMemberId.emit(this.countryId);
        this.chartValues.emit(label);
      }
    }
  }
  
}
