import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService,CommonService,SecretKey } from 'src/app/services';
import { ChangeDetectorRef } from '@angular/core';
import * as CryptoJS from 'crypto-js';

import { untilDestroyed,UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit {

  chatBotForm: FormGroup;
  chatBotLists:any = [];
  @Output() onChatBotClosed = new EventEmitter<any>();
  secretKey = SecretKey?.SECRET_KEY;


  constructor(private formBuilder: FormBuilder,private authenticationService:AuthenticationService,private commonService: CommonService, private readonly changeDetectorRef: ChangeDetectorRef) {
   }

  ngOnInit(): void {
    this.createChatBotForm();
    let getChatbotMessage = JSON.parse(JSON.stringify(localStorage.getItem('chatbotMessage')));
    let decryptedChatbot = getChatbotMessage != null &&  CryptoJS.AES.decrypt(getChatbotMessage, this.secretKey); 
    let decryptedChatbotMessage = JSON.parse(decryptedChatbot.toString(CryptoJS.enc.Utf8));
    this.chatBotLists = decryptedChatbotMessage || [];
    this.changeDetectorRef.detectChanges();
  }

  createChatBotForm() {
    this.chatBotForm = this.formBuilder.group({
      chat: ['', [Validators.required]]
    })
  }

  send(){
    this.authenticationService.chatBot(this.chatBotForm.value).pipe(untilDestroyed(this)).subscribe(res => {
        this.chatBotLists.push({"sendMessage":this.chatBotForm.value.chat,"receiveMessage":res?.chatbot, "viewMoreLink":res?.viewMoreLink})
        let encryptedChatbotMessage = CryptoJS.AES.encrypt(JSON.stringify(this.chatBotLists), this.secretKey).toString();
        localStorage.setItem('chatbotMessage',encryptedChatbotMessage);
        this.chatBotForm.reset();
        this.changeDetectorRef.detectChanges();
        this.commonService.setChatBotLoaderShownProperty(false);
    })
  }

  openLink(value:any){
    window.open(value);
  }

  closeChatbot(){
    this.onChatBotClosed.emit(false);
  }

}
