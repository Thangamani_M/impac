import { Component, OnInit,Output,EventEmitter} from "@angular/core";
import { Router} from "@angular/router";
import { CrudService, appModels, CommonService,AuthenticationService, SnackbarService, ResponseMessageTypes, ResponseMessage,headerTabList, DynamicAPIEndPoints, logoURL} from 'src/app/services';
import { HelpComponent } from 'src/app/components/shared';
import { MatDialog } from '@angular/material/dialog';
import { DeleteComponent } from 'src/app/components/shared';


import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: any;
  dynamicAPIUrl: string = DynamicAPIEndPoints?.NOTIFICATION_URL;
  labelNames: Array<any> = headerTabList;
  notification: any;
  showNotifyList:boolean = false;
  selectedIndex:number;

  @Output() showChatBot = new EventEmitter<any>();

  constructor(private authenticationService: AuthenticationService,private router: Router,private crudService: CrudService, private commonService: CommonService,public dialog: MatDialog,private snackbarService: SnackbarService) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.getNotification();
  }

  /** Logout Function */
  logout() {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '600px',
      height: '300px',
      data: {
        moduleName: 'Logout',
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((data) => {
      data !=undefined && this.authenticationService.logout();
    });
  }

    getNotification(){
      let userId = this.currentUser?.userId;
      this.crudService.get_path(`${appModels.NOTIFICATION}/${this.dynamicAPIUrl}/${appModels.USER}/${userId}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.notification = response;
        this.commonService.setLoaderShownProperty(false);
      })
    }
   
    notifyList(){
      if(this.notification.length>0){
        this.showNotifyList =  !this.showNotifyList;
        this.showNotifyList == true ? this.getNotification() : '';
      }
      else{
        this.snackbarService.openSnackbar(ResponseMessage.NO_NOTIFICATION, ResponseMessageTypes.WARNING);
        return;
      }
    }

   
    notificationRedirect(notificationList:any){
      let lableNameIndex = this.labelNames?.find((lableIndex: any) => lableIndex?.value == notificationList?.entityTypeId);
      this.selectedIndex = lableNameIndex.index;
      this.crudService.post(`${appModels.NOTIFICATION}/${appModels.NOTIFICATION}/${appModels.READ}/${notificationList?.id}`).pipe(untilDestroyed(this)).subscribe((response: any) => {
        this.commonService.setLoaderShownProperty(false);

        if(this.selectedIndex == 0){
            this.router.navigate([`policies/view-policies`],{queryParams: {entityId:notificationList.entityId, entityTypeId:notificationList.entityTypeId,selectedIndex:this.selectedIndex,menuName: lableNameIndex?.name} });
        }
        else{
            this.router.navigate([`case-management/case-management-view`],{queryParams: {entityId:notificationList.entityId, entityTypeId:notificationList.entityTypeId,selectedIndex:this.selectedIndex,menuName: lableNameIndex?.name} });
        }
        this.getNotification();
      })

    }

    chat(){
      this.showChatBot.emit(true);
    }

    help(){
      const dialogRef = this.dialog.open(HelpComponent, {
        width: '800px',
        // height: '500px',
        data: {
        },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe((data) => {
      });
    }

    logoRedirect(){
      window.open(logoURL?.url);
    }
}
