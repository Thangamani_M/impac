import {Component, Inject} from '@angular/core';
import { MatSnackBarRef} from '@angular/material/snack-bar';
import {MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';

/**
 * @title Snack-bar with a custom component
 */

@Component({
  selector: 'app-custom-snack-bar',
  templateUrl: './custom-snack-bar.component.html',
  styleUrls: ['./custom-snack-bar.component.scss']
})
export class CustomSnackBarComponent {
  constructor(public snackBarRef: MatSnackBarRef<CustomSnackBarComponent>,@Inject(MAT_SNACK_BAR_DATA) public data: any) { 
  }

}
