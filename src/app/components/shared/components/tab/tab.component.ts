import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  constructor() { }

  @Input() selectedIndex:number = 0;
  @Input() displayedLables: Array<any> = [];
  @Output() onSelectedTab = new EventEmitter();

  ngOnInit(): void {
  }

  onTabClicked(event:any){
    this.onSelectedTab.emit(event);
  }

}
