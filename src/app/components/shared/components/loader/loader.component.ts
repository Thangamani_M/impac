import { Component, OnInit,Input } from '@angular/core';
import { CommonService } from 'src/app/services';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  @Input()isLoading: boolean = false;
  isChatBotLoading:boolean = false;

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.onChatbotLoader();
  }
  
  onChatbotLoader(){
    this.commonService.getChatBotLoaderShownProperty().subscribe(({ isChatBotLoading }) => {
      this.isChatBotLoading = isChatBotLoading;
    }); 
  }
}
