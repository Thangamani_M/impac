import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { appModels, CrudService, DynamicAPIEndPoints, CommonService,SnackbarService,ResponseMessage,ResponseMessageTypes } from 'src/app/services';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {


  imageSource: any; 
  dynamicAPIUrl:string = DynamicAPIEndPoints?.SKILLS_URL;
  previewReq:any;
 
  constructor(public dialogRef: MatDialogRef<PreviewComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public sanitizer: DomSanitizer,private crudService: CrudService, private commonService: CommonService,private snackbarService: SnackbarService) {
    this.previewReq = data;

    if (this.previewReq) { 
      if(this.previewReq?.files?.fileName){
        this.crudService.get_path(`${appModels.ENTITIES}/${this.previewReq?.dynamicAPIurl}/${appModels.DOWNLOAD_FILE}/${this.previewReq?.files?.id}`).pipe(untilDestroyed(this)).subscribe((res: any) => {
          this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png/jpg/jpeg/gif/pdf/doc/docx/xls/xlsx;base64,${ res[this.previewReq?.files?.fileName]}`);
          this.commonService.setLoaderShownProperty(false);
        })
      }
      else{
        JSON.parse(this.previewReq.Upload)?.forEach((fileList:any)=>{
          this.crudService.get_path(`${appModels.ENTITIES}/${this.dynamicAPIUrl}/${appModels.DOWNLOAD_FILE}/${fileList?.id}`).pipe(untilDestroyed(this)).subscribe((res: any) => {
            this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png/jpg/jpeg/gif/pdf/doc/docx/xls/xlsx;base64,${ res[fileList?.fileName]}`);
            this.commonService.setLoaderShownProperty(false);
          })
        })
      }
    }
  }
  
  ngOnInit(): void {
  }

  /** Close the Dialog Function*/
  onNoClick(): void {
    this.dialogRef.close();
  }

  download(){
    this.commonService.downloadFile(this.previewReq?.files,this.previewReq?.dynamicAPIurl);
  }
  

  delete(){
    this.crudService.delete(`${appModels.ENTITIES}/${this.previewReq.dynamicAPIurl}/${appModels.DELETE_FILE}/${this.previewReq?.files?.id}`)
    .pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.commonService.setLoaderShownProperty(false);
      this.snackbarService.openSnackbar(ResponseMessage.DELETE, ResponseMessageTypes.SUCCESS);
      this.dialogRef.close("delete");
    })
  }

}
