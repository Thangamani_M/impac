import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CrudService,CommonService,SnackbarService,ResponseMessageTypes,ResponseMessage,AttributeType } from 'src/app/services';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy({ checkProperties: true })

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  modalForm: FormGroup = new FormGroup({});
  formResponse: any;
  modalFormObj: Array<any> = [];
  modalFormValues: Array<any> = [];
  entityAttributesValues: Array<any> = [];
  createFormValues: any;
  updateFormValues: Array<any> = [];
  attributeTypeValue = AttributeType;

  
  constructor(private crudService: CrudService,private commonService: CommonService, private snackbarService: SnackbarService, public dialog: MatDialog, private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DialogComponent>) {
    this.formResponse = data;
  }

  ngOnInit(): void {
    this.modalForm = this.createFormGroup();
  }
  //key press
  onkeypress(e: any) {
    this.commonService.onkeyUpTextbox(e);
  }
  //Key Up Text Area
  onKeyuptextarea(val: any) {
    this.commonService.onKeyUpTextarea(val);
  }
  // Key Press Text Field
  onkeypressTextField(e: any) {
    this.commonService.onKeyUpTextField(e);
  }
  //Create Form
  createFormGroup(): FormGroup {
    const group = this.formBuilder.group({});
    if (this.formResponse?.result?.length > 0) {
      this.formResponse?.result?.forEach((column: any) => {
        if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === true) {
        }
        else if (column?.isRequired == true && column?.isKey == true && column?.isDynamic === false && column.componentName == this.attributeTypeValue?.TEXT_BOX) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, [Validators.required]));
        }
        else if (column.isRequired == false) {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue));
        }
        else {
          group.addControl(column?.entityTypeAttributeName, this.formBuilder.control(column?.attributeValue, [Validators.required]));
        }
      });
    }
    return group;
  }
  //Create update
  createUpdate() {
    if (this.modalForm.invalid) {
      this.modalForm.markAllAsTouched();
      this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
      return;
    }
    else if (this.modalForm.pristine) {
      this.snackbarService.openSnackbar(ResponseMessage.NO_CHANGES, ResponseMessageTypes.WARNING);
      return;
    }
    this.modalFormObj = [];
    this.modalFormValues = [];
    Object.keys(this.modalForm.value)?.forEach((key) => {
      const currentControl = this.modalForm.controls[key];
      if (!currentControl.pristine && !currentControl.untouched) {
        this.modalFormObj.push({ [key]: this.modalForm.value[key] });
      }
    });
    this.formResponse?.result?.map((res: any) => {
      this.modalFormObj?.map((forms: any) => {
        if (res.entityTypeAttributeName == Object.keys(forms).toString()) {
          res.attributeValue = Object.values(forms).toString();
          this.modalFormValues.push(res);
        }
      })
    })
    if (this.formResponse.edit) {
      this.update();
    }
    else {
      this.create();
    }
  }
  // Create
  create() {
    this.entityAttributesValues = [];
    this.modalFormValues?.map((res: any) => {
      this.entityAttributesValues.push({ 'entityTypeAttributeId': res.id, 'attributeValue': res.attributeValue });
      this.createFormValues = {
        "entityTypeId": res.entityTypeId,
        "entityAttributes": this.entityAttributesValues,
        "isKey": res.isKey
      }
    })
    this.crudService.post(this.formResponse.api, this.createFormValues)
      .pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.dialogRef.close(res);
        this.snackbarService.openSnackbar(ResponseMessage.CREATE, ResponseMessageTypes.SUCCESS);
      })
  }
  // Up Date
  update() {
    this.updateFormValues = [];
    this.modalFormValues?.map((res: any) => {
      this.updateFormValues.push({
        "entityTypeId": this.formResponse.entityTypeID,
        "entityId": this.formResponse.entityId,
        "entityTypeAttributeId": res.entityTypeAttributeId,
        "id": res.attributeValueId ? res.attributeValueId : "",
        "attributeValue": res.attributeValue,
        "isKey": res.isKey
      })
    })
    this.crudService.update(this.formResponse.api, this.updateFormValues)
      .pipe(untilDestroyed(this)).subscribe(res => {
        this.dialogRef.close(res);
        this.snackbarService.openSnackbar(ResponseMessage.UPDATE, ResponseMessageTypes.SUCCESS);
      })
  }
  // No Click
  onNoClick(): void {
    this.dialogRef.close();
  }

  escalate(){
    this.dialogRef.close('Escalate');
  }
}
