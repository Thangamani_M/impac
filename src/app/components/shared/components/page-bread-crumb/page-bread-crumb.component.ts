import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services';

@Component({
  selector: 'app-page-bread-crumb',
  templateUrl: './page-bread-crumb.component.html',
  styleUrls: ['./page-bread-crumb.component.scss']
})
export class PageBreadCrumbComponent implements OnInit {
  breadcrumpitems:any;
  constructor(private commonService: CommonService ) { }

  ngOnInit(): void {
    this.onloadBreadCrumb();
  }

  onloadBreadCrumb(){
    this.commonService.getbreadcrumb().subscribe(res => {
      if(res){
          setTimeout(() => {
            this.breadcrumpitems = res;
          },500)
        }})
  }

}