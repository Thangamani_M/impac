import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Custom Forms,Router */
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

/** Custom Components */
import {SharedComponent,HeaderComponent,SidebarComponent,LoaderComponent,TableComponent,TabComponent,DialogComponent,DeleteComponent,CreateEditPageComponent,CustomSnackBarComponent,PageBreadCrumbComponent,ChatbotComponent,DragDropComponent,PaginationComponent,HelpComponent,PreviewComponent,DashboardComponent,FilterComponent} from 'src/app/components/shared';

/** Custom Feather Icon , Chart Module*/
import { FeatherModule } from 'angular-feather';
import { Plus,Eye,Edit,Trash2,Upload,Filter,Maximize2,Link,Search,LogOut,User,MoreVertical, X,Download, ArrowRight,CheckSquare,XSquare} from 'angular-feather/icons';
import { ChartsModule } from 'ng2-charts';

/** Custom DatePicker and Drag and Drop Module */
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { DndModule } from 'ngx-drag-drop';

/** Custom Angular Editor Module */
import { CdkAccordionModule} from '@angular/cdk/accordion';
import { NgxSummernoteModule } from 'ngx-summernote';

/** Custom DateTimePicker*/
import {
  NgxMatDateFormats,
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule,
  NGX_MAT_DATE_FORMATS
} from '@angular-material-components/datetime-picker';

import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';

/** Custom Material Module */
import { MatButtonModule} from '@angular/material/button';
import { MatInputModule} from '@angular/material/input';
import { MatListModule} from '@angular/material/list';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule} from '@angular/material/tabs';
import { MatIconModule} from '@angular/material/icon';
import { MatRadioModule} from '@angular/material/radio';
import { MatCardModule} from '@angular/material/card';
import { MatTableModule} from '@angular/material/table';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatDialogModule} from '@angular/material/dialog';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule} from '@angular/material/sort';
import { MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MaterialFileInputModule} from 'ngx-material-file-input';
import { MatChipsModule} from '@angular/material/chips';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTooltipModule} from '@angular/material/tooltip';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { StatusChangeDialogComponent } from './components/status-change-dialog/status-change-dialog.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';


/** Custom Feather Variable */
const featherIcons = {Plus,Eye,Edit,Trash2,Upload,Filter,Maximize2,Link,Search,LogOut,User,MoreVertical,X,Download,ArrowRight,CheckSquare,XSquare};

/** Custom DatePicker Format */
const CUSTOM_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  }
};

/** Custom DateTimePicker Format */
const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: 'YYYY-MM-DD, HH:mm:ss A',
  },
  display: {
    dateInput: 'YYYY-MM-DD, HH:mm:ss A',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};


/** Shared Module */
@NgModule({
  declarations: [SharedComponent, HeaderComponent, SidebarComponent, LoaderComponent,TableComponent, TabComponent, DialogComponent, DeleteComponent, CreateEditPageComponent, CustomSnackBarComponent, PageBreadCrumbComponent, ChatbotComponent, DragDropComponent, PaginationComponent, HelpComponent, PreviewComponent, DashboardComponent, FilterComponent, StatusChangeDialogComponent],
  imports: [
    CommonModule,
    MatSnackBarModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FeatherModule.pick(featherIcons),
    ChartsModule,
    DndModule,
    CdkAccordionModule,
    NgxSummernoteModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    NgxMatMomentModule,
    NgxExtendedPdfViewerModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatRadioModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatSlideToggleModule,
    MaterialFileInputModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ],
  entryComponents:[DialogComponent,DeleteComponent,CustomSnackBarComponent,PreviewComponent,FilterComponent,StatusChangeDialogComponent],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_FORMATS},
    {provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS},DatePipe],

  exports: [SharedComponent,HeaderComponent, SidebarComponent,LoaderComponent,TableComponent,TabComponent,DialogComponent,DeleteComponent,CreateEditPageComponent,ChatbotComponent,DragDropComponent,PaginationComponent,HelpComponent,PreviewComponent,DashboardComponent,FilterComponent,StatusChangeDialogComponent,

    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FeatherModule,
    ChartsModule,
    DndModule,
    CdkAccordionModule,
    NgxSummernoteModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    NgxMatMomentModule,
    NgxExtendedPdfViewerModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatRadioModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatSlideToggleModule,
    MaterialFileInputModule,
    MatChipsModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ]
})
export class SharedModule { }
