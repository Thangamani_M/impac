
import { Component, OnInit } from '@angular/core';

/** Custom Services */
import { CommonService } from 'src/app/services';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html'
})
export class SharedComponent implements OnInit {

  /** Variable Declaration */
  isLoading:boolean = false;
  isShowChatBot:boolean = false;


  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.getLoader();
    this.isShowChatBot = localStorage.getItem('isChatBot') == 'true';
  }

  /** Get Loader Service Function */
  getLoader(){
    this.commonService.getLoaderShownProperty().subscribe(({ isLoading }) => {
      setTimeout(()=>{
        this.isLoading = isLoading;
      },0);
    });
  }

  setChatbot(event:any){
    localStorage.setItem("isChatBot",event);
    this.isShowChatBot = event;
  }

}
