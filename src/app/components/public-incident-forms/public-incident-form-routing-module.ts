import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/** Custom Component */
import { PublicIncidentFormComponent } from './public-incident-form/public-incident-form.component';

/** Login Routes */
const routes: Routes = [
  { path: 'public-incident-form', component: PublicIncidentFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicIncidentFormRoutingModule { }
