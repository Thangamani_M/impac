import { Component, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators,AbstractControl } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { DatePipe } from '@angular/common';
import { appModels, ResponseMessageTypes,ResponseMessage, SnackbarService} from 'src/app/services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-public-incident-form',
  templateUrl: './public-incident-form.component.html',
  styleUrls: ['./public-incident-form.component.scss']
})
export class PublicIncidentFormComponent implements OnInit {
  publicIncidentForm: FormGroup;
  formData = new FormData();
  selectable = true;
  removable = true;
  selectedFile: any;
  listOfFiles: any[] = [];
  sitekey=environment.recaptcha.siteKey;
  isLoading:Boolean = false;

  constructor(private http: HttpClient,private datepipe: DatePipe,private snackbarService: SnackbarService) {
   }

  ngOnInit(): void {
    this.createForms();
  }
  createForms(){
    this.publicIncidentForm = new FormGroup({
      name: new FormControl('',Validators.required),
      email: new FormControl('',[Validators.required,this.commaSepEmail]),
      incidentDate: new FormControl('',Validators.required),
      description: new FormControl('',Validators.required),
      fileUpload:new FormControl(''),
      recaptcha:new FormControl('',Validators.required)
    });

  }
  commaSepEmail = (control: AbstractControl): { [key: string]: any } | null => {
    if(control.value != null){
        const emails = control.value.split(',');
        const forbidden = emails.some((email:any) => Validators.email(new FormControl(email)));
        return forbidden ? { 'email': { value: control.value } } : null;
        }
    else {
       return  { 'email': null};
    }
  };
  
  onFileSelected(file: any) {
    let FileData = file.target.files;
    for (let i = 0; i < FileData.length; i++) {
      this.selectedFile = FileData[i];
      this.listOfFiles.push(this.selectedFile)
    }
  }
  removeSelectedFile(index: any) {
      this.listOfFiles.splice(index, 1);
  }
  submit(){
    if(this.publicIncidentForm.invalid) {
      this.publicIncidentForm.markAllAsTouched();
      return this.snackbarService.openSnackbar(ResponseMessage.REQUIRE_FIELDS, ResponseMessageTypes.WARNING);
    }
    this.isLoading = true;
    this.publicIncidentForm.value.incidentDate = this.datepipe.transform(this.publicIncidentForm.value.incidentDate, 'yyyy-MM-ddTHH:mm:ss');
    let formData = new FormData();
    formData.append('name',this.publicIncidentForm.value.name);
    formData.append('email',this.publicIncidentForm.value.email);
    formData.append('incidentDate',this.publicIncidentForm.value.incidentDate);
    formData.append('description',this.publicIncidentForm.value.description);
    this.listOfFiles.forEach(res =>{
    formData.append('fileUpload',res);
    })
    this.http.post(`${appModels?.STATIC_INCIDENT}`,formData).subscribe((res: any) => {
      this.publicIncidentForm.reset();
      this.publicIncidentForm.markAsUntouched();
      this.listOfFiles.length = 0;
      this.snackbarService.openSnackbar(res.message, ResponseMessageTypes.SUCCESS);
      this.isLoading = false;
    },error =>{
      this.isLoading = false;
    });
  }
  resolved(captchaResponse: string) {
  }
}
