import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Custom Module */
import { PublicIncidentFormRoutingModule } from './public-incident-form-routing-module';
import { SharedModule } from '../shared/shared.module';

/** Custom Components */
import { PublicIncidentFormComponent } from './public-incident-form/public-incident-form.component';



@NgModule({
  declarations: [PublicIncidentFormComponent],
  imports: [
    CommonModule,
    PublicIncidentFormRoutingModule,
    SharedModule
  ]
})
export class PublicIncidentFormModule { }
