import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/** Custom Component */
import { IncidenceReportFormComponent } from './incidence-report-form.component';

/** Login Routes */
const routes: Routes = [
  { path: 'incident-report-form', component: IncidenceReportFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidenceReportFormRoutingModule { }
