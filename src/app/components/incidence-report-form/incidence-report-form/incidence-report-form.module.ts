import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Custom Module */
import { IncidenceReportFormRoutingModule } from './incidence-report-form-routing-module';
import { SharedModule } from '../../shared/shared.module';

/** Custom Components */
import { IncidenceReportFormComponent } from './incidence-report-form.component';



@NgModule({
  declarations: [IncidenceReportFormComponent],
  imports: [
    CommonModule,
    IncidenceReportFormRoutingModule,
    SharedModule
  ]
})
export class IncidenceReportFormModule { }
