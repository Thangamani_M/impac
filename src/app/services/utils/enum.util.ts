enum appModels { 
  LOGIN                                   = 'login',
  ENTITIES                                = 'entities',
  ENTITY_TYPE                             = 'entity-types',
  ENTITY_TYPES                            = 'entityType',
  DELETEINBATCH                           = 'deleteinbatch',
  GETALLWITHPAGINATION                    = 'getAllWithPagination',
  ENTITY_TYPE_DROPDOWN                    = 'entityType/dropdown',
  ENTITY_TYPE_ATTRIBUTES                  = 'entity-type-attributes',
  HEADERS                                 = 'headers',
  CREATE                                  = 'create',
  ATTRIBUTE_VALUE                         = 'attribute-value',
  PAGE                                    = 'page/entityTypeId',
  AUDIT_DATE                              = 'audit-date',
  AUDIT_BY_ENTITY                         = 'audit-by-entity',
  FILTER                                  = 'filter',
  FILTER_HEADERS                          = 'filter/headers',
  DASHBOARD                               = 'dashboard',
  STATUS                                  = 'status',
  COUNT                                   = 'count',
  UPLOAD_SUPPORT_DOCUMENT                 = 'uploadSupportingDocument',
  DOWNLOAD_FILE                           = 'downloadByModelId',
  DELETE_FILE                             = 'deleteSupportingDocument',
  EDIT_FILE                               = 'editUploadSupportingDocument',
  BLOG                                    = 'blog',
  GET_BLOG_DATA                           = 'getBlogData',
  RSS_FEEDS                               = 'rssFeed',
  GET_RSS_DATA                            = 'getRSSData',
  USER_MANAGEMENT                         = 'user-management',
  DETAILS                                 = 'details',
  ROLE                                    = 'role',
  ROLE_MENU                               = 'roleMenu',
  PERMISSION_BY_ID                        = 'permissionById',
  ENTITY_TYPE_ID                          = 'entity-type-id',
  VIEW                                    = 'view',
  MAPPING_SEARCH                          = 'mapping-search',
  COMPONENT                               = 'component',
  MAPPING                                 = 'mapping',
  SKILLSET_UPLOAD                         = 'skillset-upload',
  ENTITY_TYPE_TEMPLATE_ATTRIBUTES         = 'entity-type-template-attributes',
  EDIT                                    = 'edit',
  ENTITY                                  = 'entity',
  OTP_VERIFICATION                        = 'verify-otp',
  RESEND_OTP                              = 'resend-otp',
  CHANGE_PASSWORD                         = 'changePassword',
  FORGOT_PASSWORD                         = 'forgot-password',
  POLICY_APPROV_ROLES                     = 'policy-approve-roles',
  NOTIFICATION                            = 'notification',
  SKILLS                                  = 'skills',
  USER                                    = 'user',
  COMPONENT_VALUE                         = 'componentValue',
  ENTITY_TYPE_ATTRIBUTE                   = 'entity-type-attribute',
  POLICY_VERSION                          = 'policy-version',
  CREATE_POLICY_VERSION                   = 'policy/version',
  APPROVED_POLICY                         = 'approved/policy',
  CHAT_BOT                                = 'chatbot',
  ALL_ENTITY_TYPE                         = 'entity-types/getAll',
  STATIC_INCIDENT                         = 'public/incident',
  EXPORTEXCEL                             = 'exportExcel',
  ESCALATE                                = 'escalate',
  APP_USER                                = 'app-user',
  SETTING                                 = 'setting',
  READ                                    = 'read',
  REPORT                                  = 'report',
  EXPORTPDF                               = 'exportpdf',
  COMPONENTVALUE                          = 'component-value'
}

enum ResponseMessageTypes {
  SUCCESS   = 'success',
  WARNING   = 'warning',
  ERROR     = 'error',
  INFO      = 'info'
}

enum ResponseMessage {
  CREATE              = 'Created Successfully',
  UPDATE              = 'Updated Successfully',
  DELETE              = 'Deleted Successfully',
  SUCCESS_FULLY       = 'Successfully',
  SELECT_ITEM         = 'Please select atleast one item to delete',
  REQUIRE_FIELDS      = 'Please Enter Required Fields',
  NO_CHANGES          = 'No changes were Detected',
  TIME_VALIDATION     = 'End Time should be greater than Date Reported',
  FILE_UPLOAD         = 'Please Upload the File',
  COMPARE_ITEM        = 'Please select any two item to compare',
  PERMISSION_DENIED   = 'User Does Not have Permission',
  OTP_CREATE          = 'OTP Verified Successfull',
  OTP_SENT            = 'We have sent you an OTP to reset your password in email',
  PASSWORD_CHANGED    = 'Password Changed',
  FILE_TYPE           = 'Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx',
  LENGTH_SIZE_10      = '10 Characters can be allowed',
  LENGTH_SIZE_60      = '60 Characters can be allowed',
  LENGTH_SIZE_500     = '500 Characters can be allowed',
  SERVER_ERROR        = 'Server Error',
  NO_DATA             = 'No Data Found',
  NO_NOTIFICATION     = 'No notification found'
} 

enum SecretKey {
  SECRET_KEY = 'AMBERINNOVATIONSIMPAC'
}

enum TagList{
  TAG_LIST_ID = 8
}

enum Rss_Feeds_Filter{
  FILTER_ID = 97
}

enum EntityTypeId{
  EVENT                   = 1,
  CASE                    = 2,
  INCIDENT                = 3,
  CASE_TYPE               = 4,
  INVESTIGATION_TEMPLATE  = 6,
  CASE_FORM               = 7,
  TAG                     = 8,
  SKILL_SET               = 9,
  RSS_FEEDS               = 11,
  BLOG                    = 12,
  USER_MANAGEMENT         = 13,
  GROUP_MANAGEMENT        = 14,
  POLICIES                = 15,
  ROLE                    = 16,
  AUDIT_LOG               = 17,
  DASHBOARD               = 18,
  SKILLSET_TYPE           = 19,
  SKILLS                  = 20,
  LEGISLATION             = 22,
  SIDEBAR                 = 23,
  MEMBER_STATE            = 24,
  PUBLIC_INCIDENT         = 25
}

enum DynamicAPIEndPoints{
    CASE_TYPE_URL               = "Case Type",
    CASE_FORM_URL               = "Case Form",
    INVESTIGATION_TEMPLATE_URL  = "Investigation Template",
    TAG_URL                     = "Tag",
    GROUP_MANAGEMENT_URL        = "Group Management",
    USER_MANAGEMENT_URL         = "User Management",
    AUDIT_LOG_URL               = "Audit Logs",
    ROLE_URL                    = "Role",
    ROLE_MENU_URL               = "roleMenu",
    DASHBOARD_URL               = "dashboard",
    INCIDENT_URL                = "Incident",
    CASE_URL                    = "Case",
    EVENT_URL                   = "Event",
    SKILLS_URL                  = "Skills",
    SKILL_SET_URL               = "Skillset",
    SKILL_SET_TYPE_URL          = "Skillset Type",
    POLICIES_URL                = "Policies",
    LEGISLATION_URL             = "Legislation",
    RSS_FEEDS_URL               = "RSS Feeds",
    BLOG_URL                    = "Blog",
    TASK_TEMPLATE_URL           = "Task Template",
    NOTIFICATION_URL            = "notification",
    MENU_URL                    = "menu",
    PUBLIC_INCIDENT_URL         = "Public Incident"
}

enum AttributeType{
  TEXT_BOX          = 'TextBox',
  TEXT_FIELD        = 'TextField',
  TEXT_AREA         = 'TextArea',
  DROP_DOWN         = 'DropDown',
  DATE_TIME         = 'DateTime',
  UPLOAD            = 'Upload',
  TOGGLE            = 'Toggle',
  LABEL             = 'Label',
  RICH_TEXT_EDITOR  = 'RichTextEditor',
  EMAIL             = 'Email',
  NUMBER            = 'Number',
  COMMENTS          = 'Comments',
  REPLY             = 'Reply',
  CHECK_BOX         = 'Checkbox',
  PREVIEW           = 'Preview',
  MULTI_DROP_DOWN   = 'MultiDropDown'
}

const configurationTabList =  [
  { name: "Case Type", value: EntityTypeId?.CASE_TYPE, dynamicAPIUrl: DynamicAPIEndPoints?.CASE_TYPE_URL },
  { name: "Case Form", value: EntityTypeId?.CASE_FORM, dynamicAPIUrl: DynamicAPIEndPoints?.CASE_FORM_URL},
  { name: "Investigation Template", value: EntityTypeId?.INVESTIGATION_TEMPLATE, dynamicAPIUrl: DynamicAPIEndPoints?.INVESTIGATION_TEMPLATE_URL},
  { name: "Tag", value: EntityTypeId?.TAG, dynamicAPIUrl: DynamicAPIEndPoints?.TAG_URL  },
  { name: "Skillset Type", value: EntityTypeId?.SKILLSET_TYPE, dynamicAPIUrl: DynamicAPIEndPoints?.SKILL_SET_TYPE_URL },
];

const adminTabList = [
  { name: "Group Management", value: EntityTypeId?.GROUP_MANAGEMENT, dynamicAPIUrl: DynamicAPIEndPoints?.GROUP_MANAGEMENT_URL},
  { name: "User", value: EntityTypeId?.USER_MANAGEMENT, dynamicAPIUrl: DynamicAPIEndPoints?.USER_MANAGEMENT_URL},
  { name: "Audit Logs", value: EntityTypeId?.AUDIT_LOG , dynamicAPIUrl: DynamicAPIEndPoints?.AUDIT_LOG_URL},
  { name: "Roles And Permission", value: EntityTypeId?.ROLE, dynamicAPIUrl: DynamicAPIEndPoints?.ROLE_URL},
];

const caseManagement_List_View_TabList = [
  { name: "Dashboard", value: EntityTypeId?.DASHBOARD, dynamicAPIUrl: DynamicAPIEndPoints?.DASHBOARD_URL},
  { name: "Incident", value: EntityTypeId?.INCIDENT, dynamicAPIUrl: DynamicAPIEndPoints?.INCIDENT_URL},
  { name: "Case", value: EntityTypeId?.CASE, dynamicAPIUrl: DynamicAPIEndPoints?.CASE_URL},
  { name: "Event", value: EntityTypeId?.EVENT, dynamicAPIUrl: DynamicAPIEndPoints?.EVENT_URL}
];
const caseManagement_Public_List_TabList = [
  { name: "Dashboard", value: EntityTypeId?.DASHBOARD, dynamicAPIUrl: DynamicAPIEndPoints?.DASHBOARD_URL},
  { name: "Incident", value: EntityTypeId?.INCIDENT, dynamicAPIUrl: DynamicAPIEndPoints?.INCIDENT_URL},
  { name: "Case", value: EntityTypeId?.CASE, dynamicAPIUrl: DynamicAPIEndPoints?.CASE_URL},
  { name: "Event", value: EntityTypeId?.EVENT, dynamicAPIUrl: DynamicAPIEndPoints?.EVENT_URL},
  { name: "Public Incident", value: EntityTypeId?.PUBLIC_INCIDENT, dynamicAPIUrl: DynamicAPIEndPoints?.PUBLIC_INCIDENT_URL},
];

const caseManagement_Info_TabList = [
  { name: "Incident", value: EntityTypeId?.INCIDENT,dynamicAPIUrl: DynamicAPIEndPoints?.INCIDENT_URL},
  { name: "Case", value: EntityTypeId?.CASE, dynamicAPIUrl: DynamicAPIEndPoints?.CASE_URL},
  { name: "Event", value: EntityTypeId?.EVENT, dynamicAPIUrl: DynamicAPIEndPoints?.EVENT_URL},
];

const myProfileTabList = [
  { name: "My Profile", value: EntityTypeId?.USER_MANAGEMENT, dynamicAPIUrl: DynamicAPIEndPoints?.USER_MANAGEMENT_URL },
  { name: "Skillset", value: EntityTypeId?.SKILL_SET},
];

const mySkillsetTabList = [
  { name: "My Profile", value: EntityTypeId?.USER_MANAGEMENT },
  { name: "Skillset", value: EntityTypeId?.SKILLS,dynamicAPIUrl: DynamicAPIEndPoints?.SKILLS_URL },
];

const policyLegislationListTabList = [
  { name: "Policies", value: EntityTypeId?.POLICIES, dynamicAPIUrl: DynamicAPIEndPoints?.POLICIES_URL},
  { name: "Legislation", value: EntityTypeId?.LEGISLATION, dynamicAPIUrl: DynamicAPIEndPoints?.LEGISLATION_URL},
];
 
const viewPolicyTabList =  [
  { name: "Policy Info", value: "", dynamicAPIUrl: DynamicAPIEndPoints?.POLICIES_URL},
  { name: "Approval History", value: "", dynamicAPIUrl: DynamicAPIEndPoints?.POLICIES_URL},
  { name: "Version", value: "", dynamicAPIUrl: DynamicAPIEndPoints?.POLICIES_URL},
];

const rss_FeedsTabList = [
  {name:"Rss-Feeds",value: EntityTypeId?.RSS_FEEDS, dynamicAPIUrl: DynamicAPIEndPoints?.RSS_FEEDS_URL}
];

const blogListTabList = [
  { name: "Blog", value: EntityTypeId?.BLOG, dynamicAPIUrl: DynamicAPIEndPoints?.BLOG_URL}
];

const skillsListTabList =  [
  { name: "Skills",value: EntityTypeId?.SKILL_SET, dynamicAPIUrl: DynamicAPIEndPoints?.SKILLS_URL},
];

const headerTabList = [
  { name: "Policies", value: EntityTypeId?.POLICIES, index:0},
  { name: "Incident", value: EntityTypeId?.INCIDENT, index:1},
  { name: "Case", value: EntityTypeId?.CASE, index:2},
  { name: "Event", value: EntityTypeId?.EVENT, index:3},
];

const defaultSortingBy = {
  active: 'createdOn',
  direction: 'desc'
};

const supportedFileType = [
  'jpeg',
  'jpg',
  'png',
  'gif',
  'pdf',
  'doc',
  'docx',
  'xls',
  'xlsx',
];

const previewFileType = {
 JPEG: 'jpeg',
 JPG: 'jpg',
 PNG: 'png',
}

const pieChartEntityTypeName = {Incident:'Incident', Case:'Case', Event:'Event', RSSFeeds:'RSS Feeds', Blog:'Blog'}


enum logoURL{
  url = 'https://caricomimpacs.org/'
}

enum ConstantValue {
  Value = 'SUPER ADMIN',
  PolicyFile = 'Policy Files',
  SessionExpiry = 'User session invalid. Please try login ',
  CreateResearch = 'Create New Resource'
}
 
const sideBarRouting = {createCase:'/case-management-form/create-form', editCase:'/case-management-form/edit-form',taskTemplateCase:'/case-management/task-template',viewCase:'/case-management/case-management-view',infoCase:'/case-management/case-management-info',
createPolicy:'/policies-form/create-form', editPolicy:'/policies-form/edit-form', viewPolicy:'/policies/view-policies',
createRSS:'/rss-feeds-form/create-form', searchRSS:'/rss-feeds/search-rss-feeds-list',viewRSS:'/rss-feeds/rss-feeds-details', createBlog:'/blog-form/create-form',editBlog:'/blog-form/edit-form',
viewSkills:'/skills/view-skills',
createRole:'/roles-permission/create-roles-permission',viewRole:'/roles-permission/view-roles-permission',audit:'/audit-log/audit-log-details',createAdmin:'/admin-form/create-form',editAdmin:'/admin-form/edit-form',
createConfiguration:'/configuration-form/create-form', editConfiguration:'/configuration-form/edit-form',publicIncidentView:'/case-management/public-incident-view'};

const helpPageDetails =  [{question:"How to create a User?", answer:"Select Admin module, Select user management and create account",vedioLink:"https://www.youtube.com/watch?v=ekQ-K1769yQ&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=8"},
{question:"How to create a Case Type?",answer:"Select Configuration , Create new case type",vedioLink:"https://www.youtube.com/watch?v=q7MUCynpRFw&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=6"},
{question:"How to create a Case Form?", answer:"Select Configuration , Create new case form",vedioLink:"https://www.youtube.com/watch?v=gDinGmX00i8&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=7"},
{question:"How to create a Incident?", answer:"Select Case Management , user can able to Incident tab , click and fill all details",vedioLink:"https://www.youtube.com/watch?v=BdidckDSLj0&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=5"},
{question:"How to create a Case?", answer:"Select Case Management , user can able to Case tab , click and fill all details",vedioLink:"https://www.youtube.com/watch?v=F5heKtVj0eQ&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=4"},
{question:"How to create a Event?", answer:"Select Case Management , user can able to Event tab , click and fill all details",vedioLink:"https://www.youtube.com/watch?v=HNtUKTvyzqM&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=3"},
{question:"How to create an Rss Feed?", answer:"Select RSS feeds, create new feed",vedioLink:"https://www.youtube.com/watch?v=qHTfXON6Qfw&list=PLUDLDlqBohsMQ6DDcQHoMfAbTpL_7ZRIe&index=1"}
];

enum menuNameList{
  ADMIN           = 'Admin',
  CONFIGURATION   = 'Configuration',
  ROLE            = 'Role',
  POLICIES        = 'Policies',
  CASEMANAGEMENT  = 'Case-Management',
  BLOG            = 'Blog',
  RSSFEEDS        = 'RSS-Feeds',
  SKILLSET        = 'Skillset',
  SKILLS          = 'Skills',
  SETTING         = 'Setting',
  REPORT          = 'Report'
}

enum entityTypeAttributeName {
  Email = 'Email',
  PhoneNumber = 'Phone Number',

}

enum ResearchType{
  VIDEO = 'Video',
  RSS_FEEDS = 'RSS Feeds',
  WEB_PAGE = 'Web Pages',
  FORUMS ='Forums',
  CASE_STUDY = 'Case Study',
  EBOOKS = 'ebooks'
}

export { appModels,ResponseMessageTypes,ResponseMessage,SecretKey,TagList,Rss_Feeds_Filter,EntityTypeId,DynamicAPIEndPoints,AttributeType, configurationTabList,adminTabList, caseManagement_List_View_TabList,caseManagement_Public_List_TabList,caseManagement_Info_TabList,myProfileTabList, mySkillsetTabList,policyLegislationListTabList,viewPolicyTabList,rss_FeedsTabList,blogListTabList,skillsListTabList,headerTabList,defaultSortingBy,supportedFileType,previewFileType,pieChartEntityTypeName,logoURL,ConstantValue,sideBarRouting,helpPageDetails,menuNameList,entityTypeAttributeName,ResearchType}
