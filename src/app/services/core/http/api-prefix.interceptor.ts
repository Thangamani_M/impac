import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/** Custom Environment,Service */
import { environment } from '../../../../environments/environment';
import { appModels,AuthenticationService } from 'src/app/services';


/** ApiPrefixInterceptor Interceptor Service */
@Injectable()
export class ApiPrefixInterceptor implements HttpInterceptor {

  currentToken:any;

  constructor(private authenticationService: AuthenticationService) {}

  /** ApiPrefixInterceptor Interceptor Function */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      this.currentToken = this.authenticationService.currentTokenValue;

          if ((request.url.includes(`${appModels.LOGIN}`)  || (request.url.includes(`${appModels.CHANGE_PASSWORD}`)) || (request.url.includes(`${appModels.FORGOT_PASSWORD}`)) || (request.url.includes(`${appModels.OTP_VERIFICATION}`)) || (request.url.includes(`${appModels.RESEND_OTP}`)) || (request.url.includes(`${appModels.CHAT_BOT}`)) || (request.url.includes(`${appModels.STATIC_INCIDENT}`)) )) {
            let headers: HttpHeaders = new HttpHeaders({
              "Accept": "application/json"
            });
            request = request.clone({
              url: environment.BACKEND_URL + request.url,
              headers
            });
          }
          else{
            let headers: HttpHeaders = new HttpHeaders({
              "Accept": "application/json" ,
              Authorization: `Bearer ${this.currentToken}`,
            });
            request = request.clone({
              url: environment.BACKEND_URL + request.url,
              headers
            });
          }
          return next.handle(request);
    }
}