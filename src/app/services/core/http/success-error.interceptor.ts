import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

/** Custom Services */
import { CommonService,AuthenticationService,ResponseMessageTypes,SnackbarService,appModels,ResponseMessage, ConstantValue } from 'src/app/services';

/** Success Error Interceptor Service */
@Injectable()
export class SuccessErrorInterceptor implements HttpInterceptor {

  constructor(private commonService: CommonService,private snackbarService: SnackbarService,private authenticationService: AuthenticationService) { }

  /** Error Interceptor Function */
      // Handle the error responses and its messages
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request['url'].includes(appModels?.CHAT_BOT)) {
      this.commonService.setChatBotLoaderShownProperty(true);
    } 
    else{
      this.commonService.setLoaderShownProperty(true);
    }
        return next.handle(request).pipe(
            map(response =>
                this.successHandler(response)
            ),
            catchError(err => {
              if(err.status === 400 || err.status === 409 || err.status === 404 || err.status === 417 || err.status === 403){
                  this.snackbarService.openSnackbar(err.error?.message, ResponseMessageTypes.ERROR);
                }
                else if(err.status === 500){
                  this.snackbarService.openSnackbar(ResponseMessage.SERVER_ERROR, ResponseMessageTypes.ERROR);
                }
                else{
                  this.snackbarService.openSnackbar(ResponseMessage.SERVER_ERROR, ResponseMessageTypes.ERROR);
                }
                const error = err.error?.message || err.statusText;
                if (error) {
                  this.commonService.setLoaderShownProperty(false);
                }
                return throwError(err);
            }))

    }

  /** Success Interceptor Function */
    // Handle the success responses and its messages
   successHandler(response: HttpEvent<any>): HttpEvent<any> {
        if (response instanceof HttpResponse) {
          response.headers.get('RefreshToken') != null && this.authenticationService.setRefreshToken(response.headers.get('RefreshToken'));
          if(response.status == 203 && !response?.url?.includes(appModels?.NOTIFICATION)){
            this.snackbarService.openSnackbar(response?.body?.message, ResponseMessageTypes.ERROR);
            response?.body?.message == ConstantValue.SessionExpiry && this.authenticationService.logout();
            this.commonService.setLoaderShownProperty(false);
            let permissionResponse:any = [response?.body];
            return permissionResponse;
          }
          return response;
        }
        return response;
    }

}