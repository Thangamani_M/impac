import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';

/** Custom Service */
import {AuthenticationService,SessionExpiryService} from 'src/app/services';

/** AuthenticationGuard Service */
@Injectable({ providedIn: 'root' })
export class AuthenticationGuard implements CanActivate {

  currentToken:any;

  constructor(private router: Router, private authenticationService:AuthenticationService,private sessionExpiryService: SessionExpiryService) {}

  /** AuthenticationGuard CanActivate Function */
  canActivate() {
    this.currentToken = this.authenticationService.currentTokenValue;
          if(this.currentToken !== null || undefined){
            this.sessionExpiryService.getIdleSessionExpiry();
            return true;
          }
          else{
            this.router.navigate(['login']);
            return false;
          }
    }
}