import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";
import { Router} from '@angular/router';
import * as CryptoJS from 'crypto-js';

/** Custom Router,Service,Component*/
import { appModels,SecretKey} from 'src/app/services';


/** Authentication Service */
@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

  current_user:any;
  secretKey = SecretKey?.SECRET_KEY;
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient,private router: Router) {
     this.current_user = (localStorage.getItem('currentUser'));
     this.currentUserSubject = new BehaviorSubject<any>(this.current_user);
     this.currentUser = this.currentUserSubject.asObservable();
     }

    public get currentUserValue() {
       return this.decryptCredential();
    }

    public get currentTokenValue() {
      return this.currentUserValue?.token;
    }
/** Login */
    login(loginForm: Object) {
      return this.http.post<any>(`${appModels.LOGIN}`, loginForm).pipe(map(user => {
                if (user.status === 200) {
                    this.encryptCredential(user);
                }
                return user;
            }));
    }

    encryptCredential(userdetail:string){
        let encryptedUser = CryptoJS.AES.encrypt(JSON.stringify(userdetail), this.secretKey).toString();
        localStorage.setItem('currentUser',encryptedUser);
        localStorage.setItem('archive','false');
        this.currentUserSubject.next(encryptedUser);
    }   
    
    decryptCredential(){
        let decryptCurrentUser = this.currentUserSubject.value !== null && CryptoJS.AES.decrypt(this.currentUserSubject.value, this.secretKey); 
        let decryptedUser = JSON.parse(decryptCurrentUser.toString(CryptoJS.enc.Utf8));
        return decryptedUser;
    }

/** OTP Verification */
    otpVerification(otpVerificationForm: Object) {
      return this.http.post<any>(`${appModels.OTP_VERIFICATION}`, otpVerificationForm).pipe(map(user => {
                return user;
            }));
    }

/** Resend OTP */
    resendOTP(resendOTPForm: Object) {
      return this.http.post<any>(`${appModels.RESEND_OTP}`, resendOTPForm).pipe(map(user => {
                return user;
            }));
    }
    
/** Reset Password */
    changePassword(changePasswordForm: Object) {
      return this.http.post<any>(`${appModels.CHANGE_PASSWORD}`, changePasswordForm).pipe(map(user => {
                return user;
            }));
    }

/** Forgot Password */
    forgotPassword(forgotPasswordForm: Object) {
      return this.http.post<any>(`${appModels.FORGOT_PASSWORD}`, forgotPasswordForm).pipe(map(user => {
                return user;
            }));
    }

/** Logout */
    logout(){
      localStorage.clear();
      this.currentUserSubject.next(null);
      this.router.navigate(['login']);
    }

/** ChatBot */
    chatBot(chatBotForm: Object) {
      return this.http.post<any>(`${appModels.CHAT_BOT}`, chatBotForm).pipe(map(chatbot => {
                return chatbot;
            }));
    }

/** RefreshToken */
    setRefreshToken(refreshToken:any){
        let decryptedUser =  this.decryptCredential();
        decryptedUser.token = refreshToken;
        this.encryptCredential(decryptedUser);
    }
}