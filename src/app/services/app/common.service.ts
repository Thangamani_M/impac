import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable, Subject } from 'rxjs';
import { CrudService, SnackbarService, appModels, ResponseMessageTypes, ResponseMessage} from 'src/app/services';
import { DomSanitizer } from '@angular/platform-browser';
import { saveAs } from 'file-saver';
import { DatePipe } from '@angular/common';
import { HttpClient } from "@angular/common/http";
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
    providedIn: 'root'
})
export class CommonService {
    constructor(private crudService: CrudService, private snackbarService: SnackbarService, public sanitizer: DomSanitizer,private datepipe: DatePipe,private http: HttpClient) { }

    breadcrumbitems = new BehaviorSubject<any>(null);
    sidebaritems = new BehaviorSubject<any>(null);
    subject = new Subject<any>();
    dataurl: any;
    tableColumnSizeValue: number;
    startTime: any;
    endTime: any;

  setTableColumnSize(values:number){
    if(values >= 1 && values <=5){
      this.tableColumnSizeValue = Math.round(values /12);
    }
    else{
      this.tableColumnSizeValue = Math.round(values /3);
    }
    return this.tableColumnSizeValue;
  }

/**Breadcrumb Service */   
  setbreadcrumb(breadcrumbValues:any){
    this.breadcrumbitems.next(breadcrumbValues);
  }
  getbreadcrumb(){
    return this.breadcrumbitems
  }
/**Sidebar Service */  
  setSidebar(items:any){
    this.sidebaritems.next(items);
  }
  getSidebar(){
    return this.sidebaritems;
  }
/**Loader Service */  
  setLoaderShownProperty(isLoading: boolean): void {
    this.subject.next({ isLoading });
  }
  getLoaderShownProperty(): Observable<any> {
    return this.subject.asObservable();
  } 
/**ChatbotLoader Service */  
  setChatBotLoaderShownProperty(isChatBotLoading: boolean): void {
    this.subject.next({ isChatBotLoading });
  }

  getChatBotLoaderShownProperty(): Observable<any> {
    return this.subject.asObservable();
  }
/**Item Per Page Service */  
  pageSizeOption(value:number){
    const pageSizeOptions = value >10 && value <20 && [10,20] || value >20  && value <50  && [10,20,50] || value >50 && [10,20,50,100] || [5,10];
    return pageSizeOptions;
  }
/**Download Files */  
  downloadFile(fileList:any, dynamicAPIUrl:string){
    if(fileList?.id){
      this.crudService.get_path(`${appModels.ENTITIES}/${dynamicAPIUrl}/${appModels.DOWNLOAD_FILE}/${fileList.id}`).pipe().subscribe((res: any) => {
      this.dataurl = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png/jpg/jpeg/gif/pdf/doc/docx/xls/xlsx;base64, ${res[fileList.fileName]}`)
      const arr = this.dataurl.changingThisBreaksApplicationSecurity.split(',');
      this.onFileSave(arr, fileList.fileName);
      this.setLoaderShownProperty(false);
    })
   } else{      
    const file = fileList;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.dataurl = reader.result;
      const arr = this.dataurl.split(',');
      this.onFileSave(arr, fileList.name);
    };
  }
  }
  onFileSave(arr:any,files:any){
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    saveAs(new File([u8arr], files));
  }

  onkeyUpTextbox(event:any){
    if (event.target.value.length >= 60) {
      this.snackbarService.openSnackbar(ResponseMessage.LENGTH_SIZE_60, ResponseMessageTypes.WARNING);
      return
    }
    var charCode = String.fromCharCode(event.keyCode);
    // Only Characters
    if (/[a-zA-Z_ ]/.test(charCode)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  onKeyUpTextField(event:any){
    if (event.target.value.length >= 60) {
      this.snackbarService.openSnackbar(ResponseMessage.LENGTH_SIZE_60, ResponseMessageTypes.WARNING);
      return
    }
    var charCode = String.fromCharCode(event.keyCode);
    if (/^[a-zA-Z0-9-@#&%_ ]+$/.test(charCode)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  onKeyUpNumbersOnly(value:any){
    if (value.target.value.length >= 10) {
      this.snackbarService.openSnackbar(ResponseMessage.LENGTH_SIZE_10, ResponseMessageTypes.WARNING);
      return
    }
    var charCode = String.fromCharCode(value.keyCode);
    if (/^[0-9]*$/.test(charCode)) {
      return true;
    } else {
      value.preventDefault();
      return false;
    }
  }
  
  onKeyUpTextarea(value:any){
      value.target.value.length >= 500 && this.snackbarService.openSnackbar(ResponseMessage.LENGTH_SIZE_500, ResponseMessageTypes.WARNING);
      return
  }
  
  onKeyUpNumber(value:any){
    value.target.value.length >= 10 && this.snackbarService.openSnackbar(ResponseMessage.LENGTH_SIZE_10, ResponseMessageTypes.WARNING);
    return
  }

  onChangeTime(formValues:any){
    this.startTime = this.datepipe.transform(formValues.value['Date Reported']?._d ? formValues.value['Date Reported']?._d : formValues.value['Date Reported'], 'yyyy-MM-dd, HH:mm:ss a');
    this.endTime = this.datepipe.transform(formValues.value['End Time']?._d ? formValues.value['End Time']?._d : formValues.value['End Time'], 'yyyy-MM-dd, HH:mm:ss a');
    if (this.startTime > this.endTime) {
      this.snackbarService.openSnackbar(ResponseMessage.TIME_VALIDATION, ResponseMessageTypes.WARNING);
      formValues.controls['End Time'].reset()
      return
    }
  }

  editorConfiguration= {
    airMode: false,
    tabDisable: true,
    popover: {
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
      link: [['link', ['linkDialogShow', 'unlink']]],
      air: [['font',['bold','italic','underline']]]
    },
    height: '200px',
    toolbar: [['font',['bold','italic','underline']],
      ['fontsize', ['fontname', 'fontsize']],
      ['para', ['style0', 'ul', 'ol', 'paragraph']],
      ['insert', ['picture', 'link', 'video']],
    ],
    codeviewFilter: true,
    codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true
  };

  editorConfigurationVideoType= {
    airMode: false,
    tabDisable: true,
    popover: {
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
      link: [['link', ['linkDialogShow', 'unlink']]],
      air: [['font',['bold','italic','underline']]]
    },
    height: '200px',
    toolbar: [['font',['bold','italic','underline']],
      ['fontsize', ['fontname', 'fontsize']],
      ['para', ['style0', 'ul', 'ol', 'paragraph']],
      ['insert', ['video']],
    ],
    codeviewFilter: true,
    codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true
  };

  editorConfigurationWebpageType= {
    airMode: false,
    tabDisable: true,
    popover: {
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
      link: [['link', ['linkDialogShow', 'unlink']]],
      air: [['font',['bold','italic','underline']]]
    },
    height: '200px',
    toolbar: [['font',['bold','italic','underline']],
      ['fontsize', ['fontname', 'fontsize']],
      ['para', ['style0', 'ul', 'ol', 'paragraph']],
      ['insert', ['picture', 'link']],
    ],
    codeviewFilter: true,
    codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true
  };

  editorConfigurationEbookType= {
    airMode: false,
    tabDisable: true,
    popover: {
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
      link: [['link', ['linkDialogShow', 'unlink']]],
      air: [['font',['bold','italic','underline']]]
    },
    height: '200px',
    toolbar: [['font',['bold','italic','underline']],
      ['fontsize', ['fontname', 'fontsize']],
      ['para', ['style0', 'ul', 'ol', 'paragraph']],
      ['insert', ['link']],
    ],
    codeviewFilter: true,
    codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true
  };
  // ** Export Files ** //
  onFileDownload(entityName:any,entityID:any,selectedRecordIDs?:any){
    if(selectedRecordIDs != undefined){
      let multipleIds:any = [];
      multipleIds.push(selectedRecordIDs)
      this.http.get(`${appModels.ENTITY_TYPE}/${entityName}/${appModels.EXPORTEXCEL}/${entityID}`, {params:{entities:multipleIds},
      responseType: 'blob' as 'blob'}).subscribe((res: any) => {
        const blob = new Blob([res], { type: 'application/octet-stream' });
        const responseData= window.URL.createObjectURL(blob);
        saveAs(responseData, `${entityName}`+ EXCEL_EXTENSION);
        this.setLoaderShownProperty(false);
      })
  }
  else{
    this.http.get(`${appModels.ENTITY_TYPE}/${entityName}/${appModels.EXPORTEXCEL}/${entityID}`, {
      responseType: 'blob' as 'blob'}).subscribe((res: any) => {
        const blob = new Blob([res], { type: 'application/octet-stream' });
        const responseData= window.URL.createObjectURL(blob);
        saveAs(responseData, `${entityName}`+ EXCEL_EXTENSION);
        this.setLoaderShownProperty(false);
      })
  }
  }
  onFilterByFileDownload(request:any,entityName:any,entityID:any){
    request = JSON.parse(request);
    this.http.post(`${appModels.ENTITY_TYPE}/${entityName}/${appModels.EXPORTEXCEL}/${entityID}`, request, { responseType: 'blob' as 'blob'}).subscribe((res: any) => {
      const blob = new Blob([res], { type: 'application/octet-stream' });
      const responseData= window.URL.createObjectURL(blob);
      saveAs(responseData, `${entityName}`+ EXCEL_EXTENSION);
      this.setLoaderShownProperty(false);
    })
  }
}