import { Injectable } from '@angular/core';

/** Custom Router,Service,Component */
import { Routes, Route } from '@angular/router';
import { AuthenticationGuard } from "src/app/services";
import { SharedComponent} from "src/app/components/shared";

/** Shared Service */
@Injectable({
  providedIn: 'root'
})
/** Provides helper methods to create routes.*/
export class Shared {
  /** Creates routes using the Shared component and authentication.
   * @param routes The routes to add.
   * @return The new route using Shared as the base.
   */
  static childRoutes(routes: Routes): Route {
    return {
      path: '',
      component: SharedComponent,
      children: routes,
      canActivate: [AuthenticationGuard],
      // Reuse SharedComponent instance when navigating between child views
      // data: { reuse: true }
    };
  }
}



