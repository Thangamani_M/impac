import { Injectable } from "@angular/core";

/** Custom Service */
import { AuthenticationService } from 'src/app/services';

const MINUTES_UNITL_AUTO_LOGOUT = 45 // in mins
const CHECK_INTERVAL = 1000 // in ms
const STORE_KEY =  'lastAction';

@Injectable(
 {
  providedIn: 'root'
}
)
export class SessionExpiryService {
  val: any;

  constructor(private authenticationService: AuthenticationService) {}

  public getLastAction() {
    return Number(localStorage.getItem(STORE_KEY));
  }
  public setLastAction(lastAction: number) {
    localStorage.setItem(STORE_KEY, lastAction.toString());
  }

  getIdleSessionExpiry(){
    this.check();
    this.initListener();
    this.initInterval();
    localStorage.setItem(STORE_KEY,Date.now().toString());
  }

  initListener() {
    document.body.addEventListener('click', () => this.reset());
    document.body.addEventListener('mouseover',()=> this.reset());
    document.body.addEventListener('mouseout',() => this.reset());
    document.body.addEventListener('keydown',() => this.reset());
    document.body.addEventListener('keyup',() => this.reset());
    document.body.addEventListener('keypress',() => this.reset());
    window.addEventListener("storage",() => this.storageEvt());
  }

  reset() {
    this.setLastAction(Date.now());
  }

  initInterval() {
   setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);
  }

  check() {
    const now = Date.now();
    const timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    if (isTimeout)  {
      if(localStorage.getItem('currentToken') !== null){
      this.authenticationService.logout();
      }
      this.authenticationService.logout();
    }
  }

  storageEvt(){
    this.val = localStorage.getItem(STORE_KEY);
  }

}