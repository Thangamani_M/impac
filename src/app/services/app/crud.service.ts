import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

/** Crud Service */
@Injectable({
    providedIn: 'root'
})
export class CrudService {
    constructor(private http: HttpClient) { }

/** Create API Function */
    post(urlPath: string, body?: any): Observable<any> {
        return this.http.post(urlPath, body);
    }
/** Get API Function */
    get(urlPath: string): Observable<any> {
        return this.http.get(urlPath);
    }
/** Get with Params API Function */
    get_params(urlPath: string,params?: any): Observable<any> {
      return this.http.get(urlPath,params);
    }
/** Get with Path Variable API Function */
    get_path(urlPath: string): Observable<any> {
      return this.http.get(urlPath);
    }
/** Update API Function */
    update(urlPath: string, body?: any): Observable<any> {
      return this.http.put(urlPath, body);
    }
/** Delete API Function */
    delete(urlPath: string, params?: any): Observable<any> {
      return this.http.delete(urlPath,params);
    }
/** DeleteAll API Function */
    delete_all(urlPath: string, body?: any): Observable<any> {
      return this.http.delete(urlPath,{body:JSON.parse(body)});
    }
/** File Upload API Function */
    upload_File(urlPath: any, formData: any): Observable<any> {
      return this.http.post(urlPath, formData)
    }
    edit_upload_File(urlPath: any, formData: any): Observable<any> {
      return this.http.put(urlPath, formData)
    }
/** Policy Version API FUnction */    
    post_version(relativeUrlPath: string, body: any, params?: object): Observable<any> {
      return this.http.post(relativeUrlPath, body, {responseType: 'text'});
    }
}