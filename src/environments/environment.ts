// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: ".0.0",
  BACKEND_URL:'https://dev-impac-base.myamberinnovations.com/impac-base/',
  // BACKEND_URL: 'https://stage-impac-base.myamberinnovations.com/stage-impac-base/',
  // BACKEND_URL:"http://localhost:8080/impac-base/",
  SkipLocationChange: false,
  recaptcha: {
    siteKey: '6LfvFHYiAAAAALTwkkX7a1re-nAo32r8hccFzTU_'
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
