export const environment = {
  production: true,
   BACKEND_URL: 'https://dev-impac-base.myamberinnovations.com/impac-base/',
  // BACKEND_URL: 'https://stage-impac-base.myamberinnovations.com/stage-impac-base/',
  SkipLocationChange: true,
  recaptcha: {
    siteKey: '6LdH6YciAAAAAIr8kuEryRfvyaKCNjlbXIYSdKFM'
  }
};
